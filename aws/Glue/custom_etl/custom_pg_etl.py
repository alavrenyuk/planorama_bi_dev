import sys
import boto3
import pg8000
import sys
from awsglue.utils import getResolvedOptions

ssm = boto3.client('ssm')
redshift_user = (ssm.get_parameter(Name='power-bi-redshift-user', WithDecryption=True)['Parameter']['Value'])
redshift_password = (ssm.get_parameter(Name='power-bi-redshift-password', WithDecryption=True)['Parameter']['Value'])
redshift_catalog = ('procter_and_gamble_es')
redshift_host = (ssm.get_parameter(Name='power-bi-redshift-host', WithDecryption=True)['Parameter']['Value'])
redshift_port = int(ssm.get_parameter(Name='power-bi-redshift-port', WithDecryption=True)['Parameter']['Value'])
aws_access_key_id = (ssm.get_parameter(Name='power-bi-access-key-id', WithDecryption=True)['Parameter']['Value'])
aws_secret_access_key = (ssm.get_parameter(Name='power-bi-secret-access-key', WithDecryption=True)['Parameter']['Value'])

conn = pg8000.connect(user=redshift_user, database=redshift_catalog, host=redshift_host, port=redshift_port, password=redshift_password)
cursor = conn.cursor()

print('opportunity_l1_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.opportunity_l1_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.opportunity_l1_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.opportunity_l1_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.opportunity_l1_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')

print('opportunity_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.opportunity_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.opportunity_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.opportunity_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.opportunity_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	
print('merch_opt_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.merch_opt_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.merch_opt_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.merch_opt_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.merch_opt_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	

print('perfect_shelf_rules_merch_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.perfect_shelf_rules_merch_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.perfect_shelf_rules_merch_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.perfect_shelf_rules_merch_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.perfect_shelf_rules_merch_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	

print('perfect_shelf_rules_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.perfect_shelf_rules_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.perfect_shelf_rules_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.perfect_shelf_rules_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.perfect_shelf_rules_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')

	
print('visits_sales_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.visits_sales_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.visits_sales_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.visits_sales_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.visits_sales_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	

print('visits_sales_l3_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.visits_sales_l3_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.visits_sales_l3_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.visits_sales_l3_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.visits_sales_l3_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')


print('perfect_shelf_rules_url_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.perfect_shelf_rules_url_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.perfect_shelf_rules_url_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.perfect_shelf_rules_url_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.perfect_shelf_rules_url_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	
	
print('displays_base_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.displays_base_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.displays_base_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.displays_base_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.displays_base_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')

	
print('primary_shelf_base_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.primary_shelf_base_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.primary_shelf_base_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.primary_shelf_base_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.primary_shelf_base_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	
	
print('visit_calendar_lookup_table_1920')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS procter_and_gamble_es_mass.visit_calendar_lookup_table_1920;'
	sql_create_table = 'CREATE TABLE procter_and_gamble_es_mass.visit_calendar_lookup_table_1920 AS (SELECT * FROM procter_and_gamble_es_mass.visit_calendar_lookup_1920);'
	sql_grant = 'GRANT SELECT ON procter_and_gamble_es_mass.visit_calendar_lookup_table_1920 TO powerbi_user1, pg_es;'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	
conn.commit()
conn.close()