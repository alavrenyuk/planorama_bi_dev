import sys
import boto3
import pg8000
import sys
from awsglue.utils import getResolvedOptions

ssm = boto3.client('ssm')
redshift_user = (ssm.get_parameter(Name='power-bi-redshift-user', WithDecryption=True)['Parameter']['Value'])
redshift_password = (ssm.get_parameter(Name='power-bi-redshift-password', WithDecryption=True)['Parameter']['Value'])
redshift_catalog = ('production')
redshift_host = (ssm.get_parameter(Name='power-bi-redshift-host', WithDecryption=True)['Parameter']['Value'])
redshift_port = int(ssm.get_parameter(Name='power-bi-redshift-port', WithDecryption=True)['Parameter']['Value'])
aws_access_key_id = (ssm.get_parameter(Name='power-bi-access-key-id', WithDecryption=True)['Parameter']['Value'])
aws_secret_access_key = (ssm.get_parameter(Name='power-bi-secret-access-key', WithDecryption=True)['Parameter']['Value'])

conn = pg8000.connect(user=redshift_user, database=redshift_catalog, host=redshift_host, port=redshift_port, password=redshift_password)
cursor = conn.cursor()

print('gm_me_opt_final_table')
try:
	sql_drop_table = 'DROP TABLE IF EXISTS gm_me_opt_final_table;'
	sql_create_table = 'CREATE TABLE gm_me_opt_final_table AS (SELECT * FROM gm_me_opt_final);'
	sql_grant = 'GRANT SELECT ON gm_me_opt_final_table TO powerbi_user1;'
	sql_commit = 'COMMIT'
	cursor.execute(sql_drop_table)
	cursor.execute(sql_create_table)
	cursor.execute(sql_grant)
	cursor.execute(sql_commit)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	
conn.commit()
conn.close()