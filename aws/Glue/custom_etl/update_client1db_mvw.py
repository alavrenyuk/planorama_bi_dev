import boto3
import pg
import sys
from awsglue.utils import getResolvedOptions

# Get values from parameters store
ssm = boto3.client('ssm')
aurora_etl_user = (ssm.get_parameter(Name='power-bi-aurora-etl-user', WithDecryption=True)['Parameter']['Value'])
aurora_etl_password = (ssm.get_parameter(Name='power-bi-aurora-etl-password', WithDecryption=True)['Parameter']['Value'])
scripts_s3bucket = (ssm.get_parameter(Name='power-bi-aurora-scripts-s3bucket', WithDecryption=True)['Parameter']['Value'])

try:
    conn = pg.connect(dbname='client1db', host='database-1-instance-1.cuiigamssvrb.eu-west-1.rds.amazonaws.com', port=5432, user=aurora_etl_user, passwd=aurora_etl_password)

    s3 = boto3.resource('s3')
    obj = s3.Object(scripts_s3bucket, 'custom_etl/update_client1db_mvw.cfg')
    response = obj.get()

    # read the contents of the file and split it into a list of lines
    lines = response['Body'].read().decode('utf-8').split()

    # now iterate over those lines
    for row in lines:
        # print(row)        
        sql_command = 'REFRESH MATERIALIZED VIEW ' + row.strip() + ';'
        print(sql_command)
        q = conn.query(sql_command)

except:
    raise sys.exc_info()[0]

conn.close()

print('OK')