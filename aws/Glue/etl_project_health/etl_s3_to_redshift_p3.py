import boto3
import pg8000
import datetime
import sys
from awsglue.utils import getResolvedOptions

class IngestClient:

	def ingest_table(self, staging_table, filename_contains, filename_not_contains):
		try:
			s3 = boto3.resource('s3')
			my_bucket = s3.Bucket(s3_bucket)
			filename = None
			for object_summary in my_bucket.objects.filter(Prefix=s3_folder):
				if filename_contains in object_summary.key and filename_not_contains not in object_summary.key:
					filename = 's3://' + s3_bucket + '/' + object_summary.key
					sql_copy = 'COPY ' + staging_table + ' FROM \'' + filename + '\' iam_role ' + '\'' + iam_role + '\'' + ' CSV QUOTE ' + '\'\"\'' + ' IGNOREHEADER 1  DELIMITER \';\'  ACCEPTINVCHARS  \'?\'  TIMEFORMAT  \'auto\';'
					print(sql_copy)
					cursor.execute(sql_copy)
					newClient.archive_file(s3_bucket, object_summary.key, s3_folder, archive_folder)
		except:
			print('Exception has been thrown',sys.exc_info()[0],'occured.')
			raise sys.exc_info()[0]
		return filename

	def archive_file(self, s3_bucket_name, key, s3_folder, archive_folder):
		try:
			s3 = boto3.resource('s3')
			my_bucket = s3.Bucket(s3_bucket_name)
			archive_key = archive_folder + '/' + key
			copy_source = {'Bucket': s3_bucket_name,'Key': key}
			s3.meta.client.copy(copy_source, s3_bucket_name, archive_key)
			print('archive_key:' + archive_key)
		except:
			print('Exception has been thrown',sys.exc_info()[0],'occured.')
			raise sys.exc_info()[0]

	def delete_file(self, filename_contains, filename_not_contains):
		try:
			s3 = boto3.resource('s3')
			my_bucket = s3.Bucket(s3_bucket)
			filename = None
			for object_summary in my_bucket.objects.filter(Prefix=s3_folder):
				if filename_contains in object_summary.key and filename_not_contains not in object_summary.key:
					print('delete_key: ' + object_summary.key)
					obj = s3.Object(s3_bucket, object_summary.key)
					obj.delete()
		except:
			print('Exception has been thrown',sys.exc_info()[0],'occured.')
			raise sys.exc_info()[0]

#Here we start
#Get values from parameters store
print(datetime.datetime.now())
ssm = boto3.client('ssm')
redshift_user = (ssm.get_parameter(Name='power-bi-redshift-user', WithDecryption=True)['Parameter']['Value'])
redshift_password = (ssm.get_parameter(Name='power-bi-redshift-password', WithDecryption=True)['Parameter']['Value'])
redshift_catalog = (ssm.get_parameter(Name='power-bi-redshift-catalog-dev', WithDecryption=True)['Parameter']['Value'])
redshift_host = (ssm.get_parameter(Name='power-bi-redshift-host', WithDecryption=True)['Parameter']['Value'])
redshift_port = int(ssm.get_parameter(Name='power-bi-redshift-port', WithDecryption=True)['Parameter']['Value'])
iam_role = (ssm.get_parameter(Name='power-bi-iam-role', WithDecryption=True)['Parameter']['Value'])

s3_bucket = (ssm.get_parameter(Name='power-bi-s3-s3bucket', WithDecryption=True)['Parameter']['Value'])
archive_folder = (ssm.get_parameter(Name='power-bi-s3-archive', WithDecryption=True)['Parameter']['Value'])


print('Parameters')
print('redshift_user: ' + redshift_user)
print('redshift_password: ' + redshift_password)
print('redshift_catalog: ' + redshift_catalog)
print('redshift_host: ' + redshift_host)
print('redshift_port: ' + str(redshift_port))
print('iam_role: ' + iam_role)
print(' ')

# Get job keys
# args = getResolvedOptions(sys.argv, ['account_id', 's3_bucket', 's3_folder', 'archive_folder'])
args = getResolvedOptions(sys.argv, ['account_id', 's3_folder'])

account_id = args['account_id']
# s3_bucket = args['s3_bucket']
s3_folder = args['s3_folder']
# archive_folder = args['archive_folder']

print('Job keys')
print('account_id: ' + account_id)
print('s3_bucket: ' + s3_bucket)
print('s3_folder : ' + s3_folder)
print('archive_folder : ' + archive_folder)

newClient = IngestClient()

conn = pg8000.connect(user=redshift_user, database=redshift_catalog, host=redshift_host, port=redshift_port, password=redshift_password)
cursor = conn.cursor()

print(' ')

print('master-data-products')
try:
	sql_begin = 'BEGIN TRANSACTION'
	cursor.execute(sql_begin)

	sql_delete_stg = 'DELETE FROM staging.master_data_product WHERE account_id=\'' + account_id + '\'' + ';'
	print(sql_delete_stg)
	cursor.execute(sql_delete_stg)
	product_key = newClient.ingest_table('staging.master_data_product', 'master-data-products', 'nonsense') # We surely don't expect 'nonsense' filename part
	if product_key is not None:
		sql_delete_products = 'DELETE FROM consolidated.master_product WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.master_product_transformation t2 WHERE t2.account_id = consolidated.master_product.account_id AND t2.product_id = consolidated.master_product.product_id);'
		print(sql_delete_products)
		cursor.execute(sql_delete_products)
		sql_insert_products = 'INSERT INTO consolidated.master_product SELECT * FROM staging.master_product_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_products)
		cursor.execute(sql_insert_products)
		sql_insert_products_err = 'INSERT INTO error.master_data_product SELECT * FROM staging.master_product_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_products_err)
		cursor.execute(sql_insert_products_err)
		cursor.execute(sql_delete_stg)
		newClient.delete_file('master-data-products', 'nonsense')

	sql_end = 'END TRANSACTION'
	cursor.execute(sql_end)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	raise sys.exc_info()[0]
print(' ')

print('master-data-store')
try:
	sql_begin = 'BEGIN TRANSACTION'
	cursor.execute(sql_begin)

	sql_delete_stg = 'DELETE FROM staging.master_data_store WHERE account_id=\'' + account_id + '\'' + ';'
	print(sql_delete_stg)
	cursor.execute(sql_delete_stg)
	store_key = newClient.ingest_table('staging.master_data_store', 'master-data-store', 'nonsense')
	if store_key is not None:
		sql_delete_store = 'DELETE FROM consolidated.master_store WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.master_store_transformation t2 WHERE t2.account_id = consolidated.master_store.account_id AND t2.store_id = consolidated.master_store.store_id);'
		print(sql_delete_store)
		cursor.execute(sql_delete_store)
		sql_insert_store = 'INSERT INTO consolidated.master_store SELECT * FROM staging.master_store_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_store)
		cursor.execute(sql_insert_store)
		sql_insert_store_err = 'INSERT INTO error.master_data_store SELECT * FROM staging.master_store_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		cursor.execute(sql_insert_store_err)
		cursor.execute(sql_delete_stg)
		newClient.delete_file('master-data-store', 'nonsense')

	sql_end = 'END TRANSACTION'
	cursor.execute(sql_end)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	raise sys.exc_info()[0]
print(' ')

print('master-data-user')
try:
	sql_begin = 'BEGIN TRANSACTION'
	cursor.execute(sql_begin)

	sql_delete_stg = 'DELETE FROM staging.master_data_user WHERE account_id=\'' + account_id + '\'' + ';'
	print(sql_delete_stg)
	cursor.execute(sql_delete_stg)
	user_key = newClient.ingest_table('staging.master_data_user', 'master-data-user', 'hierarchy') # We find 'master_data_user' filename part, but it shouldn't contain 'hierarchy'
	if user_key is not None:
		sql_delete_user = 'DELETE FROM consolidated.master_user WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.master_user_transformation t2 WHERE t2.account_id = consolidated.master_user.account_id AND t2.user_id = consolidated.master_user.owner_id);'
		print(sql_delete_user)
		cursor.execute(sql_delete_user)
		sql_insert_user = 'INSERT INTO consolidated.master_user SELECT * FROM staging.master_user_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_user)
		cursor.execute(sql_insert_user)
		sql_insert_user_err = 'INSERT INTO error.master_data_user SELECT * FROM staging.master_user_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		cursor.execute(sql_insert_user_err)
		cursor.execute(sql_delete_stg)
		newClient.delete_file('master-data-user', 'hierarchy')

	sql_end = 'END TRANSACTION'
	cursor.execute(sql_end)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	raise sys.exc_info()[0]
print(' ')

print('master-data-user-hierarchy')
try:
	sql_begin = 'BEGIN TRANSACTION'
	cursor.execute(sql_begin)

	sql_delete_stg = 'DELETE FROM staging.master_data_user_hierarchy WHERE account_id=\'' + account_id + '\'' + ';'
	print(sql_delete_stg)
	cursor.execute(sql_delete_stg)
	hier_key = newClient.ingest_table('staging.master_data_user_hierarchy', 'master-data-user-hierarchy', 'nonsense') # We surely don't expect 'nonsense' filename part
	if hier_key is not None:
		sql_delete_hier = 'DELETE FROM consolidated.master_user_hierarchy WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.master_user_hierarchy_transformation t2 WHERE t2.account_id = consolidated.master_user_hierarchy.account_id);'
		print(sql_delete_hier)
		cursor.execute(sql_delete_hier)
		sql_insert_hier = 'INSERT INTO consolidated.master_user_hierarchy SELECT * FROM staging.master_user_hierarchy_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_hier)
		cursor.execute(sql_insert_hier)
		cursor.execute(sql_delete_stg)
		newClient.delete_file('master-data-user-hierarchy', 'nonsense')

	sql_end = 'END TRANSACTION'
	cursor.execute(sql_end)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	raise sys.exc_info()[0]
print(' ')

print('new-analysis')
try:
	sql_begin = 'BEGIN TRANSACTION'
	cursor.execute(sql_begin)

	sql_delete_stg = 'DELETE FROM staging.new_analysis WHERE account_id=\'' + account_id + '\'' + ';'
	print(sql_delete_stg)
	cursor.execute(sql_delete_stg)
	analysis_key = newClient.ingest_table('staging.new_analysis', 'new-analysis', 'nonsense') # We surely don't expect 'nonsense' filename part
	if analysis_key is not None:
		print('calendar')
		sql_delete_cld = 'DELETE FROM consolidated.calendar WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.calendar_transformation t2 WHERE t2.account_id = consolidated.calendar.account_id AND t2.visit_date = consolidated.calendar.visit_date);'
		print(sql_delete_cld)
		cursor.execute(sql_delete_cld)
		sql_insert_cld = 'INSERT INTO consolidated.calendar SELECT * FROM staging.calendar_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_cld)
		cursor.execute(sql_insert_cld)

		print('compliance_fact')
		sql_delete_cmp = 'DELETE FROM consolidated.compliance_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.compliance_transformation t2 WHERE t2.account_id = consolidated.compliance_fact.account_id AND t2.visit_id = consolidated.compliance_fact.visit_id AND t2.kpi_id = consolidated.compliance_fact.kpi_id);'
		print(sql_delete_cmp)
		cursor.execute(sql_delete_cmp)
		sql_insert_cmp = 'INSERT INTO consolidated.compliance_fact SELECT * FROM staging.compliance_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_cmp)
		cursor.execute(sql_insert_cmp)
		sql_insert_cmp_err = 'INSERT INTO error.compliance_fact SELECT * FROM staging.compliance_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';' 
		print(sql_insert_cmp_err)
		cursor.execute(sql_insert_cmp_err) 
		
		print('photo_fact')
		sql_delete_pho = 'DELETE FROM consolidated.photo_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.photo_transformation t2 WHERE t2.account_id = consolidated.photo_fact.account_id AND t2.visit_id = consolidated.photo_fact.visit_id AND t2.photo_index = consolidated.photo_fact.photo_index);'
		print(sql_delete_pho)
		cursor.execute(sql_delete_pho)
		sql_insert_pho = 'INSERT INTO consolidated.photo_fact SELECT * FROM staging.photo_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_pho)
		cursor.execute(sql_insert_pho)
		sql_insert_pho_err = 'INSERT INTO error.photo_fact SELECT * FROM staging.photo_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';' 
		print(sql_insert_pho_err)
		cursor.execute(sql_insert_pho_err)
		
		print('product_fact')
		sql_delete_prd = 'DELETE FROM consolidated.product_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.product_transformation t2 WHERE t2.account_id = consolidated.product_fact.account_id AND t2.visit_id = consolidated.product_fact.visit_id AND t2.product_id = consolidated.product_fact.product_id);'
		print(sql_delete_prd)
		cursor.execute(sql_delete_prd)
		sql_insert_prd = 'INSERT INTO consolidated.product_fact SELECT * FROM staging.product_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_prd)
		cursor.execute(sql_insert_prd)
		sql_insert_product_err = 'INSERT INTO error.product_fact SELECT * FROM staging.product_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';' 
		print(sql_insert_product_err)
		cursor.execute(sql_insert_product_err)

		print('product_kpi_fact')
		sql_delete_kpi = 'DELETE FROM consolidated.product_kpi_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.product_kpi_transformation t2 WHERE t2.account_id = consolidated.product_kpi_fact.account_id AND t2.visit_id = consolidated.product_kpi_fact.visit_id AND t2.product_id = consolidated.product_kpi_fact.product_id AND t2.analysis = consolidated.product_kpi_fact.analysis AND t2.kpi = consolidated.product_kpi_fact.kpi);'
		print(sql_delete_kpi)
		cursor.execute(sql_delete_kpi)
		sql_insert_kpi = 'INSERT INTO consolidated.product_kpi_fact SELECT * FROM staging.product_kpi_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_kpi)
		cursor.execute(sql_insert_kpi)
		sql_insert_product_kpi_err = 'INSERT INTO error.product_kpi_fact SELECT * FROM staging.product_kpi_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_product_kpi_err)
		cursor.execute(sql_insert_product_kpi_err)

		print('visit_kpi_fact')
		sql_delete_visit_kpi = 'DELETE FROM consolidated.visit_kpi_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.visit_kpi_transformation t2 WHERE t2.account_id = consolidated.visit_kpi_fact.account_id AND t2.visit_id = consolidated.visit_kpi_fact.visit_id AND t2.row_id = consolidated.visit_kpi_fact.row_id);'
		print(sql_delete_visit_kpi)
		cursor.execute(sql_delete_visit_kpi)
		sql_insert_visit_kpi = 'INSERT INTO consolidated.visit_kpi_fact SELECT * FROM staging.visit_kpi_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_visit_kpi)
		cursor.execute(sql_insert_visit_kpi)
		sql_insert_visit_kpi_err = 'INSERT INTO error.visit_kpi_fact SELECT * FROM staging.visit_kpi_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_visit_kpi_err)
		cursor.execute(sql_insert_visit_kpi_err)

		print('visit_fact')
		sql_delete_visit = 'DELETE FROM consolidated.visit_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.visit_transformation t2 WHERE t2.account_id = consolidated.visit_fact.account_id AND t2.visit_id = consolidated.visit_fact.visit_id);'
		print(sql_delete_visit)
		cursor.execute(sql_delete_visit)
		sql_insert_visit = 'INSERT INTO consolidated.visit_fact SELECT * FROM staging.visit_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_visit)
		cursor.execute(sql_insert_visit)
		sql_insert_visit_err = 'INSERT INTO error.visit_fact SELECT * FROM staging.visit_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_visit_err)
		cursor.execute(sql_insert_visit_err)

		cursor.execute(sql_delete_stg)
		newClient.delete_file('new-analysis', 'nonsense')

	sql_end = 'END TRANSACTION'
	cursor.execute(sql_end)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	raise sys.exc_info()[0]
print(' ')

print('accuracy-tracking')
try:
	sql_begin = 'BEGIN TRANSACTION'
	cursor.execute(sql_begin)

	sql_delete_stg = 'DELETE FROM staging.accuracy_tracking WHERE account_id=\'' + account_id + '\'' + ';'
	print(sql_delete_stg)
	cursor.execute(sql_delete_stg)
	accuracy_key = newClient.ingest_table('staging.accuracy_tracking', 'accuracy-tracking', 'nonsense') # We surely don't expect 'nonsense' filename part
	if accuracy_key is not None:
		sql_delete_accvst = 'DELETE FROM consolidated.accuracy_visit_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.accuracy_visit_transformation t2 WHERE t2.account_id = consolidated.accuracy_visit_fact.account_id AND t2.visit_id = consolidated.accuracy_visit_fact.visit_id);'
		print(sql_delete_accvst)
		cursor.execute(sql_delete_accvst)
		
		sql_insert_accvst = 'INSERT INTO consolidated.accuracy_visit_fact SELECT * FROM staging.accuracy_visit_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_accvst)
		cursor.execute(sql_insert_accvst)
		
		sql_delete_accprd = 'DELETE FROM consolidated.accuracy_product_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.accuracy_product_transformation t2 WHERE t2.account_id = consolidated.accuracy_product_fact.account_id AND t2.visit_id = consolidated.accuracy_product_fact.visit_id);'
		print(sql_delete_accprd)
		cursor.execute(sql_delete_accprd)
		
		sql_insert_accprd = 'INSERT INTO consolidated.accuracy_product_fact SELECT * FROM staging.accuracy_product_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_accprd)
		cursor.execute(sql_insert_accprd)
		
		sql_insert_acc_err = 'INSERT INTO error.accuracy_tracking SELECT * FROM staging.accuracy_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_acc_err)
		cursor.execute(sql_insert_acc_err)
		
		cursor.execute(sql_delete_stg)
		newClient.delete_file('accuracy-tracking', 'nonsense')

	sql_end = 'END TRANSACTION'
	cursor.execute(sql_end)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')	
	raise sys.exc_info()[0]
print(' ')

print('new-time-tracking')
try:
	sql_begin = 'BEGIN TRANSACTION'
	cursor.execute(sql_begin)

	sql_delete_stg = 'DELETE FROM staging.new_time_tracking WHERE account_id=\'' + account_id + '\'' + ';'
	print(sql_delete_stg)
	cursor.execute(sql_delete_stg)
	time_key = newClient.ingest_table('staging.new_time_tracking', 'new-time-tracking', 'nonsense') # We surely don't expect 'nonsense' filename part
	if time_key is not None:
		sql_delete_ntt = 'DELETE FROM consolidated.new_time_tracking_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.new_time_tracking_transformation t2 WHERE t2.account_id = consolidated.new_time_tracking_fact.account_id AND t2.visit_id = consolidated.new_time_tracking_fact.visit_id);'
		print(sql_delete_ntt)
		cursor.execute(sql_delete_ntt)
		
		sql_insert_ntt = 'INSERT INTO consolidated.new_time_tracking_fact SELECT * FROM staging.new_time_tracking_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_ntt)
		cursor.execute(sql_insert_ntt)

		sql_insert_err = 'INSERT INTO error.new_time_tracking SELECT * FROM staging.new_time_tracking_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_err)
		cursor.execute(sql_insert_err)        
		
		cursor.execute(sql_delete_stg)
		newClient.delete_file('new-time-tracking', 'nonsense')

	sql_end = 'END TRANSACTION'
	cursor.execute(sql_end)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	raise sys.exc_info()[0]
print(' ')

print('user-feedback')
try:
	sql_begin = 'BEGIN TRANSACTION'
	cursor.execute(sql_begin)

	sql_delete_stg = 'DELETE FROM staging.user_feedback WHERE account_id=\'' + account_id + '\'' + ';'
	print(sql_delete_stg)
	cursor.execute(sql_delete_stg)
	user_key = newClient.ingest_table('staging.user_feedback', 'user-feedback', 'nonsense') # We surely don't expect 'nonsense' filename part
	if user_key is not None:
		sql_delete_usr = 'DELETE FROM consolidated.user_feedback_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.user_feedback_transformation t2 WHERE t2.account_id = consolidated.user_feedback_fact.account_id AND t2.visit_id = consolidated.user_feedback_fact.visit_id AND t2.product_ean = consolidated.user_feedback_fact.product_ean AND t2.kpi_id = consolidated.user_feedback_fact.kpi_id);'
		print(sql_delete_usr)
		cursor.execute(sql_delete_usr)
		
		sql_insert_usr = 'INSERT INTO consolidated.user_feedback_fact SELECT * FROM staging.user_feedback_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_usr)
		cursor.execute(sql_insert_usr)

		sql_insert_err = 'INSERT INTO error.user_feedback SELECT * FROM staging.user_feedback_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_err)
		cursor.execute(sql_insert_err)
		
		cursor.execute(sql_delete_stg)
		newClient.delete_file('user-feedback', 'nonsense')
	
	sql_end = 'END TRANSACTION'
	cursor.execute(sql_end)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')
	raise sys.exc_info()[0]
print(' ')

print('geolocation')
try:
	sql_begin = 'BEGIN TRANSACTION'
	cursor.execute(sql_begin)

	sql_delete_stg = 'DELETE FROM staging.geolocation WHERE account_id=\'' + account_id + '\'' + ';'
	print(sql_delete_stg)
	cursor.execute(sql_delete_stg)
	product_key = newClient.ingest_table('staging.geolocation', 'geolocation', 'nonsense') # We surely don't expect 'nonsense' filename part
	if product_key is not None:
		sql_delete_geo = 'DELETE FROM consolidated.geolocation WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.geolocation_transformation t2 WHERE t2.account_id = consolidated.geolocation.account_id AND t2.visit_id = consolidated.geolocation.visit_id);'
		print(sql_delete_geo)
		cursor.execute(sql_delete_geo)
		
		sql_insert_geo = 'INSERT INTO consolidated.geolocation SELECT * FROM staging.geolocation_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_geo)
		cursor.execute(sql_insert_geo)

		sql_insert_err = 'INSERT INTO error.geolocation SELECT * FROM staging.geolocation_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_err)
		cursor.execute(sql_insert_err)

		cursor.execute(sql_delete_stg)
		newClient.delete_file('geolocation', 'nonsense')
	
	sql_end = 'END TRANSACTION'
	cursor.execute(sql_end)
except:
	print('Exception has been thrown',sys.exc_info()[0],'occured.')	
	raise sys.exc_info()[0]
print(' ')

print('monitor-visits-from-source')
try:
	sql_begin = 'BEGIN TRANSACTION'
	cursor.execute(sql_begin)

	sql_delete_stg = 'DELETE FROM staging.monitor_visit_source WHERE account_id=\'' + account_id + '\'' + ';'
	print(sql_delete_stg)
	cursor.execute(sql_delete_stg)
	
	product_key = newClient.ingest_table('staging.monitor_visit_source', 'visits-from-source', 'nonsense') # We surely don't expect 'nonsense' filename part
	if product_key is not None:
		sql_delete_mon = 'DELETE FROM consolidated.monitor_visit_source WHERE account_id=\'' + account_id + '\'' + 	' AND EXISTS (SELECT 1 FROM staging.monitor_visit_source_transformation t2 WHERE t2.account_id = consolidated.monitor_visit_source.account_id AND t2.visit_id = consolidated.monitor_visit_source.visit_id);'
		print(sql_delete_mon)
		cursor.execute(sql_delete_mon)

		sql_insert_mon = 'INSERT INTO consolidated.monitor_visit_source SELECT * FROM staging.monitor_visit_source_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_mon)
		cursor.execute(sql_insert_mon)

		sql_insert_err = 'INSERT INTO error.monitor_visit_source SELECT * FROM staging.monitor_visit_source_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
		print(sql_insert_err)
		cursor.execute(sql_insert_err)

		cursor.execute(sql_delete_stg)
		newClient.delete_file('visits-from-source', 'nonsense')

	sql_end = 'END TRANSACTION'
	cursor.execute(sql_end)
except:
	print('Exception has been thrown', sys.exc_info()[0], 'occured.')
	raise sys.exc_info()[0]
print(' ')

conn.commit()
conn.close()

print(datetime.datetime.now())
print('OK')