import boto3
import pg
import time
import sys
from awsglue.utils import getResolvedOptions

class IngestClient:

    def ingest_table(self, staging_table, filename_contains, filename_not_contains):
        try:
            s3 = boto3.resource('s3')
            my_bucket = s3.Bucket(s3_bucket)
            filename = None
            for object_summary in my_bucket.objects.filter(Prefix=s3_folder):
                if filename_contains in object_summary.key and filename_not_contains not in object_summary.key:
                    
                    oldFileName = object_summary.key
                    newFileName = object_summary.key.replace(':','_')
                    if newFileName != oldFileName:
                        s3.Object(s3_bucket,newFileName).copy_from(CopySource = s3_bucket + '/' + oldFileName)
                        s3.Object(s3_bucket,oldFileName).delete()
                    
                    csvRows =  newClient.count_csvRows(s3_bucket, newFileName)
                    
                    time.sleep(2)
                    
                    if csvRows > 0:
                        filename = 's3://' + s3_bucket + '/' + newFileName
                        sql_copy = 'SELECT aws_s3.table_import_from_s3(\'' + staging_table + '\',\'\',\'DELIMITER \'\'~\'\' CSV HEADER ENCODING \'\' UTF8\'\'\',' + ' aws_commons.create_s3_uri(\''  + s3_bucket + '\',\'' + newFileName + '\',\'' + aws_region + '\'))'
                        print(sql_copy)
                        q = conn.query(sql_copy)
                        print(q.getresult())
                    newClient.archive_file(s3_bucket, newFileName, s3_folder, archive_folder)
        except:
            raise sys.exc_info()[0]
        return filename

    def archive_file(self, s3_bucket_name, key, s3_folder, archive_folder):
        try:
            s3 = boto3.resource('s3')
            my_bucket = s3.Bucket(s3_bucket_name)
            archive_key = archive_folder + '/' + key
            copy_source = {'Bucket': s3_bucket_name,'Key': key}
            s3.meta.client.copy(copy_source, s3_bucket_name, archive_key)
            print('archive_key:' + archive_key)
            delete_key = key
            print('delete_key: ' + delete_key)
            obj = s3.Object(s3_bucket_name, delete_key)
            obj.delete()
        except:
            raise sys.exc_info()[0]
    
    def count_csvRows(self, s3_bucket_name, fileName):
        try:
            s3 = boto3.resource('s3')
            s3obj = s3.Object(s3_bucket_name, fileName)
            filedata = s3obj.get()["Body"].read()
            csvRows = filedata.decode('utf8').count('\n')-1
        except:
            raise sys.exc_info()[0]            
        return csvRows

# Get values from parameters store
ssm = boto3.client('ssm')
aurora_etl_user = (ssm.get_parameter(Name='power-bi-aurora-etl-user', WithDecryption=True)['Parameter']['Value'])
aurora_etl_password = (ssm.get_parameter(Name='power-bi-aurora-etl-password', WithDecryption=True)['Parameter']['Value'])
aws_region = (ssm.get_parameter(Name='power-bi-aws-region', WithDecryption=True)['Parameter']['Value'])
s3_bucket = (ssm.get_parameter(Name='power-bi-aurora-etl-s3bucket', WithDecryption=True)['Parameter']['Value'])
archive_folder = (ssm.get_parameter(Name='power-bi-aurora-etl-archive', WithDecryption=True)['Parameter']['Value'])

# Get values from job parameters
args = getResolvedOptions(sys.argv, ['account_id', 's3_folder'])
account_id = args['account_id']
s3_folder = args['s3_folder']

print('Job keys')
print('account_id: ' + account_id)
print('s3_bucket: ' + s3_bucket)
print('s3_folder : ' + s3_folder)
print('archive_folder : ' + archive_folder)

# print('power-bi-aurora-etl-user : ' + aurora_etl_user)
# print('power-bi-aurora-etl-password : ' + aurora_etl_password)
# print('power-bi-aws-region : ' + aws_region)

newClient = IngestClient()

conn = pg.connect(dbname='postgres', host='database-1-instance-1.cuiigamssvrb.eu-west-1.rds.amazonaws.com', port=5432, user=aurora_etl_user, passwd=aurora_etl_password)

print('master-data-products')
try:
    sql_delete_stg = 'DELETE FROM staging.master_data_product WHERE account_id=\'' + account_id + '\';'
    print(sql_delete_stg)
    q = conn.query(sql_delete_stg)
    product_key = newClient.ingest_table('staging.master_data_product', 'master-data-products', 'nonsense') # We definetely don't expect 'nonsense' filename part
    if product_key is not None:
        # sql_delete_products = 'DELETE FROM consolidated.master_product WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.master_product_transformation t2 WHERE t2.account_id = consolidated.master_product.account_id AND t2.product_id = consolidated.master_product.product_id);'
        sql_delete_products = 'DELETE FROM consolidated.master_product WHERE account_id=\'' + account_id + '\';'
        print(sql_delete_products)
        q = conn.query(sql_delete_products)

        sql_insert_products = 'INSERT INTO consolidated.master_product SELECT * FROM staging.master_product_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_products)
        q = conn.query(sql_insert_products)

        sql_insert_products_err = 'INSERT INTO error.master_data_product SELECT * FROM staging.master_product_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_products_err)
        q = conn.query(sql_insert_products_err)

        q = conn.query(sql_delete_stg)
except:
    raise sys.exc_info()[0]
print(' ')

print('master-data-store')
try:
    sql_delete_stg = 'DELETE FROM staging.master_data_store WHERE account_id=\'' + account_id + '\'' + ';'
    print(sql_delete_stg)
    q = conn.query(sql_delete_stg)
    store_key = newClient.ingest_table('staging.master_data_store', 'master-data-store', 'nonsense')
    if store_key is not None:
        # sql_delete_store = 'DELETE FROM consolidated.master_store WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.master_store_transformation t2 WHERE t2.account_id = consolidated.master_store.account_id AND t2.store_id = consolidated.master_store.store_id);'
        sql_delete_store = 'DELETE FROM consolidated.master_store WHERE account_id=\'' + account_id + '\'' + ';'
        print(sql_delete_store)
        q = conn.query(sql_delete_store)

        sql_insert_store = 'INSERT INTO consolidated.master_store SELECT * FROM staging.master_store_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_store)
        q = conn.query(sql_insert_store)

        sql_insert_store_err = 'INSERT INTO error.master_data_store SELECT * FROM staging.master_store_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_store_err)
        q = conn.query(sql_insert_store_err)
       
        q = conn.query(sql_delete_stg)
except:
    raise sys.exc_info()[0]
print(' ')

print('master-data-user')
try:
    sql_delete_stg = 'DELETE FROM staging.master_data_user WHERE account_id=\'' + account_id + '\'' + ';'
    print(sql_delete_stg)
    q = conn.query(sql_delete_stg)
    user_key = newClient.ingest_table('staging.master_data_user', 'master-data-user', 'hierarchy') # We find 'master_data_user' filename part, but it shouldn't contain 'hierarchy'
    if user_key is not None:
        # sql_delete_user = 'DELETE FROM consolidated.master_user WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.master_user_transformation t2 WHERE t2.account_id = consolidated.master_user.account_id AND t2.user_id = consolidated.master_user.owner_id);'
        sql_delete_user = 'DELETE FROM consolidated.master_user WHERE account_id=\'' + account_id + '\';'
        print(sql_delete_user)
        q = conn.query(sql_delete_user)
        
        sql_insert_user = 'INSERT INTO consolidated.master_user SELECT * FROM staging.master_user_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_user)
        q = conn.query(sql_insert_user)

        sql_insert_user_err = 'INSERT INTO error.master_data_user SELECT * FROM staging.master_user_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_user_err)
        q = conn.query(sql_insert_user_err)
        
        q = conn.query(sql_delete_stg)
except:
    raise sys.exc_info()[0]
print(' ')

print('master-data-user-hierarchy')
try:
    sql_delete_stg = 'DELETE FROM staging.master_data_user_hierarchy WHERE account_id=\'' + account_id + '\'' + ';'
    print(sql_delete_stg)
    q = conn.query(sql_delete_stg)
    hier_key = newClient.ingest_table('staging.master_data_user_hierarchy', 'master-data-user-hierarchy', 'nonsense')
    if hier_key is not None:
        # sql_delete_hier = 'DELETE FROM consolidated.master_user_hierarchy WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.master_user_hierarchy_transformation t2 WHERE t2.account_id = consolidated.master_user_hierarchy.account_id AND t2.parent = consolidated.owner_name_parent AND t2.username = consolidated.owner_name_child);'
        sql_delete_hier = 'DELETE FROM consolidated.master_user_hierarchy WHERE account_id=\'' + account_id + '\';'
        print(sql_delete_hier)
        q = conn.query(sql_delete_hier)
        
        sql_insert_hier = 'INSERT INTO consolidated.master_user_hierarchy SELECT * FROM staging.master_user_hierarchy_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_hier)
        q = conn.query(sql_insert_hier)
        
        q = conn.query(sql_delete_stg)
except:
    raise sys.exc_info()[0]
print(' ')

print('new-analysis')
try:
    sql_delete_stg = 'DELETE FROM staging.new_analysis WHERE account_id=\'' + account_id + '\'' + ';'
    print(sql_delete_stg)
    q = conn.query(sql_delete_stg)
    analysis_key = newClient.ingest_table('staging.new_analysis', 'new-analysis', 'nonsense')
    if analysis_key is not None:
        
        print('calendar')
        sql_delete_cld = 'DELETE FROM consolidated.calendar WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.calendar_transformation t2 WHERE t2.account_id = consolidated.calendar.account_id AND t2.visit_date = consolidated.calendar.visit_date);'
        print(sql_delete_cld)
        q = conn.query(sql_delete_cld)
        sql_insert_cld = 'INSERT INTO consolidated.calendar SELECT * FROM staging.calendar_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_cld)
        q = conn.query(sql_insert_cld)

        print('compliance_fact')
        sql_delete_cmp = 'DELETE FROM consolidated.compliance_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.compliance_transformation t2 WHERE t2.account_id = consolidated.compliance_fact.account_id AND t2.visit_id = consolidated.compliance_fact.visit_id AND t2.kpi_id = consolidated.compliance_fact.kpi_id);'
        print(sql_delete_cmp)
        q = conn.query(sql_delete_cmp)
        sql_insert_cmp = 'INSERT INTO consolidated.compliance_fact SELECT * FROM staging.compliance_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_cmp)
        q = conn.query(sql_insert_cmp)
        sql_insert_compliance_err = 'INSERT INTO error.compliance_fact SELECT * FROM staging.compliance_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';' 
        print(sql_insert_compliance_err)
        q = conn.query(sql_insert_compliance_err) 
        
        print('photo_fact')
        sql_delete_pho = 'DELETE FROM consolidated.photo_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.photo_transformation t2 WHERE t2.account_id = consolidated.photo_fact.account_id AND t2.visit_id = consolidated.photo_fact.visit_id AND t2.photo_index = consolidated.photo_fact.photo_index);'
        print(sql_delete_pho)
        q = conn.query(sql_delete_pho)
        sql_insert_pho = 'INSERT INTO consolidated.photo_fact SELECT * FROM staging.photo_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_pho)
        q = conn.query(sql_insert_pho)
        sql_insert_photo_err = 'INSERT INTO error.photo_fact SELECT * FROM staging.photo_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';' 
        print(sql_insert_photo_err)
        q = conn.query(sql_insert_photo_err) 
        
        print('product_fact')
        sql_delete_prd = 'DELETE FROM consolidated.product_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.product_transformation t2 WHERE t2.account_id = consolidated.product_fact.account_id AND t2.visit_id = consolidated.product_fact.visit_id AND t2.product_id = consolidated.product_fact.product_id);'
        print(sql_delete_prd)
        q = conn.query(sql_delete_prd)
        sql_insert_prd = 'INSERT INTO consolidated.product_fact SELECT * FROM staging.product_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_prd)
        q = conn.query(sql_insert_prd)
        sql_insert_product_err = 'INSERT INTO error.product_fact SELECT * FROM staging.product_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';' 
        print(sql_insert_product_err)
        q = conn.query(sql_insert_product_err) 

        print('product_kpi_fact')
        sql_delete_kpi = 'DELETE FROM consolidated.product_kpi_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.product_kpi_transformation t2 WHERE t2.account_id = consolidated.product_kpi_fact.account_id AND t2.visit_id = consolidated.product_kpi_fact.visit_id AND t2.product_id = consolidated.product_kpi_fact.product_id AND t2.analysis = consolidated.product_kpi_fact.analysis AND t2.kpi = consolidated.product_kpi_fact.kpi);'
        print(sql_delete_kpi)
        q = conn.query(sql_delete_kpi)
        sql_insert_kpi = 'INSERT INTO consolidated.product_kpi_fact SELECT * FROM staging.product_kpi_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_kpi)
        q = conn.query(sql_insert_kpi)
        sql_insert_product_kpi_err = 'INSERT INTO error.product_kpi_fact SELECT * FROM staging.product_kpi_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_product_kpi_err)
        q = conn.query(sql_insert_product_kpi_err) 

        print('visit_kpi_fact')
        sql_delete_visit_kpi = 'DELETE FROM consolidated.visit_kpi_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.visit_kpi_transformation t2 WHERE t2.account_id = consolidated.visit_kpi_fact.account_id AND t2.visit_id = consolidated.visit_kpi_fact.visit_id AND t2.row_id = consolidated.visit_kpi_fact.row_id);'
        print(sql_delete_visit_kpi)
        q = conn.query(sql_delete_visit_kpi)
        sql_insert_visit_kpi = 'INSERT INTO consolidated.visit_kpi_fact SELECT * FROM staging.visit_kpi_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_visit_kpi)
        q = conn.query(sql_insert_visit_kpi)
        sql_insert_visit_kpi_err = 'INSERT INTO error.visit_kpi_fact SELECT * FROM staging.visit_kpi_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_visit_kpi_err)
        q = conn.query(sql_insert_visit_kpi_err)

        print('visit_fact')
        sql_delete_visit = 'DELETE FROM consolidated.visit_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.visit_transformation t2 WHERE t2.account_id = consolidated.visit_fact.account_id AND t2.visit_id = consolidated.visit_fact.visit_id);'
        print(sql_delete_visit)
        q = conn.query(sql_delete_visit)
        sql_insert_visit = 'INSERT INTO consolidated.visit_fact SELECT * FROM staging.visit_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_visit)
        q = conn.query(sql_insert_visit)
        sql_insert_visit_err = 'INSERT INTO error.visit_fact SELECT * FROM staging.visit_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_visit_err)
        q = conn.query(sql_insert_visit_err)
        
        q = conn.query(sql_delete_stg)
except:
    raise sys.exc_info()[0]
print(' ')

print('accuracy-tracking')
try:
    sql_delete_stg = 'DELETE FROM staging.accuracy_tracking WHERE account_id=\'' + account_id + '\'' + ';'
    print(sql_delete_stg)
    q = conn.query(sql_delete_stg)
    accuracy_key = newClient.ingest_table('staging.accuracy_tracking', 'accuracy-tracking', 'nonsense')
    if accuracy_key is not None:
        sql_delete_accvst = 'DELETE FROM consolidated.accuracy_visit_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.accuracy_visit_transformation t2 WHERE t2.account_id = consolidated.accuracy_visit_fact.account_id AND t2.visit_id = consolidated.accuracy_visit_fact.visit_id);'
        print(sql_delete_accvst)
        q = conn.query(sql_delete_accvst)
        
        sql_insert_accvst = 'INSERT INTO consolidated.accuracy_visit_fact SELECT * FROM staging.accuracy_visit_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_accvst)
        q = conn.query(sql_insert_accvst)
        
        sql_delete_accprd = 'DELETE FROM consolidated.accuracy_product_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.accuracy_product_transformation t2 WHERE t2.account_id = consolidated.accuracy_product_fact.account_id AND t2.visit_id = consolidated.accuracy_product_fact.visit_id);'
        print(sql_delete_accprd)
        q = conn.query(sql_delete_accprd)
        
        sql_insert_accprd = 'INSERT INTO consolidated.accuracy_product_fact SELECT * FROM staging.accuracy_product_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_accprd)
        q = conn.query(sql_insert_accprd)
        
        sql_insert_acc_err = 'INSERT INTO error.accuracy_tracking SELECT * FROM staging.accuracy_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_acc_err)
        q = conn.query(sql_insert_acc_err)
        
        q = conn.query(sql_delete_stg)
except:
    raise sys.exc_info()[0]
print(' ')

print('new-time-tracking')
try:
    sql_delete_stg = 'DELETE FROM staging.new_time_tracking WHERE account_id=\'' + account_id + '\'' + ';'
    print(sql_delete_stg)
    q = conn.query(sql_delete_stg)
    time_key = newClient.ingest_table('staging.new_time_tracking', 'new-time-tracking', 'nonsense')
    if time_key is not None:
        sql_delete_ntt = 'DELETE FROM consolidated.new_time_tracking_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.new_time_tracking_transformation t2 WHERE t2.account_id = consolidated.new_time_tracking_fact.account_id AND t2.visit_id = consolidated.new_time_tracking_fact.visit_id);'
        print(sql_delete_ntt)
        q = conn.query(sql_delete_ntt)
        
        sql_insert_ntt = 'INSERT INTO consolidated.new_time_tracking_fact SELECT * FROM staging.new_time_tracking_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_ntt)
        q = conn.query(sql_insert_ntt)

        sql_insert_err = 'INSERT INTO error.new_time_tracking SELECT * FROM staging.new_time_tracking_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_err)
        q = conn.query(sql_insert_err)        
        
        q = conn.query(sql_delete_stg)
except:
    raise sys.exc_info()[0]
print(' ')

print('user-feedback')
try:
    sql_delete_stg = 'DELETE FROM staging.user_feedback WHERE account_id=\'' + account_id + '\'' + ';'
    print(sql_delete_stg)
    q = conn.query(sql_delete_stg)
    user_key = newClient.ingest_table('staging.user_feedback', 'user-feedback', 'nonsense')
    if user_key is not None:
        sql_delete_usr = 'DELETE FROM consolidated.user_feedback_fact WHERE account_id=\'' + account_id + '\'' + ' AND EXISTS (SELECT 1 FROM staging.user_feedback_transformation t2 WHERE t2.account_id = consolidated.user_feedback_fact.account_id AND t2.visit_id = consolidated.user_feedback_fact.visit_id AND t2.product_ean = consolidated.user_feedback_fact.product_ean AND t2.kpi_id = consolidated.user_feedback_fact.kpi_id);'
        print(sql_delete_usr)
        q = conn.query(sql_delete_usr)
        
        sql_insert_usr = 'INSERT INTO consolidated.user_feedback_fact SELECT * FROM staging.user_feedback_transformation t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_usr)
        q = conn.query(sql_insert_usr)

        sql_insert_err = 'INSERT INTO error.user_feedback SELECT * FROM staging.user_feedback_transformation_err t1 WHERE t1.account_id=\'' + account_id + '\';'
        print(sql_insert_err)
        q = conn.query(sql_insert_err)
        
        q = conn.query(sql_delete_stg)
except:
    raise sys.exc_info()[0]
print(' ')

conn.close()

print('OK')