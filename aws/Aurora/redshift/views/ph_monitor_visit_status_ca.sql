DROP VIEW redshift.ph_monitor_visit_status_ca;

CREATE OR REPLACE VIEW redshift.ph_monitor_visit_status_ca
 AS
 SELECT ph_monitor_visit_status_ca_mvw.account_id,
    ph_monitor_visit_status_ca_mvw.uid,
    ph_monitor_visit_status_ca_mvw.visits_uploading,
    ph_monitor_visit_status_ca_mvw.visits_processing,
    ph_monitor_visit_status_ca_mvw.visits_live_rdy,
    ph_monitor_visit_status_ca_mvw.visits_verified,
    ph_monitor_visit_status_ca_mvw.visits_deleted,
    ph_monitor_visit_status_ca_mvw.visits_skipped,
    ph_monitor_visit_status_ca_mvw.visits_not_processed,
    ph_monitor_visit_status_ca_mvw.visits_analysed,
    ph_monitor_visit_status_ca_mvw.visits_under_analysis,
    ph_monitor_visit_status_ca_mvw.visits_total_relevant,
    ph_monitor_visit_status_ca_mvw.visits_total_w_not_precessed
   FROM redshift.ph_monitor_visit_status_ca_mvw;

COMMIT;