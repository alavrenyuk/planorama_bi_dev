DROP VIEW IF EXISTS redshift.ph_accuracy CASCADE;

CREATE OR REPLACE VIEW redshift.ph_accuracy
(
  uid,
  account_id,
  year_week,
  audit_total_facing,
  audit_total_facing_correct,
  overall_accuracy,
  osa_total_facing,
  osa_correct_facing,
  osa_accuracy,
  number_of_reported_osa,
  number_of_osa,
  ratio_of_reported_osa
)
AS 
 SELECT ph_accuracy_mvw.uid,
    ph_accuracy_mvw.account_id,
    ph_accuracy_mvw.year_week,
    ph_accuracy_mvw.audit_total_facing,
    ph_accuracy_mvw.audit_total_facing_correct,
    ph_accuracy_mvw.overall_accuracy,
    ph_accuracy_mvw.osa_total_facing,
    ph_accuracy_mvw.osa_correct_facing,
    ph_accuracy_mvw.osa_accuracy,
    ph_accuracy_mvw.number_of_reported_osa,
    ph_accuracy_mvw.number_of_osa,
    ph_accuracy_mvw.ratio_of_reported_osa
   FROM redshift.ph_accuracy_mvw;

COMMIT;