DROP VIEW IF EXISTS redshift.ph_home_timetracking CASCADE;

CREATE OR REPLACE VIEW redshift.ph_home_timetracking
(
  account_id,
  week_no,
  year_week,
  uid,
  current_tt,
  current_ptt,
  prev_week_tt,
  prev_week_ptt
)
AS 
 SELECT ph_home_timetracking_mvw.account_id,
    ph_home_timetracking_mvw.week_no,
    ph_home_timetracking_mvw.year_week,
    ph_home_timetracking_mvw.uid,
    ph_home_timetracking_mvw.current_tt,
    ph_home_timetracking_mvw.current_ptt,
    ph_home_timetracking_mvw.prev_week_tt,
    ph_home_timetracking_mvw.prev_week_ptt
   FROM redshift.ph_home_timetracking_mvw;

COMMIT;