DROP VIEW IF EXISTS redshift.ph_reported_osa CASCADE;

CREATE OR REPLACE VIEW redshift.ph_reported_osa
(
  uid,
  account_id,
  year_week,
  all_osa,
  reported_osa
)
AS 
 SELECT ph_reported_osa_mvw.uid,
    ph_reported_osa_mvw.account_id,
    ph_reported_osa_mvw.year_week,
    ph_reported_osa_mvw.all_osa,
    ph_reported_osa_mvw.reported_osa
   FROM redshift.ph_reported_osa_mvw;

COMMIT;