DROP VIEW IF EXISTS redshift.ph_timetracking_per_chain CASCADE;

CREATE OR REPLACE VIEW redshift.ph_timetracking_per_chain
(
  uid,
  account_id,
  year_week,
  chainstore,
  tt,
  ptt
)
AS 
 SELECT ph_timetracking_per_chain_mvw.uid,
    ph_timetracking_per_chain_mvw.account_id,
    ph_timetracking_per_chain_mvw.year_week,
    ph_timetracking_per_chain_mvw.chainstore,
    ph_timetracking_per_chain_mvw.tt,
    ph_timetracking_per_chain_mvw.ptt
   FROM redshift.ph_timetracking_per_chain_mvw;

COMMIT;