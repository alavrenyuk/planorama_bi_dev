DROP VIEW redshift.ph_monitor_visit_tt_ca;

CREATE OR REPLACE VIEW redshift.ph_monitor_visit_tt_ca
 AS
 SELECT ph_monitor_visit_tt_ca_mvw.account_id,
    ph_monitor_visit_tt_ca_mvw.uid,
    ph_monitor_visit_tt_ca_mvw.avg_tt_live,
    ph_monitor_visit_tt_ca_mvw.avg_tt_final,
    ph_monitor_visit_tt_ca_mvw.avg_ptt,
    ph_monitor_visit_tt_ca_mvw.late_live_visits,
    ph_monitor_visit_tt_ca_mvw.late_final_visits,
    ph_monitor_visit_tt_ca_mvw.avg_connection_time,
    ph_monitor_visit_tt_ca_mvw.avg_upload_time,
    ph_monitor_visit_tt_ca_mvw.avg_capture_time,
    ph_monitor_visit_tt_ca_mvw.visits_total,
    ph_monitor_visit_tt_ca_mvw.visits_above_sla_live,
    ph_monitor_visit_tt_ca_mvw.visits_above_sla_final
   FROM redshift.ph_monitor_visit_tt_ca_mvw;

COMMIT;