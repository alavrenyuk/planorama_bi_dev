DROP VIEW IF EXISTS redshift.ph_userfeedback_per_ean_result CASCADE;

CREATE OR REPLACE VIEW redshift.ph_userfeedback_per_ean_result
(
  uid,
  account_id,
  year_week,
  categ_name,
  product_ean,
  sum_of_reports
)
AS 
 SELECT ph_userfeedback_per_ean_result_mvw.uid,
    ph_userfeedback_per_ean_result_mvw.account_id,
    ph_userfeedback_per_ean_result_mvw.year_week,
    ph_userfeedback_per_ean_result_mvw.categ_name,
    ph_userfeedback_per_ean_result_mvw.product_ean,
    ph_userfeedback_per_ean_result_mvw.sum_of_reports
   FROM redshift.ph_userfeedback_per_ean_result_mvw;

GRANT ALL ON TABLE redshift.ph_userfeedback_per_ean_result  TO powerbi_user1;
GRANT ALL ON TABLE redshift.ph_userfeedback_per_ean_result  TO planoexport;

COMMIT;