DROP VIEW IF EXISTS redshift.ph_volume CASCADE;

CREATE OR REPLACE VIEW redshift.ph_volume
(
  uid,
  account_id,
  year_week,
  number_of_visits,
  number_of_pictures,
  avg_photos_per_visit
)
AS 
 SELECT ph_volume_mvw.uid,
    ph_volume_mvw.account_id,
    ph_volume_mvw.year_week,
    ph_volume_mvw.number_of_visits,
    ph_volume_mvw.number_of_pictures,
    ph_volume_mvw.avg_photos_per_visit
   FROM redshift.ph_volume_mvw;

COMMIT;