DROP VIEW IF EXISTS redshift.ph_userfeedback;

CREATE OR REPLACE VIEW redshift.ph_userfeedback
 AS
 SELECT ph_userfeedback_mvw.uid,
    ph_userfeedback_mvw.account_id,
    ph_userfeedback_mvw.year_week,
    ph_userfeedback_mvw.number_of_reported_visits,
    ph_userfeedback_mvw.number_of_visits,
    ph_userfeedback_mvw.ratio_of_reported_visits
   FROM redshift.ph_userfeedback_mvw;

ALTER TABLE redshift.ph_userfeedback
    OWNER TO planoexport;

GRANT SELECT ON TABLE redshift.ph_userfeedback TO powerbi_user1;
GRANT ALL ON TABLE redshift.ph_userfeedback TO planoexport;

COMMIT;