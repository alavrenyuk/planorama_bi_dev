DROP VIEW redshift.ph_monitor_visit_ca_list;

CREATE OR REPLACE VIEW redshift.ph_monitor_visit_ca_list
 AS
 SELECT ph_monitor_visit_ca_list_mvw.account_id,
    ph_monitor_visit_ca_list_mvw.account_name
   FROM redshift.ph_monitor_visit_ca_list_mvw;

COMMIT;