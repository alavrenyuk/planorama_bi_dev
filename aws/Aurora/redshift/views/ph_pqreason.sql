DROP VIEW IF EXISTS redshift.ph_pqreason CASCADE;

CREATE OR REPLACE VIEW redshift.ph_pqreason
(
  uid,
  account_id,
  year_week,
  photo_quality_reason,
  number_of_photos
)
AS 
 SELECT ph_pqreason_mvw.uid,
    ph_pqreason_mvw.account_id,
    ph_pqreason_mvw.year_week,
    ph_pqreason_mvw.photo_quality_reason,
    ph_pqreason_mvw.number_of_photos
   FROM redshift.ph_pqreason_mvw;

COMMIT;