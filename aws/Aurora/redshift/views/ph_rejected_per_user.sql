DROP VIEW IF EXISTS redshift.ph_rejected_per_user CASCADE;

CREATE OR REPLACE VIEW redshift.ph_rejected_per_user
(
  uid,
  account_id,
  username,
  categ_name,
  chainstore,
  description,
  city,
  address,
  visit_date,
  year_week,
  visit_url,
  photo_url,
  photo_index,
  photo_quality,
  photo_quality_reason,
  number_of_photos
)
AS 
 SELECT ph_rejected_per_user_mvw.uid,
    ph_rejected_per_user_mvw.account_id,
    ph_rejected_per_user_mvw.username,
    ph_rejected_per_user_mvw.categ_name,
    ph_rejected_per_user_mvw.chainstore,
    ph_rejected_per_user_mvw.description,
    ph_rejected_per_user_mvw.city,
    ph_rejected_per_user_mvw.address,
    ph_rejected_per_user_mvw.visit_date,
    ph_rejected_per_user_mvw.year_week,
    ph_rejected_per_user_mvw.visit_url,
    ph_rejected_per_user_mvw.photo_url,
    ph_rejected_per_user_mvw.photo_index,
    ph_rejected_per_user_mvw.photo_quality,
    ph_rejected_per_user_mvw.photo_quality_reason,
    ph_rejected_per_user_mvw.number_of_photos
   FROM redshift.ph_rejected_per_user_mvw;

COMMIT;