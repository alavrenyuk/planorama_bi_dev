DROP VIEW redshift.ph_home_volume;

CREATE OR REPLACE VIEW redshift.ph_home_volume
 AS
 SELECT ph_home_volume_mvw.account_id,
    ph_home_volume_mvw.year,
    ph_home_volume_mvw.week_no,
    ph_home_volume_mvw.year_week,
    ph_home_volume_mvw.uid,
    ph_home_volume_mvw.number_of_pictures,
    ph_home_volume_mvw.prev_week_photos,
    ph_home_volume_mvw.number_of_visits,
    ph_home_volume_mvw.prev_week_visits,
    ph_home_volume_mvw.change_ratio_photo,
    ph_home_volume_mvw.change_ratio_visit,
    ph_home_volume_mvw.pic_volume_flagged,
    ph_home_volume_mvw.visit_volume_flagged,
    ph_home_volume_mvw.pic_change_direction,
    ph_home_volume_mvw.visit_change_direction
   FROM redshift.ph_home_volume_mvw;

ALTER TABLE redshift.ph_home_volume OWNER TO planoexport;

GRANT SELECT ON TABLE redshift.ph_home_volume TO powerbi_user1;
GRANT ALL ON TABLE redshift.ph_home_volume TO planoexport;

COMMIT;