DROP VIEW IF EXISTS redshift.ph_geolocation CASCADE;

CREATE OR REPLACE VIEW redshift.ph_geolocation
(
  account_id,
  visit_id,
  visit_date,
  owner_id,
  store_code,
  number_of_pictures,
  photo_index,
  store_latitude,
  store_longitude,
  photo_latitude,
  photo_longitude,
  location_accuracy,
  suspicious_photo,
  photo_distance_tolerance,
  location_difference,
  uid
)
AS 
 SELECT ph_geolocation_mvw.account_id,
    ph_geolocation_mvw.visit_id,
    ph_geolocation_mvw.visit_date,
    ph_geolocation_mvw.owner_id,
    ph_geolocation_mvw.store_code,
    ph_geolocation_mvw.number_of_pictures,
    ph_geolocation_mvw.photo_index,
    ph_geolocation_mvw.store_latitude,
    ph_geolocation_mvw.store_longitude,
    ph_geolocation_mvw.photo_latitude,
    ph_geolocation_mvw.photo_longitude,
    ph_geolocation_mvw.location_accuracy,
    ph_geolocation_mvw.suspicious_photo,
    ph_geolocation_mvw.photo_distance_tolerance,
    ph_geolocation_mvw.location_difference,
    ph_geolocation_mvw.uid
   FROM redshift.ph_geolocation_mvw;

COMMIT;