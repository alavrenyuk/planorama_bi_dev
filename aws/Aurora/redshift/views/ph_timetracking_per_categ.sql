DROP VIEW IF EXISTS redshift.ph_timetracking_per_categ CASCADE;

CREATE OR REPLACE VIEW redshift.ph_timetracking_per_categ
(
  uid,
  account_id,
  year_week,
  categ_name,
  tt,
  ptt
)
AS 
 SELECT ph_timetracking_per_categ_mvw.uid,
    ph_timetracking_per_categ_mvw.account_id,
    ph_timetracking_per_categ_mvw.year_week,
    ph_timetracking_per_categ_mvw.categ_name,
    ph_timetracking_per_categ_mvw.tt,
    ph_timetracking_per_categ_mvw.ptt
   FROM redshift.ph_timetracking_per_categ_mvw;

COMMIT;