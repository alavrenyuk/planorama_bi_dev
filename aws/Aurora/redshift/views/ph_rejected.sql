DROP VIEW IF EXISTS redshift.ph_rejected CASCADE;

CREATE OR REPLACE VIEW redshift.ph_rejected
(
  uid,
  account_id,
  year_week,
  number_of_pictures,
  is_not_analyzable,
  is_analyzable,
  is_good,
  ratio_of_not_analyzable
)
AS 
 SELECT ph_rejected_mvw.uid,
    ph_rejected_mvw.account_id,
    ph_rejected_mvw.year_week,
    ph_rejected_mvw.number_of_pictures,
    ph_rejected_mvw.is_not_analyzable,
    ph_rejected_mvw.is_analyzable,
    ph_rejected_mvw.is_good,
    ph_rejected_mvw.ratio_of_not_analyzable
   FROM redshift.ph_rejected_mvw;

COMMIT;