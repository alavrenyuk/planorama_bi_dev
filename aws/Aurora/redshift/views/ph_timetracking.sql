DROP VIEW IF EXISTS redshift.ph_timetracking CASCADE;

CREATE OR REPLACE VIEW redshift.ph_timetracking
(
  uid,
  account_id,
  year_week,
  tt,
  ptt
)
AS 
 SELECT ph_timetracking_mvw.uid,
    ph_timetracking_mvw.account_id,
    ph_timetracking_mvw.year_week,
    ph_timetracking_mvw.tt,
    ph_timetracking_mvw.ptt
   FROM redshift.ph_timetracking_mvw;

COMMIT;