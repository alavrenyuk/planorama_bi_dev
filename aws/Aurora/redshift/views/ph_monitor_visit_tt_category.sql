DROP VIEW redshift.ph_monitor_visit_tt_category;

CREATE OR REPLACE VIEW redshift.ph_monitor_visit_tt_category
 AS
 SELECT ph_monitor_visit_tt_category_mvw.account_id,
    ph_monitor_visit_tt_category_mvw.uid,
    ph_monitor_visit_tt_category_mvw.base_name,
    ph_monitor_visit_tt_category_mvw.base_id,
    ph_monitor_visit_tt_category_mvw.avg_tt_live,
    ph_monitor_visit_tt_category_mvw.avg_tt_final,
    ph_monitor_visit_tt_category_mvw.avg_ptt,
    ph_monitor_visit_tt_category_mvw.late_live_visits,
    ph_monitor_visit_tt_category_mvw.late_final_visits,
    ph_monitor_visit_tt_category_mvw.avg_connection_time,
    ph_monitor_visit_tt_category_mvw.avg_upload_time,
    ph_monitor_visit_tt_category_mvw.avg_capture_time,
    ph_monitor_visit_tt_category_mvw.visits_total,
    ph_monitor_visit_tt_category_mvw.visits_above_sla_live,
    ph_monitor_visit_tt_category_mvw.visits_above_sla_final
   FROM redshift.ph_monitor_visit_tt_category_mvw;

COMMIT;