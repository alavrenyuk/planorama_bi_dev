DROP VIEW redshift.ph_home_rejected CASCADE;

CREATE OR REPLACE VIEW redshift.ph_home_rejected
 AS
 SELECT ph_home_rejected_mvw.account_id,
    ph_home_rejected_mvw.year,
    ph_home_rejected_mvw.week_no,
    ph_home_rejected_mvw.year_week,
    ph_home_rejected_mvw.uid,
    ph_home_rejected_mvw.number_of_pictures,
    ph_home_rejected_mvw.is_not_analyzable,
    ph_home_rejected_mvw.prev_week_photos,
    ph_home_rejected_mvw.prev_week_rejected,
    ph_home_rejected_mvw.ratio_rejected_crnt,
    ph_home_rejected_mvw.ratio_rejected_prev,
    ph_home_rejected_mvw.rejected_change_dir,
    ph_home_rejected_mvw.rejected_flag
   FROM redshift.ph_home_rejected_mvw;

ALTER TABLE redshift.ph_home_rejected
    OWNER TO planoexport;

GRANT ALL ON TABLE redshift.ph_home_rejected TO powerbi_user1;
GRANT ALL ON TABLE redshift.ph_home_rejected TO planoexport;

COMMIT;