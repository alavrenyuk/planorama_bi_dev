DROP VIEW IF EXISTS redshift.ph_pqoverview CASCADE;

CREATE OR REPLACE VIEW redshift.ph_pqoverview
(
  uid,
  account_id,
  year_week,
  photo_quality,
  is_not_analyzable,
  is_analyzable,
  is_good,
  number_of_photos
)
AS 
 SELECT ph_pqoverview_mvw.uid,
    ph_pqoverview_mvw.account_id,
    ph_pqoverview_mvw.year_week,
    ph_pqoverview_mvw.photo_quality,
    ph_pqoverview_mvw.is_not_analyzable,
    ph_pqoverview_mvw.is_analyzable,
    ph_pqoverview_mvw.is_good,
    ph_pqoverview_mvw.number_of_photos
   FROM redshift.ph_pqoverview_mvw;

COMMIT;