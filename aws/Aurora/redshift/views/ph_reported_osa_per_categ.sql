DROP VIEW IF EXISTS redshift.ph_reported_osa_per_categ CASCADE;

CREATE OR REPLACE VIEW redshift.ph_reported_osa_per_categ
(
  uid,
  account_id,
  year_week,
  categ_name,
  all_osa,
  reported_osa
)
AS 
 SELECT ph_reported_osa_per_categ_mvw.uid,
    ph_reported_osa_per_categ_mvw.account_id,
    ph_reported_osa_per_categ_mvw.year_week,
    ph_reported_osa_per_categ_mvw.categ_name,
    ph_reported_osa_per_categ_mvw.all_osa,
    ph_reported_osa_per_categ_mvw.reported_osa
   FROM redshift.ph_reported_osa_per_categ_mvw;

COMMIT;