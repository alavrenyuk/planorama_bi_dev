DROP VIEW IF EXISTS redshift.ph_accuracy_export CASCADE;

CREATE OR REPLACE VIEW redshift.ph_accuracy_export
(
  uid,
  account_id,
  year_week,
  visit_id,
  category,
  product_ean,
  number_of_reported_osa
)
AS 
 SELECT ph_accuracy_export_mvw.uid,
    ph_accuracy_export_mvw.account_id,
    ph_accuracy_export_mvw.year_week,
    ph_accuracy_export_mvw.visit_id,
    ph_accuracy_export_mvw.category,
    ph_accuracy_export_mvw.product_ean,
    ph_accuracy_export_mvw.number_of_reported_osa
   FROM redshift.ph_accuracy_export_mvw;

COMMIT;