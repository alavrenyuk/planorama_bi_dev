DROP VIEW IF EXISTS redshift.ph_volume_per_categ CASCADE;

CREATE OR REPLACE VIEW redshift.ph_volume_per_categ
(
  uid,
  account_id,
  year_week,
  categ_name,
  number_of_visits,
  number_of_pictures,
  avg_photos_per_visit
)
AS 
 SELECT ph_volume_per_categ_mvw.uid,
    ph_volume_per_categ_mvw.account_id,
    ph_volume_per_categ_mvw.year_week,
    ph_volume_per_categ_mvw.categ_name,
    ph_volume_per_categ_mvw.number_of_visits,
    ph_volume_per_categ_mvw.number_of_pictures,
    ph_volume_per_categ_mvw.avg_photos_per_visit
   FROM redshift.ph_volume_per_categ_mvw;

COMMIT;