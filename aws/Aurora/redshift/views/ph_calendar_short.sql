DROP VIEW IF EXISTS redshift.ph_calendar_short CASCADE;

CREATE OR REPLACE VIEW redshift.ph_calendar_short
(
  account_id,
  week,
  week_no,
  year_week,
  uid,
  mindate,
  maxdate,
  week_of
)
AS 
 SELECT ph_calendar_short_mvw.account_id,
    ph_calendar_short_mvw.week,
    ph_calendar_short_mvw.week_no,
    ph_calendar_short_mvw.year_week,
    ph_calendar_short_mvw.uid,
    ph_calendar_short_mvw.mindate,
    ph_calendar_short_mvw.maxdate,
    ph_calendar_short_mvw.week_of::character varying(50) AS week_of
   FROM redshift.ph_calendar_short_mvw;

COMMIT;