DROP VIEW redshift.ph_calendar_extended;

CREATE OR REPLACE VIEW redshift.ph_calendar_extended
 AS
 SELECT ph_calendar_extended_mvw.year,
    ph_calendar_extended_mvw.month_no,
    ph_calendar_extended_mvw.month_name,
    ph_calendar_extended_mvw.week_no,
    ph_calendar_extended_mvw.week,
    ph_calendar_extended_mvw.start_of_week,
    ph_calendar_extended_mvw.year_month,
    ph_calendar_extended_mvw.year_week,
    ph_calendar_extended_mvw.week_of,
    ph_calendar_extended_mvw.exclude_crnt_week
   FROM redshift.ph_calendar_extended_mvw;

COMMIT;