DROP VIEW IF EXISTS redshift.ph_allphoto_per_user CASCADE;

CREATE OR REPLACE VIEW redshift.ph_allphoto_per_user
(
  uid,
  account_id,
  username,
  categ_name,
  chainstore,
  description,
  city,
  address,
  visit_date,
  year_week,
  visit_url,
  photo_url,
  photo_index,
  photo_quality,
  photo_quality_reason,
  number_of_photos,
  is_not_analyzable,
  is_analyzable,
  is_good
)
AS 
 SELECT ph_allphoto_per_user_mvw.uid,
    ph_allphoto_per_user_mvw.account_id,
    ph_allphoto_per_user_mvw.username,
    ph_allphoto_per_user_mvw.categ_name,
    ph_allphoto_per_user_mvw.chainstore,
    ph_allphoto_per_user_mvw.description,
    ph_allphoto_per_user_mvw.city,
    ph_allphoto_per_user_mvw.address,
    ph_allphoto_per_user_mvw.visit_date,
    ph_allphoto_per_user_mvw.year_week,
    ph_allphoto_per_user_mvw.visit_url,
    ph_allphoto_per_user_mvw.photo_url,
    ph_allphoto_per_user_mvw.photo_index,
    ph_allphoto_per_user_mvw.photo_quality,
    ph_allphoto_per_user_mvw.photo_quality_reason,
    ph_allphoto_per_user_mvw.number_of_photos,
    ph_allphoto_per_user_mvw.is_not_analyzable,
    ph_allphoto_per_user_mvw.is_analyzable,
    ph_allphoto_per_user_mvw.is_good
   FROM redshift.ph_allphoto_per_user_mvw;

COMMIT;