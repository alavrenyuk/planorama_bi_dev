DROP VIEW redshift.ph_monitor_visit_connection;

CREATE OR REPLACE VIEW redshift.ph_monitor_visit_connection
 AS
 SELECT ph_monitor_visit_connection_mvw.account_id,
    ph_monitor_visit_connection_mvw.store_id,
    ph_monitor_visit_connection_mvw.uid,
    ph_monitor_visit_connection_mvw.owner_id,
    ph_monitor_visit_connection_mvw.avg_connection_time,
    ph_monitor_visit_connection_mvw.avg_upload_time,
    ph_monitor_visit_connection_mvw.avg_capture_time,
    ph_monitor_visit_connection_mvw.visits_total
   FROM redshift.ph_monitor_visit_connection_mvw;

COMMIT;