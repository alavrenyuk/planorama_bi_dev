DROP VIEW IF EXISTS redshift.ph_accuracy_per_categ CASCADE;

CREATE OR REPLACE VIEW redshift.ph_accuracy_per_categ
(
  uid,
  account_id,
  year_week,
  category,
  audit_total_facing,
  audit_total_facing_correct,
  overall_accuracy,
  osa_total_facing,
  osa_correct_facing,
  osa_accuracy,
  number_of_reported_osa,
  number_of_osa,
  ratio_of_reported_osa
)
AS 
 SELECT ph_accuracy_per_categ_mvw.uid,
    ph_accuracy_per_categ_mvw.account_id,
    ph_accuracy_per_categ_mvw.year_week,
    ph_accuracy_per_categ_mvw.category,
    ph_accuracy_per_categ_mvw.audit_total_facing,
    ph_accuracy_per_categ_mvw.audit_total_facing_correct,
    ph_accuracy_per_categ_mvw.overall_accuracy,
    ph_accuracy_per_categ_mvw.osa_total_facing,
    ph_accuracy_per_categ_mvw.osa_correct_facing,
    ph_accuracy_per_categ_mvw.osa_accuracy,
    ph_accuracy_per_categ_mvw.number_of_reported_osa,
    ph_accuracy_per_categ_mvw.number_of_osa,
    ph_accuracy_per_categ_mvw.ratio_of_reported_osa
   FROM redshift.ph_accuracy_per_categ_mvw;

COMMIT;