DROP VIEW IF EXISTS redshift.ph_userfeedback_per_categ;

CREATE OR REPLACE VIEW redshift.ph_userfeedback_per_categ
 AS
 SELECT ph_userfeedback_per_categ_mvw.uid,
    ph_userfeedback_per_categ_mvw.account_id,
    ph_userfeedback_per_categ_mvw.year_week,
    ph_userfeedback_per_categ_mvw.categ_name,
    ph_userfeedback_per_categ_mvw.number_of_reported_visits,
    ph_userfeedback_per_categ_mvw.number_of_visits,
    ph_userfeedback_per_categ_mvw.ratio_of_reported_visits
   FROM redshift.ph_userfeedback_per_categ_mvw;

ALTER TABLE redshift.ph_userfeedback_per_categ
    OWNER TO planoexport;

GRANT SELECT ON TABLE redshift.ph_userfeedback_per_categ TO powerbi_user1;
GRANT ALL ON TABLE redshift.ph_userfeedback_per_categ TO planoexport;

COMMIT;