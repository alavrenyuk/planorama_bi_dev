DROP MATERIALIZED VIEW redshift.ph_monitor_visit_connection_mvw;

CREATE MATERIALIZED VIEW redshift.ph_monitor_visit_connection_mvw
TABLESPACE pg_default
AS
 SELECT x.account_id,
    x.store_id,
    x.uid,
    x.owner_id,
    x.avg_connection_time,
    x.avg_upload_time,
    x.avg_capture_time,
    x.visits_total
   FROM dblink('foreign_server', $REDSHIFT$
  SELECT 
  account_id,
  store_id,
  uid,
  owner_id,
  avg_connection_time,
  avg_upload_time,
  avg_capture_time,
  visits_total
  FROM consolidated.ph_monitor_visit_connection; $REDSHIFT$) AS x
  (account_id character varying(255), 
  store_id character varying(255),
  uid character varying(255), 
  owner_id character varying(255), 
  avg_connection_time double precision, 
  avg_upload_time double precision, 
  avg_capture_time double precision, 
  visits_total integer);

  COMMIT;