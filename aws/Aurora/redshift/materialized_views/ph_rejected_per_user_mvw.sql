DROP MATERIALIZED VIEW IF EXISTS redshift.ph_rejected_per_user_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_rejected_per_user_mvw
(
  uid,
  account_id,
  username,
  categ_name,
  chainstore,
  description,
  city,
  address,
  visit_date,
  year_week,
  visit_url,
  photo_url,
  photo_index,
  photo_quality,
  photo_quality_reason,
  number_of_photos
)
AS 
 SELECT t1.uid,
    t1.account_id,
    t1.username,
    t1.categ_name,
    t1.chainstore,
    t1.description,
    t1.city,
    t1.address,
    t1.visit_date,
    t1.year_week,
    t1.visit_url,
    t1.photo_url,
    t1.photo_index,
    t1.photo_quality,
    t1.photo_quality_reason,
    t1.number_of_photos
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       username,
       categ_name,
       chainstore,
       description,
       city,
       address,
       visit_date,
       year_week,
       visit_url,
       photo_url,
       photo_index,
       photo_quality,
       photo_quality_reason,
       number_of_photos
FROM consolidated.ph_rejected_per_user;
$REDSHIFT$) t1(uid text, account_id character varying(50), username character varying(50), categ_name character varying(255), chainstore character varying(50), description character varying(255), city character varying(50), address character varying(255), visit_date date, year_week character varying(12), visit_url text, photo_url character varying(2083), photo_index real, photo_quality character varying(255), photo_quality_reason character varying(255), number_of_photos bigint);

CREATE INDEX IF NOT EXISTS ph_rejected_per_user_idx 
ON redshift.ph_rejected_per_user_mvw (account_id, year_week, uid);

COMMIT;