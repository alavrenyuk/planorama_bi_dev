DROP MATERIALIZED VIEW redshift.ph_home_volume_mvw;

CREATE MATERIALIZED VIEW redshift.ph_home_volume_mvw
TABLESPACE pg_default
AS
 SELECT x.account_id,
    x.year,
    x.week_no,
    x.year_week,
    x.uid,
    x.number_of_pictures,
    x.prev_week_photos,
    x.number_of_visits,
    x.prev_week_visits,
    x.change_ratio_photo,
    x.change_ratio_visit,
    x.pic_volume_flagged,
    x.visit_volume_flagged,
    x.pic_change_direction,
    x.visit_change_direction
   FROM dblink('foreign_server'::text, $REDSHIFT$
  SELECT 
    account_id,
    year,
	  week_no,
    year_week,
    uid,
    number_of_pictures,
    prev_week_photos,
    number_of_visits,
    prev_week_visits,
    change_ratio_photo,
    change_ratio_visit,
    pic_volume_flagged,
    visit_volume_flagged,
    pic_change_direction,
    visit_change_direction
  FROM consolidated.ph_home_volume; $REDSHIFT$) AS x 
  (account_id character varying(50), 
  year integer, 
  week_no integer, 
  year_week character varying(12), 
  uid character varying(255), 
  number_of_pictures bigint, 
  prev_week_photos bigint, 
  number_of_visits bigint, 
  prev_week_visits bigint, 
  change_ratio_photo double precision, 
  change_ratio_visit double precision, 
  pic_volume_flagged integer, 
  visit_volume_flagged integer, 
  pic_change_direction integer, 
  visit_change_direction integer)
WITH DATA;

ALTER TABLE redshift.ph_home_volume_mvw OWNER TO planoexport;