DROP MATERIALIZED VIEW redshift.ph_userfeedback_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_userfeedback_mvw
TABLESPACE pg_default
AS
 SELECT x.uid,
    x.account_id,
    x.year_week,
    x.number_of_reported_visits,
    x.number_of_visits,
    x.ratio_of_reported_visits
   FROM dblink('foreign_server'::text, $REDSHIFT$
  SELECT 
	uid,
	account_id,
	year_week,
	number_of_reported_visits,
	number_of_visits,
	ratio_of_reported_visits
  FROM consolidated.ph_userfeedback; $REDSHIFT$) 
  as x
  (uid character varying(255), 
  account_id character varying(255), 
  year_week character varying(255), 
  number_of_reported_visits integer, 
  number_of_visits integer, 
  ratio_of_reported_visits double precision)
WITH DATA;

ALTER TABLE redshift.ph_userfeedback_mvw OWNER TO planoexport;

CREATE INDEX IF NOT EXISTS ph_userfeedback_idx 
ON redshift.ph_userfeedback_mvw (uid, account_id, year_week);

COMMIT;