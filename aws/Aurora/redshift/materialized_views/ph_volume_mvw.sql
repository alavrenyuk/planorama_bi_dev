DROP MATERIALIZED VIEW IF EXISTS redshift.ph_volume_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_volume_mvw
(
  uid,
  account_id,
  year_week,
  number_of_visits,
  number_of_pictures,
  avg_photos_per_visit
)
AS 
 SELECT t1.uid,
    t1.account_id,
    t1.year_week,
    t1.number_of_visits,
    t1.number_of_pictures,
    t1.avg_photos_per_visit
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       year_week,
       number_of_visits,
       number_of_pictures,
       avg_photos_per_visit
FROM consolidated.ph_volume;
$REDSHIFT$) AS t1
(uid text, account_id character varying(50), year_week character varying(12), number_of_visits bigint, number_of_pictures double precision, avg_photos_per_visit double precision);

CREATE INDEX IF NOT EXISTS ph_volume_idx 
ON redshift.ph_volume_mvw (account_id, year_week, uid);

COMMIT;