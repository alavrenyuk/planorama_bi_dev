DROP MATERIALIZED VIEW IF EXISTS redshift.ph_volume_per_categ_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_volume_per_categ_mvw
(
  uid,
  account_id,
  year_week,
  categ_name,
  number_of_visits,
  number_of_pictures,
  avg_photos_per_visit
)
AS 
 SELECT t1.uid,
    t1.account_id,
    t1.year_week,
    t1.categ_name,
    t1.number_of_visits,
    t1.number_of_pictures,
    t1.avg_photos_per_visit
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       year_week,
       categ_name,
       number_of_visits,
       number_of_pictures,
       avg_photos_per_visit
FROM consolidated.ph_volume_per_categ;
$REDSHIFT$) AS t1
(uid text, account_id character varying(50), year_week character varying(12), categ_name character varying(255), number_of_visits bigint, number_of_pictures double precision, avg_photos_per_visit double precision);

CREATE INDEX IF NOT EXISTS ph_volume_per_categ_idx 
ON redshift.ph_volume_per_categ_mvw (account_id, year_week, uid, categ_name);

COMMIT;