DROP MATERIALIZED VIEW IF EXISTS redshift.ph_calendar_short_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_calendar_short_mvw
(
  account_id,
  week,
  week_no,
  year_week,
  uid,
  mindate,
  maxdate,
  week_of
)
AS 
 SELECT t1.account_id,
    t1.week,
    t1.week_no,
    t1.year_week,
    t1.uid,
    t1.mindate,
    t1.maxdate,
    t1.week_of
   FROM dblink('foreign_server', $REDSHIFT$
SELECT account_id,
       week,
       week_no,
       year_week,
       uid,
       mindate,
       maxdate,
       week_of
FROM consolidated.ph_calendar_short;
$REDSHIFT$) AS t1
        (account_id character varying(50), 
        week character varying(16), 
        week_no integer, 
        year_week character varying(12), 
        uid text, mindate date, 
        maxdate date, 
        week_of text);

CREATE INDEX IF NOT EXISTS ph_calendar_short_idx 
ON redshift.ph_calendar_short_mvw (account_id, year_week, uid);

COMMIT;