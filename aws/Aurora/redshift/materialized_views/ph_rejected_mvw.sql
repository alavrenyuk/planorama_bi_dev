DROP MATERIALIZED VIEW IF EXISTS redshift.ph_rejected_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_rejected_mvw
(
  uid,
  account_id,
  year_week,
  number_of_pictures,
  is_not_analyzable,
  is_analyzable,
  is_good,
  ratio_of_not_analyzable
)
AS 
 SELECT t1.uid,
    t1.account_id,
    t1.year_week,
    t1.number_of_pictures,
    t1.is_not_analyzable,
    t1.is_analyzable,
    t1.is_good,
    t1.ratio_of_not_analyzable
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       year_week,
       number_of_pictures,
       is_not_analyzable,
       is_analyzable,
       is_good,
       ratio_of_not_analyzable
FROM consolidated.ph_rejected;
$REDSHIFT$) AS t1
(uid text, account_id character varying(50), year_week character varying(12), number_of_pictures bigint, is_not_analyzable bigint, is_analyzable bigint, is_good bigint, ratio_of_not_analyzable numeric);

CREATE INDEX IF NOT EXISTS ph_rejected_idx 
ON redshift.ph_rejected_mvw (account_id, year_week, uid);

COMMIT;