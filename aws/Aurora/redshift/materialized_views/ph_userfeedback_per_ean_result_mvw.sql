DROP MATERIALIZED VIEW IF EXISTS redshift.ph_userfeedback_per_ean_result_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_userfeedback_per_ean_result_mvw
(
  uid,
  account_id,
  year_week,
  categ_name,
  product_ean,
  sum_of_reports
)
AS 
 SELECT t1.uid,
    t1.account_id,
    t1.year_week,
    t1.categ_name,
    t1.product_ean,
    t1.sum_of_reports
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       year_week,
       categ_name,
       product_ean,
       sum_of_reports
FROM consolidated.ph_userfeedback_per_ean_result;
$REDSHIFT$) AS t1
(uid text, account_id character varying(50), year_week character varying(12), categ_name character varying(255), product_ean character varying(500), sum_of_reports bigint);

CREATE INDEX IF NOT EXISTS ph_userfeedback_per_ean_result_idx 
ON redshift.ph_userfeedback_per_ean_result_mvw (account_id, year_week, uid, categ_name);

COMMIT;