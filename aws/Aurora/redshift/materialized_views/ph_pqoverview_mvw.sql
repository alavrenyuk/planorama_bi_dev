DROP MATERIALIZED VIEW IF EXISTS redshift.ph_pqoverview_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_pqoverview_mvw
(
  uid,
  account_id,
  year_week,
  photo_quality,
  is_not_analyzable,
  is_analyzable,
  is_good,
  number_of_photos
)
AS 
 SELECT t1.uid,
    t1.account_id,
    t1.year_week,
    t1.photo_quality,
    t1.is_not_analyzable,
    t1.is_analyzable,
    t1.is_good,
    t1.number_of_photos
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       year_week,
       photo_quality,
       is_not_analyzable,
       is_analyzable,
       is_good,
       number_of_photos
FROM consolidated.ph_pqoverview;
$REDSHIFT$) AS t1
(uid text, account_id character varying(50), year_week character varying(12), photo_quality character varying(255), is_not_analyzable bigint, is_analyzable bigint, is_good bigint, number_of_photos bigint);

CREATE INDEX IF NOT EXISTS ph_pqoverview_idx 
ON redshift.ph_pqoverview_mvw (account_id, year_week, uid);

COMMIT;