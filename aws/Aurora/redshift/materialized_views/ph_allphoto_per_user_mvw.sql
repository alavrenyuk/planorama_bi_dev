DROP MATERIALIZED VIEW IF EXISTS redshift.ph_allphoto_per_user_mvw;

CREATE MATERIALIZED VIEW redshift.ph_allphoto_per_user_mvw
AS
 SELECT t1.uid,
    t1.account_id,
    t1.username,
    t1.categ_name,
    t1.chainstore,
    t1.description,
    t1.city,
    t1.address,
    t1.visit_date,
    t1.year_week,
    t1.visit_url,
    t1.photo_url,
    t1.photo_index,
    t1.photo_quality,
    t1.photo_quality_reason,
    t1.number_of_photos,
    t1.is_not_analyzable,
    t1.is_analyzable,
    t1.is_good
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       username,
       categ_name,
       chainstore,
       description,
       city,
       address,
       visit_date,
       year_week,
       visit_url,
       photo_url,
       photo_index,
       photo_quality,
       photo_quality_reason,
       number_of_photos,
       is_not_analyzable,
       is_analyzable,
       is_good
FROM consolidated.ph_allphoto_per_user;
$REDSHIFT$) AS t1
        (uid text, 
        account_id character varying(50), 
        username character varying(50), 
        categ_name character varying(255), 
        chainstore character varying(50), 
        description character varying(255), 
        city character varying(50), 
        address character varying(255), 
        visit_date date, 
        year_week character varying(12), 
        visit_url text, 
        photo_url character varying(2083), 
        photo_index real, 
        photo_quality character varying(255), 
        photo_quality_reason character varying(255), 
        number_of_photos bigint, 
        is_not_analyzable bigint, 
        is_analyzable bigint, 
        is_good bigint)
WITH DATA;

CREATE INDEX IF NOT EXISTS ph_allphoto_per_user_idx
    ON redshift.ph_allphoto_per_user_mvw 
    (account_id, year_week, uid, username);

COMMIT;