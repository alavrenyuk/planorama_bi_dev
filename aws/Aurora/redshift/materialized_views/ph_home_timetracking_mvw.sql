DROP MATERIALIZED VIEW IF EXISTS redshift.ph_home_timetracking_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_home_timetracking_mvw
(
  account_id,
  week_no,
  year_week,
  uid,
  current_tt,
  current_ptt,
  prev_week_tt,
  prev_week_ptt
)
AS 
 SELECT t1.account_id,
    t1.week_no,
    t1.year_week,
    t1.uid,
    t1.current_tt,
    t1.current_ptt,
    t1.prev_week_tt,
    t1.prev_week_ptt
   FROM dblink('foreign_server', $REDSHIFT$
SELECT account_id,
       week_no,
       year_week,
       uid,
       current_tt,
       current_ptt,
       prev_week_tt,
       prev_week_ptt
FROM consolidated.ph_home_timetracking;
$REDSHIFT$) AS t1
(account_id character varying(50), 
week_no integer, 
year_week character varying(12), 
uid text, 
current_tt double precision, 
current_ptt double precision, 
prev_week_tt double precision, 
prev_week_ptt double precision);

CREATE INDEX IF NOT EXISTS ph_home_timetracking_idx 
ON redshift.ph_home_timetracking_mvw (account_id, year_week, uid);

COMMIT;