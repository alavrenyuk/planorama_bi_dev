DROP MATERIALIZED VIEW redshift.ph_monitor_visit_tt_category_mvw;

CREATE MATERIALIZED VIEW redshift.ph_monitor_visit_tt_category_mvw
TABLESPACE pg_default
AS
 SELECT x.account_id,
    x.uid,
    x.base_name,
    x.base_id,
    x.avg_tt_live,
    x.avg_tt_final,
    x.avg_ptt,
    x.late_live_visits,
    x.late_final_visits,
    x.avg_connection_time,
    x.avg_upload_time,
    x.avg_capture_time,
    x.visits_total,
    x.visits_above_sla_live,
    x.visits_above_sla_final
   FROM dblink('foreign_server', $REDSHIFT$
  SELECT 
	account_id,
	uid,
	base_name,
	base_id,
	avg_tt_live,
	avg_tt_final,
	avg_ptt,
	late_live_visits,
	late_final_visits,
	avg_connection_time,
	avg_upload_time,
	avg_capture_time,
	visits_total,
	visits_above_sla_live,
	visits_above_sla_final
  FROM consolidated.ph_monitor_visit_tt_category; $REDSHIFT$) AS x
  (account_id character varying(255), 
  uid character varying(255), 
  base_name character varying(255), 
  base_id character varying(255), 
  avg_tt_live double precision, 
  avg_tt_final double precision, 
  avg_ptt double precision, 
  late_live_visits integer, 
  late_final_visits integer, 
  avg_connection_time double precision, 
  avg_upload_time double precision, 
  avg_capture_time double precision, 
  visits_total integer, 
  visits_above_sla_live integer, 
  visits_above_sla_final integer);

  COMMIT;