DROP MATERIALIZED VIEW IF EXISTS redshift.ph_accuracy_mvw;

CREATE MATERIALIZED VIEW redshift.ph_accuracy_mvw
AS
 SELECT t1.uid,
    t1.account_id,
    t1.year_week,
    t1.audit_total_facing,
    t1.audit_total_facing_correct,
    t1.overall_accuracy,
    t1.osa_total_facing,
    t1.osa_correct_facing,
    t1.osa_accuracy,
    t1.number_of_reported_osa,
    t1.number_of_osa,
    t1.ratio_of_reported_osa
   FROM dblink('foreign_server', $REDSHIFT$
       SELECT uid,
       account_id,
       year_week,
       audit_total_facing,
       audit_total_facing_correct,
       overall_accuracy,
       osa_total_facing,
       osa_correct_facing,
       osa_accuracy,
       number_of_reported_osa,
       number_of_osa,
       ratio_of_reported_osa
FROM consolidated.ph_accuracy;
$REDSHIFT$) AS t1
        (uid text, 
        account_id character varying(50), 
        year_week character varying(12), 
        audit_total_facing double precision, 
        audit_total_facing_correct double precision, 
        overall_accuracy double precision, 
        osa_total_facing double precision, 
        osa_correct_facing double precision, 
        osa_accuracy double precision, 
        number_of_reported_osa bigint, 
        number_of_osa bigint, 
        ratio_of_reported_osa numeric(38,16))
WITH DATA;

CREATE INDEX ph_accuracy_idx
    ON redshift.ph_accuracy_mvw 
    (account_id, year_week, uid);