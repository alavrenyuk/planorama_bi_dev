DROP MATERIALIZED VIEW redshift.ph_userfeedback_per_categ_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_userfeedback_per_categ_mvw
TABLESPACE pg_default
AS
 SELECT x.uid,
    x.account_id,
    x.year_week,
    x.categ_name,
    x.number_of_reported_visits,
    x.number_of_visits,
    x.ratio_of_reported_visits
   FROM dblink('foreign_server', $REDSHIFT$
  SELECT 
	uid,
	account_id,
	year_week,
	categ_name,
	number_of_reported_visits,
	number_of_visits,
	ratio_of_reported_visits
  FROM consolidated.ph_userfeedback_per_categ; $REDSHIFT$)
  AS x
  (uid character varying(255), 
  account_id character varying(255), 
  year_week character varying(255), 
  categ_name character varying(255), 
  number_of_reported_visits integer, 
  number_of_visits integer, 
  ratio_of_reported_visits double precision)
WITH DATA;

CREATE INDEX IF NOT EXISTS ph_userfeedback_per_categ_idx 
ON redshift.ph_userfeedback_per_categ_mvw (uid, account_id, year_week, uid, categ_name);

ALTER TABLE redshift.ph_userfeedback_per_categ_mvw
    OWNER TO planoexport;

COMMIT;