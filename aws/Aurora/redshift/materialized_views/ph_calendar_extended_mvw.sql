DROP MATERIALIZED VIEW IF EXISTS redshift.ph_calendar_extended_mvw CASCADE;

CREATE OR REPLACE MATERIALIZED VIEW redshift.ph_calendar_extended_mvw
(
  year,
  month_no,
  month_name,
  week_no,
  week,
  start_of_week,
  year_month,
  year_week,
  week_of,
  exclude_crnt_week
)
AS 
 SELECT x.year,
    x.month_no,
    x.month_name,
    x.week_no,
    x.week,
    x.start_of_week,
    x.year_month,
    x.year_week,
    x.week_of,
    x.exclude_crnt_week
   FROM dblink('foreign_server', $REDSHIFT$
  SELECT 
  year,
  month_no,
  month_name, 
  week_no,
  week,
  start_of_week,
  year_month,
  year_week,
  week_of,
  exclude_crnt_week
  FROM consolidated.ph_calendar_extended; $REDSHIFT$) AS x
  (year integer, month_no integer, month_name character varying(50), week_no integer, week character varying(16), start_of_week date, year_month character varying(50), year_week character varying(50), week_of character varying(50), exclude_crnt_week integer);

CREATE INDEX IF NOT EXISTS ph_calendar_extended_idx 
ON redshift.ph_calendar_extended_mvw (year, month_no, week_no, year_week);

COMMIT;