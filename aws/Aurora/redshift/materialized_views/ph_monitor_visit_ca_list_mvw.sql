DROP MATERIALIZED VIEW redshift.ph_monitor_visit_ca_list_mvw;

CREATE MATERIALIZED VIEW redshift.ph_monitor_visit_ca_list_mvw
TABLESPACE pg_default
AS
 SELECT x.account_id,
    x.account_name
   FROM dblink('foreign_server', $REDSHIFT$
  SELECT 
	account_id,
	account_name
  FROM consolidated.ph_monitor_visit_ca_list; $REDSHIFT$) AS x
  (account_id character varying(255), account_name character varying(255));

  COMMIT;