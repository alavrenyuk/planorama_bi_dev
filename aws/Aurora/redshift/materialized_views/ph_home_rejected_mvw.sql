DROP MATERIALIZED VIEW redshift.ph_home_rejected_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_home_rejected_mvw
TABLESPACE pg_default
AS
 SELECT x.account_id,
    x.year,
    x.week_no,
    x.year_week,
    x.uid,
    x.number_of_pictures,
    x.is_not_analyzable,
    x.prev_week_photos,
    x.prev_week_rejected,
    x.ratio_rejected_crnt,
    x.ratio_rejected_prev,
    x.rejected_change_dir,
    x.rejected_flag
   FROM dblink('foreign_server'::text, '
  SELECT 
	account_id,
	year,
	week_no, 
	year_week,
	uid,
	number_of_pictures,
	is_not_analyzable,
	prev_week_photos,
	prev_week_rejected,
	ratio_rejected_crnt,
	ratio_rejected_prev,
	rejected_change_dir,
	rejected_flag
  FROM consolidated.ph_home_rejected; '::text) x(account_id character varying(50), year integer, week_no integer, year_week character varying(12), uid character varying(255), number_of_pictures bigint, is_not_analyzable bigint, prev_week_photos bigint, prev_week_rejected bigint, ratio_rejected_crnt double precision, ratio_rejected_prev double precision, rejected_change_dir integer, rejected_flag integer)
WITH DATA;

CREATE INDEX IF NOT EXISTS ph_home_rejected_idx 
ON redshift.ph_home_rejected_mvw (account_id, year_week, uid);

COMMIT;