DROP MATERIALIZED VIEW IF EXISTS redshift.ph_pqreason_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_pqreason_mvw
(
  uid,
  account_id,
  year_week,
  photo_quality_reason,
  number_of_photos
)
AS 
 SELECT t1.uid,
    t1.account_id,
    t1.year_week,
    t1.photo_quality_reason,
    t1.number_of_photos
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       year_week,
       photo_quality_reason,
       number_of_photos
FROM consolidated.ph_pqreason;
$REDSHIFT$) AS t1
(uid text, account_id character varying(50), year_week character varying(12), photo_quality_reason character varying(255), number_of_photos bigint);

CREATE INDEX IF NOT EXISTS ph_pqreason_idx 
ON redshift.ph_pqreason_mvw (account_id, year_week, uid);

COMMIT;