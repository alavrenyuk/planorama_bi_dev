DROP MATERIALIZED VIEW IF EXISTS redshift.ph_accuracy_export_mvw;

CREATE MATERIALIZED VIEW redshift.ph_accuracy_export_mvw
AS
 SELECT t1.uid,
    t1.account_id,
    t1.year_week,
    t1.visit_id,
    t1.category,
    t1.product_ean,
    t1.number_of_reported_osa
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       year_week,
       visit_id,
       category,
       product_ean,
       number_of_reported_osa
FROM consolidated.ph_accuracy_export;
$REDSHIFT$) AS t1 
       (uid text, 
       account_id character varying(50), 
       year_week character varying(12), 
       visit_id character varying(50), 
       category character varying(255), 
       product_ean character varying(50), 
       number_of_reported_osa bigint)
WITH DATA;

CREATE INDEX IF NOT EXISTS ph_accuracy_export_idx
ON redshift.ph_accuracy_export_mvw (account_id, year_week, uid, category);

COMMIT;