DROP MATERIALIZED VIEW IF EXISTS redshift.ph_geolocation_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_geolocation_mvw
(
  account_id,
  visit_id,
  visit_date,
  owner_id,
  store_code,
  number_of_pictures,
  photo_index,
  store_latitude,
  store_longitude,
  photo_latitude,
  photo_longitude,
  location_accuracy,
  suspicious_photo,
  photo_distance_tolerance,
  location_difference,
  uid
)
AS 
 SELECT x.account_id,
    x.visit_id,
    x.visit_date,
    x.owner_id,
    x.store_code,
    x.number_of_pictures,
    x.photo_index,
    x.store_latitude,
    x.store_longitude,
    x.photo_latitude,
    x.photo_longitude,
    x.location_accuracy,
    x.suspicious_photo,
    x.photo_distance_tolerance,
    x.location_difference,
    x.uid
   FROM dblink('foreign_server', $REDSHIFT$
  SELECT 
    account_id,
    visit_id,
    visit_date,
    owner_id,
    store_code,
    number_of_pictures,
    photo_index,
    store_latitude,
    store_longitude,
    photo_latitude,
    photo_longitude,
    location_accuracy,
    suspicious_photo,
    photo_distance_tolerance,
    location_difference,
	uid
  FROM consolidated.ph_geolocation; $REDSHIFT$) AS x 
  (account_id character varying(255), 
  visit_id character varying(255), 
  visit_date timestamp without time zone, 
  owner_id character varying(255), 
  store_code character varying(255), 
  number_of_pictures double precision, 
  photo_index double precision, 
  store_latitude double precision, 
  store_longitude double precision, 
  photo_latitude double precision, 
  photo_longitude double precision, 
  location_accuracy double precision, 
  suspicious_photo character varying(255), 
  photo_distance_tolerance double precision, 
  location_difference double precision, 
  uid character varying(255));

CREATE INDEX IF NOT EXISTS ph_geolocation_idx 
ON redshift.ph_geolocation_mvw (account_id, visit_id, photo_index);

COMMIT;