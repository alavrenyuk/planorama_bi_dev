DROP MATERIALIZED VIEW IF EXISTS redshift.ph_reported_osa_per_categ_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_reported_osa_per_categ_mvw
(
  uid,
  account_id,
  year_week,
  categ_name,
  all_osa,
  reported_osa
)
AS 
 SELECT t1.uid,
    t1.account_id,
    t1.year_week,
    t1.categ_name,
    t1.all_osa,
    t1.reported_osa
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       year_week,
       categ_name,
       all_osa,
       reported_osa
FROM consolidated.ph_reported_osa_per_categ;
$REDSHIFT$) t1(uid text, account_id character varying(50), year_week character varying(12), categ_name character varying(255), all_osa bigint, reported_osa bigint);

CREATE INDEX IF NOT EXISTS ph_reported_osa_per_categ_idx 
ON redshift.ph_reported_osa_per_categ_mvw (account_id, year_week, uid, categ_name);

COMMIT;
