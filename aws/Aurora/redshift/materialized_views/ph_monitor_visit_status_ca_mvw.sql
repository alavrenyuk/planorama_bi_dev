DROP MATERIALIZED VIEW redshift.ph_monitor_visit_status_ca_mvw;

CREATE MATERIALIZED VIEW redshift.ph_monitor_visit_status_ca_mvw
TABLESPACE pg_default
AS
 SELECT x.account_id,
    x.uid,
    x.visits_uploading,
    x.visits_processing,
    x.visits_live_rdy,
    x.visits_verified,
    x.visits_deleted,
    x.visits_skipped,
    x.visits_not_processed,
    x.visits_analysed,
    x.visits_under_analysis,
    x.visits_total_relevant,
    x.visits_total_w_not_precessed
   FROM dblink('foreign_server', $REDSHIFT$
  SELECT 
	account_id,
	uid,
	visits_uploading,
	visits_processing,
	visits_live_rdy,
	visits_verified,
	visits_deleted,
	visits_skipped,
	visits_not_processed,
	visits_analysed,
	visits_under_analysis,
	visits_total_relevant,
	visits_total_w_not_precessed
  FROM consolidated.ph_monitor_visit_status_ca; $REDSHIFT$) AS x
  (account_id character varying(255), 
  uid character varying(255), 
  visits_uploading integer, 
  visits_processing integer, 
  visits_live_rdy integer, 
  visits_verified integer, 
  visits_deleted integer, 
  visits_skipped integer, 
  visits_not_processed integer, 
  visits_analysed integer, 
  visits_under_analysis integer, 
  visits_total_relevant integer, 
  visits_total_w_not_precessed integer);

  COMMIT;