DROP MATERIALIZED VIEW IF EXISTS redshift.ph_timetracking_per_chain_mvw CASCADE;

CREATE MATERIALIZED VIEW redshift.ph_timetracking_per_chain_mvw
(
  uid,
  account_id,
  year_week,
  chainstore,
  tt,
  ptt
)
AS 
 SELECT t1.uid,
    t1.account_id,
    t1.year_week,
    t1.chainstore,
    t1.tt,
    t1.ptt
   FROM dblink('foreign_server', $REDSHIFT$
SELECT uid,
       account_id,
       year_week,
       chainstore,
       tt,
       ptt
FROM consolidated.ph_timetracking_per_chain;
$REDSHIFT$) AS t1
(uid text, account_id character varying(50), year_week character varying(12), chainstore character varying(50), tt double precision, ptt double precision);

CREATE INDEX IF NOT EXISTS ph_timetracking_per_chain_idx 
ON redshift.ph_timetracking_per_chain_mvw (account_id, year_week, uid, chainstore);

COMMIT;
