CREATE EXTENSION postgres_fdw;
CREATE EXTENSION dblink;

CREATE SERVER foreign_server
        FOREIGN DATA WRAPPER postgres_fdw
        OPTIONS (host 'planorama-dw-1.cweb45po0wga.eu-west-1.redshift.amazonaws.com', port '5439', dbname 'production', sslmode 'disable'); /*allow*/

CREATE USER MAPPING FOR planoexport
        SERVER foreign_server
        OPTIONS (user 'plano_auto_export', password 'Hj45%gfhnj45_hj3HJutR');

GRANT ALL PRIVILEGES ON FOREIGN SERVER foreign_server TO planoexport;