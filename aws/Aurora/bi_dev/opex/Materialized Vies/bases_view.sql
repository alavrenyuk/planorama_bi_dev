DROP MATERIALIZED VIEW opex.bases_view;

CREATE MATERIALIZED VIEW opex.bases_view
TABLESPACE pg_default
AS
 SELECT t1.visit_base_name,
    t1.visit_base_id,
    t1.visit_account_name,
    t1.account_nwp_id
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.bases_view '::text) 
   t1
   (visit_base_name character varying(255), 
   visit_base_id character varying(255), 
   visit_account_name character varying(255), 
   account_nwp_id character(24))
WITH DATA;

ALTER TABLE opex.bases_view OWNER TO planoexport;