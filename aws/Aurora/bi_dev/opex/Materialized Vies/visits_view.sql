DROP MATERIALIZED VIEW opex.visits_view;

CREATE MATERIALIZED VIEW opex.visits_view
TABLESPACE pg_default
AS
 SELECT t1.office,
    t1.account_id,
    t1.visit_id,
    t1.status_det,
    t1.week_num,
    t1.nb_photos,
    t1.accuracy,
    t1.tt_live,
    t1.ptt_live,
    t1.base_id,
    t1.daydate,
    t1.late_live,
    t1.late_final
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.visits_view_v2 '::text) 
   t1
   (office character varying(10), 
   account_id character varying(255), 
   visit_id character(24), 
   status_det character varying(20), 
   week_num double precision, 
   nb_photos smallint, 
   accuracy double precision, 
   tt_live double precision, 
   ptt_live double precision, 
   base_id character varying(255), 
   daydate date, 
   late_live character varying(5), 
   late_final character varying(5))
WITH DATA;

ALTER TABLE opex.visits_view OWNER TO planoexport;