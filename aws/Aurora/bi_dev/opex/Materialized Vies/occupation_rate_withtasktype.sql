DROP MATERIALIZED VIEW opex.occupation_rate_withtasktype;

CREATE MATERIALIZED VIEW opex.occupation_rate_withtasktype
TABLESPACE pg_default
AS
 SELECT t1.duration,
    t1.account_id,
    t1.week_num,
    t1.daydate,
    t1.office,
    t1.operation,
    t1.isvirtual
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.occupation_rate_withtasktype '::text) 
   t1
   (duration double precision, 
   account_id character varying(255), 
   week_num double precision, 
   daydate date, 
   office character varying(10), 
   operation character varying(20), 
   isvirtual character varying(255))
WITH DATA;

ALTER TABLE opex.occupation_rate_withtasktype OWNER TO planoexport;