DROP MATERIALIZED VIEW opex.volumes_treated_training;

CREATE MATERIALIZED VIEW opex.volumes_treated_training
TABLESPACE pg_default
AS
 SELECT t1.nb_treated,
    t1.avgtreatmenttime,
    t1.account_id,
    t1.visit_base_id,
    t1.week_num,
    t1.daydate,
    t1.visits,
    t1.isvirtual
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.volumes_treated_training '::text) 
   t1
   (nb_treated bigint, 
   avgtreatmenttime double precision, 
   account_id character varying(255), 
   visit_base_id character varying(255), 
   week_num double precision, 
   daydate date, 
   visits bigint, 
   isvirtual character varying(255))
WITH DATA;

ALTER TABLE opex.volumes_treated_training OWNER TO planoexport;