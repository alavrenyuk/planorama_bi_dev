DROP MATERIALIZED VIEW opex.occupation_bytasktype;

CREATE MATERIALIZED VIEW opex.occupation_bytasktype
TABLESPACE pg_default
AS
 SELECT t1.duration,
    t1.nb_tasks,
    t1.operation,
    t1.week_num,
    t1.daydate
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.occupation_bytasktype '::text) 
   t1
   (duration double precision, 
   nb_tasks bigint, 
   operation character varying(20), 
   week_num double precision, 
   daydate date)
WITH DATA;

ALTER TABLE opex.occupation_bytasktype OWNER TO planoexport;