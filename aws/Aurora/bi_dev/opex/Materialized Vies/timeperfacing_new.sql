DROP MATERIALIZED VIEW opex.timeperfacing_new;

CREATE MATERIALIZED VIEW opex.timeperfacing_new
TABLESPACE pg_default
AS
 SELECT t1.nb_facings,
    t1.duration,
    t1.task_type,
    t1.status,
    t1.account_id,
    t1.visit_base_id,
    t1.daydate,
    t1.week_num,
    t1.nb_tasks,
    t1.office,
    t1.visit_status
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.timeperfacingnew2 '::text) 
   t1
   (nb_facings numeric(38,0), 
   duration double precision, 
   task_type character varying(20), 
   status character varying(20), 
   account_id character varying(255), 
   visit_base_id character varying(255), 
   daydate date, 
   week_num double precision, 
   nb_tasks bigint, 
   office character varying(10), 
   visit_status character varying(20))
WITH DATA;

ALTER TABLE opex.timeperfacing_new OWNER TO planoexport;