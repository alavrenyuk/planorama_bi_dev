DROP MATERIALIZED VIEW opex.volumes_received_new;

CREATE MATERIALIZED VIEW opex.volumes_received_new
TABLESPACE pg_default
AS
 SELECT t1.nb_uploads,
    t1.account_id,
    t1.visit_base_id,
    t1.week_num,
    t1.daydate,
    t1.visit_status,
    t1.visits
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.volumes_received_new '::text) 
   t1
   (nb_uploads bigint, 
   account_id character varying(255), 
   visit_base_id character varying(255), 
   week_num double precision, 
   daydate date, 
   visit_status character varying(20), 
   visits bigint)
WITH DATA;

ALTER TABLE opex.volumes_received_new OWNER TO planoexport;