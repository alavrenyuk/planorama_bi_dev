DROP MATERIALIZED VIEW opex.own_generics;

CREATE MATERIALIZED VIEW opex.own_generics
TABLESPACE pg_default
AS
 SELECT t1.accoun_id,
    t1.sum_own_facings,
    t1.sum_own_generic_facings,
    t1.sum_total_facings,
    t1.sum_total_generic_facings,
    t1.month,
    t1.year,
    t1.daydate
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.own_generics '::text) t1(accoun_id character varying(24), sum_own_facings bigint, sum_own_generic_facings double precision, sum_total_facings bigint, sum_total_generic_facings double precision, month double precision, year double precision, daydate date)
WITH DATA;

ALTER TABLE opex.own_generics OWNER TO planoexport;

CREATE INDEX IF NOT EXISTS own_generics_idx ON opex.own_generics (accoun_id, month, year, daydate);