DROP MATERIALIZED VIEW opex.picture_quality;

CREATE MATERIALIZED VIEW opex.picture_quality
TABLESPACE pg_default
AS
 SELECT t1.nb_pics,
    t1.value_text,
    t1.photo_quality_reason,
    t1.account_id,
    t1.visit_base_name,
    t1.week_num,
    t1.daydate
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.picture_quality '::text) 
   t1
   (nb_pics bigint, 
   value_text character varying(255), 
   photo_quality_reason character varying(255), 
   account_id character varying(255), 
   visit_base_name character varying(255), 
   week_num double precision, 
   daydate date)
WITH DATA;

ALTER TABLE opex.picture_quality OWNER TO planoexport;