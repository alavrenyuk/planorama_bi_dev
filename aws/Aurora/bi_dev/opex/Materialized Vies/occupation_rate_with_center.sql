DROP MATERIALIZED VIEW opex.occupation_rate_with_center;

CREATE MATERIALIZED VIEW opex.occupation_rate_with_center
TABLESPACE pg_default
AS
 SELECT t1.duration,
    t1.account_id,
    t1.week_num,
    t1.daydate,
    t1.office
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.occupation_rate_with_center '::text) 
   t1
   (duration double precision, 
   account_id character varying(255), 
   week_num double precision, 
   daydate date, 
   office character varying(10))
WITH DATA;

ALTER TABLE opex.occupation_rate_with_center OWNER TO planoexport;