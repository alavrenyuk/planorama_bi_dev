DROP MATERIALIZED VIEW opex.waiting_by_op;

CREATE MATERIALIZED VIEW opex.waiting_by_op
TABLESPACE pg_default
AS
 SELECT t1.operator,
    t1.daydate,
    t1.duration_of_waiting
   FROM dblink('foreign_server'::text, ' SELECT * FROM public.waiting_by_op '::text) 
   t1
   (operator character varying(255), 
   daydate date, 
   duration_of_waiting double precision)
WITH DATA;

ALTER TABLE opex.waiting_by_op OWNER TO planoexport;

CREATE INDEX IF NOT EXISTS waiting_by_op_idx ON opex.waiting_by_op (operator, daydate);