DROP VIEW opex.occupation_rate_with_center_vw;

CREATE OR REPLACE VIEW opex.occupation_rate_with_center_vw
 AS
 SELECT occupation_rate_with_center.duration,
    occupation_rate_with_center.account_id,
    occupation_rate_with_center.week_num,
    occupation_rate_with_center.daydate,
    occupation_rate_with_center.office
   FROM opex.occupation_rate_with_center;

ALTER TABLE opex.occupation_rate_with_center_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.occupation_rate_with_center_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.occupation_rate_with_center_vw TO planoexport;