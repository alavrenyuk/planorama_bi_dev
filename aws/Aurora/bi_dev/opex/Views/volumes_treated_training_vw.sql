DROP VIEW opex.volumes_treated_training_vw;

CREATE OR REPLACE VIEW opex.volumes_treated_training_vw
 AS
 SELECT volumes_treated_training.nb_treated,
    volumes_treated_training.avgtreatmenttime,
    volumes_treated_training.account_id,
    volumes_treated_training.visit_base_id,
    volumes_treated_training.week_num,
    volumes_treated_training.daydate,
    volumes_treated_training.visits,
    volumes_treated_training.isvirtual
   FROM opex.volumes_treated_training;

ALTER TABLE opex.volumes_treated_training_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.volumes_treated_training_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.volumes_treated_training_vw TO planoexport;