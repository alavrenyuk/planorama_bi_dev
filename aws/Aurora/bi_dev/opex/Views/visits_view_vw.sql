DROP VIEW opex.visits_view_vw;

CREATE OR REPLACE VIEW opex.visits_view_vw
 AS
 SELECT visits_view.office,
    visits_view.account_id,
    visits_view.visit_id,
    visits_view.status_det,
    visits_view.week_num,
    visits_view.nb_photos,
    visits_view.accuracy,
    visits_view.tt_live,
    visits_view.ptt_live,
    visits_view.base_id,
    visits_view.daydate,
    visits_view.late_live,
    visits_view.late_final
   FROM opex.visits_view;

ALTER TABLE opex.visits_view_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.visits_view_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.visits_view_vw TO planoexport;