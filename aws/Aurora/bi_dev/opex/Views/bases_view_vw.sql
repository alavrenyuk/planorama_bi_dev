DROP VIEW opex.bases_view_vw;

CREATE OR REPLACE VIEW opex.bases_view_vw
 AS
 SELECT bases_view.visit_base_name,
    bases_view.visit_base_id,
    bases_view.visit_account_name,
    bases_view.account_nwp_id
   FROM opex.bases_view;

ALTER TABLE opex.bases_view_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.bases_view_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.bases_view_vw TO planoexport;