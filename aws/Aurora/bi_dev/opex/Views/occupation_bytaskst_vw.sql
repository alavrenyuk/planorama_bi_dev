DROP VIEW opex.occupation_bytaskst_vw;

CREATE OR REPLACE VIEW opex.occupation_bytaskst_vw
 AS
 SELECT occupation_bytaskst.duration,
    occupation_bytaskst.nb_tasks,
    occupation_bytaskst.status,
    occupation_bytaskst.week_num,
    occupation_bytaskst.daydate
   FROM opex.occupation_bytaskst;

ALTER TABLE opex.occupation_bytaskst_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.occupation_bytaskst_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.occupation_bytaskst_vw TO planoexport;