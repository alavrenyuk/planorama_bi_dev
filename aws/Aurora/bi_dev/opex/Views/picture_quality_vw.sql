DROP VIEW opex.picture_quality_vw;

CREATE OR REPLACE VIEW opex.picture_quality_vw
 AS
 SELECT picture_quality.nb_pics,
    picture_quality.value_text,
    picture_quality.photo_quality_reason,
    picture_quality.account_id,
    picture_quality.visit_base_name,
    picture_quality.week_num,
    picture_quality.daydate
   FROM opex.picture_quality;

ALTER TABLE opex.picture_quality_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.picture_quality_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.picture_quality_vw TO planoexport;