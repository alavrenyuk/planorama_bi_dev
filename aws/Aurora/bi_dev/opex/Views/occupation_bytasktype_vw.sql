DROP VIEW opex.occupation_bytasktype_vw;

CREATE OR REPLACE VIEW opex.occupation_bytasktype_vw
 AS
 SELECT occupation_bytasktype.duration,
    occupation_bytasktype.nb_tasks,
    occupation_bytasktype.operation,
    occupation_bytasktype.week_num,
    occupation_bytasktype.daydate
   FROM opex.occupation_bytasktype;

ALTER TABLE opex.occupation_bytasktype_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.occupation_bytasktype_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.occupation_bytasktype_vw TO planoexport;