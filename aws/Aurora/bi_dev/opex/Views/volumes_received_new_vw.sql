DROP VIEW opex.volumes_received_new_vw;

CREATE OR REPLACE VIEW opex.volumes_received_new_vw
 AS
 SELECT volumes_received_new.nb_uploads,
    volumes_received_new.account_id,
    volumes_received_new.visit_base_id,
    volumes_received_new.week_num,
    volumes_received_new.daydate,
    volumes_received_new.visit_status,
    volumes_received_new.visits
   FROM opex.volumes_received_new;

ALTER TABLE opex.volumes_received_new_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.volumes_received_new_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.volumes_received_new_vw TO planoexport;