DROP VIEW opex.waiting_by_op_vw;

CREATE OR REPLACE VIEW opex.waiting_by_op_vw
 AS
 SELECT waiting_by_op.operator,
    waiting_by_op.daydate,
    waiting_by_op.duration_of_waiting
   FROM opex.waiting_by_op;

ALTER TABLE opex.waiting_by_op_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.waiting_by_op_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.waiting_by_op_vw TO planoexport;