DROP VIEW opex.timeperfacing_new_vw;

CREATE OR REPLACE VIEW opex.timeperfacing_new_vw
 AS
 SELECT timeperfacing_new.nb_facings,
    timeperfacing_new.duration,
    timeperfacing_new.task_type,
    timeperfacing_new.status,
    timeperfacing_new.account_id,
    timeperfacing_new.visit_base_id,
    timeperfacing_new.daydate,
    timeperfacing_new.week_num,
    timeperfacing_new.nb_tasks,
    timeperfacing_new.office,
    timeperfacing_new.visit_status
   FROM opex.timeperfacing_new;

ALTER TABLE opex.timeperfacing_new_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.timeperfacing_new_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.timeperfacing_new_vw TO planoexport;