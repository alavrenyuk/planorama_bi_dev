DROP VIEW opex.occupation_rate_withtasktype_vw;

CREATE OR REPLACE VIEW opex.occupation_rate_withtasktype_vw
 AS
 SELECT occupation_rate_withtasktype.duration,
    occupation_rate_withtasktype.account_id,
    occupation_rate_withtasktype.week_num,
    occupation_rate_withtasktype.daydate,
    occupation_rate_withtasktype.office,
    occupation_rate_withtasktype.operation,
    occupation_rate_withtasktype.isvirtual
   FROM opex.occupation_rate_withtasktype;

ALTER TABLE opex.occupation_rate_withtasktype_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.occupation_rate_withtasktype_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.occupation_rate_withtasktype_vw TO planoexport;