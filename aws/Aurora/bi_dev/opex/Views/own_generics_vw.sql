DROP VIEW opex.own_generics_vw;

CREATE OR REPLACE VIEW opex.own_generics_vw
 AS
 SELECT own_generics.accoun_id,
    own_generics.sum_own_facings,
    own_generics.sum_own_generic_facings,
    own_generics.sum_total_facings,
    own_generics.sum_total_generic_facings,
    own_generics.month,
    own_generics.year,
    own_generics.daydate
   FROM opex.own_generics;

ALTER TABLE opex.own_generics_vw
    OWNER TO planoexport;

GRANT SELECT ON TABLE opex.own_generics_vw TO powerbi_user1;
GRANT ALL ON TABLE opex.own_generics_vw TO planoexport;