CREATE OR REPLACE VIEW ca_58dcbd8fc70eae0a14633dfa00.compliance_fact
 AS
 SELECT compliance_fact_mvw.account_id,
    compliance_fact_mvw.visit_id,
    compliance_fact_mvw.kpi,
    compliance_fact_mvw.kpi_id,
    compliance_fact_mvw.value,
    compliance_fact_mvw.compliant,
    compliance_fact_mvw.threshold,
    compliance_fact_mvw.weight,
    compliance_fact_mvw.field,
    compliance_fact_mvw.group_name,
    compliance_fact_mvw.parent,
    compliance_fact_mvw.field_value,
    compliance_fact_mvw.force_na
   FROM ca_58dcbd8fc70eae0a14633dfa00.compliance_fact_mvw;

COMMIT;