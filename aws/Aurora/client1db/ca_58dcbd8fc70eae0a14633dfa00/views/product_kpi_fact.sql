CREATE OR REPLACE VIEW ca_58dcbd8fc70eae0a14633dfa00.product_kpi_fact
 AS
 SELECT product_kpi_fact_mvw.account_id,
    product_kpi_fact_mvw.visit_id,
    product_kpi_fact_mvw.product_id,
    product_kpi_fact_mvw.store_id,
    product_kpi_fact_mvw.owner_id,
    product_kpi_fact_mvw.visit_date,
    product_kpi_fact_mvw.prod_ean,
    product_kpi_fact_mvw.analysis,
    product_kpi_fact_mvw.kpi,
    product_kpi_fact_mvw.own,
    product_kpi_fact_mvw.currnt_stat,
    product_kpi_fact_mvw.value,
    product_kpi_fact_mvw.out_of_stock,
    product_kpi_fact_mvw.display_group,
    product_kpi_fact_mvw.ranking,
    product_kpi_fact_mvw.categ_name
   FROM ca_58dcbd8fc70eae0a14633dfa00.product_kpi_fact_mvw;

COMMIT;