CREATE OR REPLACE VIEW ca_58dcbd8fc70eae0a14633dfa00.photo_fact
 AS
 SELECT photo_fact_mvw.account_id,
    photo_fact_mvw.visit_id,
    photo_fact_mvw.owner_id,
    photo_fact_mvw.store_id,
    photo_fact_mvw.visit_date,
    photo_fact_mvw.photo_quality_int,
    photo_fact_mvw.photo_quality,
    photo_fact_mvw.photo_tag,
    photo_fact_mvw.photo_url,
    photo_fact_mvw.max_shelf,
    photo_fact_mvw.photo_quality_reason,
    photo_fact_mvw.photo_index,
    photo_fact_mvw.section_index,
    photo_fact_mvw.photo_group,
    photo_fact_mvw.photo_group_type,
    photo_fact_mvw.photo_group_tags
   FROM ca_58dcbd8fc70eae0a14633dfa00.photo_fact_mvw;

COMMIT;