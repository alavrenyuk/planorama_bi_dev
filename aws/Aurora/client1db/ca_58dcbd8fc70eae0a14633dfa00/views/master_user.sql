CREATE OR REPLACE VIEW ca_58dcbd8fc70eae0a14633dfa00.master_user
 AS
 SELECT master_user_mvw.account_id,
    master_user_mvw.owner_id,
    master_user_mvw.username,
    master_user_mvw.first_name,
    master_user_mvw.last_name,
    master_user_mvw.email,
    master_user_mvw.role,
    master_user_mvw.roles,
    master_user_mvw.manager,
    master_user_mvw.password,
    master_user_mvw.desc_a,
    master_user_mvw.desc_b,
    master_user_mvw.desc_c,
    master_user_mvw.desc_d,
    master_user_mvw.desc_e,
    master_user_mvw.desc_f,
    master_user_mvw.desc_g,
    master_user_mvw.desc_h,
    master_user_mvw.active
   FROM ca_58dcbd8fc70eae0a14633dfa00.master_user_mvw;

COMMIT;