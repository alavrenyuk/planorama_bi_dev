CREATE OR REPLACE VIEW ca_58dcbd8fc70eae0a14633dfa00.calendar
 AS
 SELECT calendar_mvw.account_id,
    calendar_mvw.visit_date,
    calendar_mvw.year,
    calendar_mvw.month_no,
    calendar_mvw.month_name,
    calendar_mvw.week,
    calendar_mvw.week_no,
    calendar_mvw.year_week,
    calendar_mvw.quarter,
    calendar_mvw.quarter_no,
    calendar_mvw.past_year,
    calendar_mvw.past_month_no,
    calendar_mvw.past_week_no,
    calendar_mvw.past_quarter_no
   FROM ca_58dcbd8fc70eae0a14633dfa00.calendar_mvw;

COMMIT;