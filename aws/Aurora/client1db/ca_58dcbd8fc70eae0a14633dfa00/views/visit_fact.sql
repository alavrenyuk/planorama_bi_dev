CREATE OR REPLACE VIEW ca_58dcbd8fc70eae0a14633dfa00.visit_fact
 AS
 SELECT visit_fact_mvw.account_id,
    visit_fact_mvw.visit_id,
    visit_fact_mvw.visit_date,
    visit_fact_mvw.analysis_date,
    visit_fact_mvw.categ_code,
    visit_fact_mvw.categ_name,
    visit_fact_mvw.owner_id,
    visit_fact_mvw.store_id,
    visit_fact_mvw.number_of_pictures,
    visit_fact_mvw.number_of_sections,
    visit_fact_mvw.process_type,
    visit_fact_mvw.user_comment,
    visit_fact_mvw.plan_tag
   FROM ca_58dcbd8fc70eae0a14633dfa00.visit_fact_mvw;

COMMIT;