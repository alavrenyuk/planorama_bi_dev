CREATE OR REPLACE VIEW ca_58dcbd8fc70eae0a14633dfa00.product_fact
 AS
 SELECT product_fact_mvw.account_id,
    product_fact_mvw.visit_id,
    product_fact_mvw.owner_id,
    product_fact_mvw.store_id,
    product_fact_mvw.product_id,
    product_fact_mvw.visit_date,
    product_fact_mvw.photo_index,
    product_fact_mvw.section_index,
    product_fact_mvw.photo_group,
    product_fact_mvw.photo_group_type,
    product_fact_mvw.shelf_level,
    product_fact_mvw.horizontal_position_index,
    product_fact_mvw.vertical_position,
    product_fact_mvw.categ_code,
    product_fact_mvw.categ_name,
    product_fact_mvw.ranking,
    product_fact_mvw.facings_tgt,
    product_fact_mvw.own,
    product_fact_mvw.currnt_stat,
    product_fact_mvw.value,
    product_fact_mvw.out_of_stock,
    product_fact_mvw.linear,
    product_fact_mvw.facings,
    product_fact_mvw.packages,
    product_fact_mvw.is_promo,
    product_fact_mvw.price
   FROM ca_58dcbd8fc70eae0a14633dfa00.product_fact_mvw;

COMMIT;