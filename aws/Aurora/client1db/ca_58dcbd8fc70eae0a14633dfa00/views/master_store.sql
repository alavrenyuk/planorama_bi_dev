CREATE OR REPLACE VIEW ca_58dcbd8fc70eae0a14633dfa00.master_store
 AS
 SELECT master_store_mvw.account_id,
    master_store_mvw.store_id,
    master_store_mvw.internal_code,
    master_store_mvw.chainstore,
    master_store_mvw.description,
    master_store_mvw.address,
    master_store_mvw.region,
    master_store_mvw.city,
    master_store_mvw.postal_code,
    master_store_mvw.owners,
    master_store_mvw.desc_a,
    master_store_mvw.desc_b,
    master_store_mvw.desc_c,
    master_store_mvw.desc_d,
    master_store_mvw.desc_e,
    master_store_mvw.desc_f,
    master_store_mvw.desc_g,
    master_store_mvw.desc_h,
    master_store_mvw.country,
    master_store_mvw.latitude,
    master_store_mvw.longitude,
    master_store_mvw.is_deleted
   FROM ca_58dcbd8fc70eae0a14633dfa00.master_store_mvw;

COMMIT;