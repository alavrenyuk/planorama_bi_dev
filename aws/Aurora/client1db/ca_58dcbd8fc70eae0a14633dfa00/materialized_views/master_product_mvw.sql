DROP MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.master_product_mvw;

CREATE MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.master_product_mvw
AS
 SELECT *
   FROM dblink('foreign_server_redshift'::text, $REDSHIFT$ 
			   SELECT * FROM consolidated.master_product
			   $REDSHIFT$) t1
			   (   
   account_id    varchar(50),
   product_id    varchar(50),
   base          varchar(255),
   product_ean   varchar(50),
   description   varchar(255),
   manufacturer  varchar(50),
   brand         varchar(50),
   height        integer,
   width         integer,
   depth         integer,
   code          varchar(255),
   crea_on       timestamp,
   mod_on        timestamp,
   seg1          varchar(255),
   seg2          varchar(255),
   seg3          varchar(255),
   seg4          varchar(255),
   seg5          varchar(255),
   desc_a        varchar(255),
   desc_b        varchar(255),
   desc_c        varchar(255),
   desc_d        varchar(255),
   desc_e        varchar(255),
   desc_f        varchar(255),
   desc_g        varchar(255),
   desc_h        varchar(255),
   desc_i        varchar(255),
   desc_j        varchar(255),
   kind          integer,
   labelling     varchar(255),
   is_validated  boolean,
   excl_from     boolean,
   img           integer,
   is_deleted    boolean,
   desc_k        varchar(256),
   desc_l        varchar(256),
   desc_m        varchar(256),
   desc_n        varchar(256),
   desc_o        varchar(256),
   desc_p        varchar(256),
   desc_q        varchar(256),
   desc_r        varchar(256),
   desc_s        varchar(256),
   desc_t        varchar(256),
   desc_u        varchar(256),
   desc_v        varchar(256),
   desc_w        varchar(256),
   desc_x        varchar(256),
   desc_y        varchar(256),
   desc_z        varchar(256)
			   )
   WHERE account_id IN (SELECT account_id FROM ca_58dcbd8fc70eae0a14633dfa00.account)
WITH DATA;

CREATE INDEX IF NOT EXISTS master_product_idx
ON ca_58dcbd8fc70eae0a14633dfa00.master_product_mvw (account_id, product_id);

COMMIT;