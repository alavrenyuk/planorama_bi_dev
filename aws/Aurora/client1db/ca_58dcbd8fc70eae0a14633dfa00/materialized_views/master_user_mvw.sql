DROP MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.master_user_mvw;

CREATE MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.master_user_mvw
AS
 SELECT *
   FROM dblink('foreign_server_redshift'::text, $REDSHIFT$ 
			   SELECT * FROM consolidated.master_user
			   $REDSHIFT$) t1
			   (
   account_id  varchar(50),
   owner_id    varchar(50),
   username    varchar(50),
   first_name  varchar(50),
   last_name   varchar(50),
   email       varchar(255),
   role        varchar(255),
   roles       varchar(255),
   manager     varchar(255),
   password    varchar(255),
   desc_a      varchar(255),
   desc_b      varchar(255),
   desc_c      varchar(255),
   desc_d      varchar(255),
   desc_e      varchar(255),
   desc_f      varchar(255),
   desc_g      varchar(255),
   desc_h      varchar(255),
   active      boolean				   
			   )
   WHERE account_id IN (SELECT account_id FROM ca_58dcbd8fc70eae0a14633dfa00.account)
WITH DATA;

CREATE INDEX IF NOT EXISTS master_store_idx
ON ca_58dcbd8fc70eae0a14633dfa00.master_store_mvw (account_id, store_id);

COMMIT;