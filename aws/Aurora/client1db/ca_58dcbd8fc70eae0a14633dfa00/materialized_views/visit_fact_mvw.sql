DROP MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.visit_fact_mvw;

CREATE MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.visit_fact_mvw
AS
 SELECT *
   FROM dblink('foreign_server_redshift'::text, $REDSHIFT$ 
			   SELECT * FROM consolidated.visit_fact
			   $REDSHIFT$) t1
			   (
    account_id          varchar(50),
   visit_id            varchar(50),
   visit_date          date,
   analysis_date       date,
   categ_code          varchar(30),
   categ_name          varchar(255),
   owner_id            varchar(50),
   store_id            varchar(50),
   number_of_pictures  float4,
   number_of_sections  float4,
   process_type        varchar(50),
   user_comment        varchar(1000),
   plan_tag            varchar(255)
			   )
   WHERE account_id IN (SELECT account_id FROM ca_58dcbd8fc70eae0a14633dfa00.account)
WITH DATA;

CREATE INDEX IF NOT EXISTS visit_fact_idx
ON ca_58dcbd8fc70eae0a14633dfa00.visit_fact_mvw (account_id, visit_id);

COMMIT;