DROP MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.compliance_fact_mvw;

CREATE MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.compliance_fact_mvw
AS
 SELECT *
   FROM dblink('foreign_server_redshift'::text, $REDSHIFT$ 
			   SELECT * FROM consolidated.compliance_fact
			   $REDSHIFT$) t1
			   (
   account_id   varchar(50),
   visit_id     varchar(50),
   kpi          varchar(255),
   kpi_id       float8,
   value        float8,
   compliant    boolean,
   threshold    float8,
   weight       float8,
   field        varchar(255),
   group_name   varchar(255),
   parent       varchar(255),
   field_value  varchar(255),
   force_na     boolean
			   )
   WHERE account_id IN (SELECT account_id FROM ca_58dcbd8fc70eae0a14633dfa00.account)
WITH DATA;

CREATE INDEX IF NOT EXISTS compliance_fact_idx
ON ca_58dcbd8fc70eae0a14633dfa00.compliance_fact_mvw (account_id, visit_id, kpi_id);

COMMIT;