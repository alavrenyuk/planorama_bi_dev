DROP MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.calendar_mvw;

CREATE MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.calendar_mvw
AS
 SELECT *
   FROM dblink('foreign_server_redshift'::text, $REDSHIFT$ 
			   SELECT * FROM consolidated.calendar 
			   $REDSHIFT$) t1
			   (   account_id       varchar(50),
   visit_date       date,
   year             integer,
   month_no         integer,
   month_name       varchar(50),
   week             varchar(16),
   week_no          integer,
   year_week        varchar(12),
   quarter          varchar(10),
   quarter_no       integer,
   past_year        integer,
   past_month_no    integer,
   past_week_no     integer,
   past_quarter_no  integer)
   WHERE account_id IN (SELECT account_id FROM ca_58dcbd8fc70eae0a14633dfa00.account)
WITH DATA;

CREATE INDEX IF NOT EXISTS calendar_idx
ON ca_58dcbd8fc70eae0a14633dfa00.calendar_mvw (account_id, visit_date);

COMMIT;