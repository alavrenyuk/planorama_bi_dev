DROP MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.photo_fact_mvw;

CREATE MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.photo_fact_mvw
AS
 SELECT *
   FROM dblink('foreign_server_redshift'::text, $REDSHIFT$ 
			   SELECT * FROM consolidated.photo_fact
			   $REDSHIFT$) t1
			   (
   account_id            varchar(50),
   visit_id              varchar(50),
   owner_id              varchar(50),
   store_id              varchar(50),
   visit_date            date,
   photo_quality_int     smallint,
   photo_quality         varchar(255),
   photo_tag             varchar(255),
   photo_url             varchar(2083),
   max_shelf             float4,
   photo_quality_reason  varchar(255),
   photo_index           float4,
   section_index         float4,
   photo_group           float8,
   photo_group_type      varchar(255),
   photo_group_tags      varchar(255)
			   )
   WHERE account_id IN (SELECT account_id FROM ca_58dcbd8fc70eae0a14633dfa00.account)
WITH DATA;

CREATE INDEX IF NOT EXISTS photo_fact_idx
ON ca_58dcbd8fc70eae0a14633dfa00.photo_fact_mvw (account_id, visit_id, photo_index);

COMMIT;