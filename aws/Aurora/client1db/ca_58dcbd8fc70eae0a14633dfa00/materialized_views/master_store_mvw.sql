DROP MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.master_store_mvw;

CREATE MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.master_store_mvw
AS
 SELECT *
   FROM dblink('foreign_server_redshift'::text, $REDSHIFT$ 
			   SELECT * FROM consolidated.master_store
			   $REDSHIFT$) t1
			   (   
			      account_id     varchar(50),
   store_id       varchar(50),
   internal_code  varchar(255),
   chainstore     varchar(255),
   description    varchar(255),
   address        varchar(255),
   region         varchar(255),
   city           varchar(50),
   postal_code    varchar(20),
   owners         varchar(65535),
   desc_a         varchar(255),
   desc_b         varchar(255),
   desc_c         varchar(255),
   desc_d         varchar(255),
   desc_e         varchar(255),
   desc_f         varchar(255),
   desc_g         varchar(255),
   desc_h         varchar(255),
   country        varchar(50),
   latitude       float8,
   longitude      float8,
   is_deleted     boolean
			   )
   WHERE account_id IN (SELECT account_id FROM ca_58dcbd8fc70eae0a14633dfa00.account)
WITH DATA;

CREATE INDEX IF NOT EXISTS master_store_idx
ON ca_58dcbd8fc70eae0a14633dfa00.master_store_mvw (account_id, store_id);

COMMIT;