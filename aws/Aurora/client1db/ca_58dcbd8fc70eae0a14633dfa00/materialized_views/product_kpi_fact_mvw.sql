DROP MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.product_kpi_fact_mvw;

CREATE MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.product_kpi_fact_mvw
AS
 SELECT *
   FROM dblink('foreign_server_redshift'::text, $REDSHIFT$ 
			   SELECT * FROM consolidated.product_kpi_fact
			   $REDSHIFT$) t1
			   (
   account_id     varchar(50),
   visit_id       varchar(50),
   product_id     varchar(50),
   store_id       varchar(50),
   owner_id       varchar(50),
   visit_date     date,
   prod_ean       varchar(500),
   analysis       varchar(255),
   kpi            varchar(255),
   own            float4,
   currnt_stat    varchar(50),
   value          float8,
   out_of_stock   float4,
   display_group  varchar(255),
   ranking        smallint,
   categ_name     varchar(255)	   
			   )
   WHERE account_id IN (SELECT account_id FROM ca_58dcbd8fc70eae0a14633dfa00.account)
WITH DATA;

CREATE INDEX IF NOT EXISTS product_kpi_fact_idx
ON ca_58dcbd8fc70eae0a14633dfa00.product_kpi_fact_mvw (account_id, visit_id, product_id, kpi);

COMMIT;