DROP MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.visit_kpi_fact_mvw;

CREATE MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.visit_kpi_fact_mvw
AS
 SELECT *
   FROM dblink('foreign_server_redshift'::text, $REDSHIFT$ 
			   SELECT * FROM consolidated.visit_kpi_fact
			   $REDSHIFT$) t1
			   (
   account_id          char(24),
   visit_id            char(24),
   visit_date          timestamp,
   store_id            varchar(256),
   owner_id            varchar(256),
   analysis            varchar(256),
   kpi                 varchar(256),
   kpi_id              integer,
   own                 boolean,
   display_group       varchar(256),
   question_english    varchar(256),
   question_local      varchar(256),
   product_group_name  varchar(256),
   prod_manufacturer   varchar(256),
   prod_brand          varchar(256),
   linear              float8,
   facings             integer,
   packages            integer,
   value               float8,
   target              float8,
   discrepancy         float8,
   compliant           boolean,
   kpi_group           varchar(256),
   question_answers    varchar(256),
   photo_group         smallint,
   photo_group_type    varchar(256),
   custom_field_a      varchar(256),
   custom_field_b      varchar(256),
   custom_field_c      varchar(256),
   custom_field_d      varchar(256),
   custom_field_e      varchar(256),
   row_id              integer  
			   )
   WHERE account_id IN (SELECT account_id FROM ca_58dcbd8fc70eae0a14633dfa00.account)
WITH DATA;

CREATE INDEX IF NOT EXISTS visit_kpi_fact_idx
ON ca_58dcbd8fc70eae0a14633dfa00.visit_kpi_fact_mvw (account_id, visit_id, kpi_id);

COMMIT;