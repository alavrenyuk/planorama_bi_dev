DROP MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.product_fact_mvw;

CREATE MATERIALIZED VIEW ca_58dcbd8fc70eae0a14633dfa00.product_fact_mvw
AS
 SELECT *
   FROM dblink('foreign_server_redshift'::text, $REDSHIFT$ 
			   SELECT * FROM consolidated.product_fact
			   $REDSHIFT$) t1
			   (
   account_id                 varchar(50),
   visit_id                   varchar(50),
   owner_id                   varchar(50),
   store_id                   varchar(50),
   product_id                 varchar(50),
   visit_date                 date       ,
   photo_index                smallint,
   section_index              smallint,
   photo_group                smallint,
   photo_group_type           varchar(255),
   shelf_level                smallint,
   horizontal_position_index  smallint,
   vertical_position          smallint,
   categ_code                 varchar(255),
   categ_name                 varchar(255),
   ranking                    float8,
   facings_tgt                float8,
   own                        float4,
   currnt_stat                varchar(50),
   value                      float8,
   out_of_stock               float4,
   linear                     float8,
   facings                    float8,
   packages                   float8,
   is_promo                   boolean,
   price                      float8				   
			   )
   WHERE account_id IN (SELECT account_id FROM ca_58dcbd8fc70eae0a14633dfa00.account)
WITH DATA;

CREATE INDEX IF NOT EXISTS product_fact_idx
ON ca_58dcbd8fc70eae0a14633dfa00.product_fact_mvw (account_id, visit_id, product_id, photo_index);

COMMIT;