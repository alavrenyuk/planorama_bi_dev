DROP TABLE IF EXISTS error.visit_fact CASCADE;

CREATE TABLE error.visit_fact
(
   account_id          varchar(50),
   visit_id            varchar(50),
   visit_date          date,
   visit_datetime      timestamptz,
   categ_code          varchar(255),
   categ_name          varchar(255),
   owner_id            varchar(50),
   store_id            varchar(50),
   number_of_pictures  float4,
   number_of_sections  float4,
   process_type        varchar(50),
   processed_on        timestamptz,
   error_type          varchar(50)
);

COMMIT;