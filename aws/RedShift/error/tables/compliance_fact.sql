DROP TABLE IF EXISTS error.compliance_fact CASCADE;

CREATE TABLE error.compliance_fact
(
   account_id    varchar(50),
   visit_id      varchar(50),
   kpi           varchar(255),
   kpi_id        float8,
   value         float8,
   compliant     boolean,
   threshold     float8,
   weight        float8,
   field         varchar(255),
   group_name    varchar(255),
   parent        varchar(255),
   field_value   varchar(255),
   force_na      boolean,
   row_id        bigint,
   processed_on  timestamptz,
   error_type    varchar(256)
);

COMMIT;