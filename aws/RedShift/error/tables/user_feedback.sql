DROP TABLE IF EXISTS error.user_feedback CASCADE;

CREATE TABLE error.user_feedback
(
   account_id                  varchar(50),
   visit_id                    varchar(50),
   visit_date                  timestamp,
   analysis_date               timestamp,
   feedback_registration_date  timestamp,
   category_code               varchar(50),
   store_code                  varchar(50),
   owner_id                    varchar(50),
   kpi_id                      varchar(10),
   kpi                         varchar(50),
   product_ean                 varchar(500),
   toggle1                     varchar(2000),
   toggle2                     varchar(50),
   toggle3                     varchar(50),
   toggle4                     varchar(50),
   toggle5                     varchar(50),
   status_after_verif          varchar(10),
   row_id                      bigint,
   processed_on                timestamptz,
   error_type                  varchar(11)
);

COMMIT;