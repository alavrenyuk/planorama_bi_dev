DROP TABLE IF EXISTS error.master_data_store CASCADE;

CREATE TABLE error.master_data_store
(
   account_id     varchar(50),
   store_id       varchar(50),
   internal_code  varchar(255),
   chainstore     varchar(255),
   description    varchar(255),
   address        varchar(255),
   region         varchar(255),
   city           varchar(50),
   postal_code    varchar(50),
   owners         varchar(65535),
   desc_a         varchar(255),
   desc_b         varchar(255),
   desc_c         varchar(255),
   desc_d         varchar(255),
   desc_e         varchar(255),
   desc_f         varchar(255),
   desc_g         varchar(255),
   desc_h         varchar(255),
   country        varchar(50),
   latitude       float8,
   longitude      float8,
   is_deleted     boolean,
   row_id         bigint,
   processed_on   timestamptz,
   error_type     varchar(256)
);

COMMIT;