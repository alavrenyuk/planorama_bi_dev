DROP TABLE IF EXISTS error.photo_fact CASCADE;

CREATE TABLE error.photo_fact
(
   account_id            varchar(50),
   visit_id              varchar(50),
   owner_id              varchar(50),
   store_id              varchar(50),
   visit_date            date,
   photo_quality         varchar(255),
   photo_tag             varchar(255),
   photo_url             varchar(2083),
   max_shelf             float4,
   photo_quality_reason  varchar(255),
   photo_index           float4,
   section_index         float4,
   stack_index           float4,
   photo_group           float8,
   photo_group_type      varchar(255),
   photo_group_tags      varchar(255),
   row_id                bigint,
   processed_on          timestamptz,
   error_type            varchar(256)
);

COMMIT;