DROP TABLE IF EXISTS error.new_time_tracking CASCADE;

CREATE TABLE error.new_time_tracking
(
   account_id                             varchar(50),
   visit_id                               varchar(50),
   number_of_pictures                     float4,
   category_name                          varchar(255),
   start_photo_capture                    timestamp,
   end_photo_capture                      timestamp,
   duration_photo_capture                 float4,
   visit_creation_on_server               timestamp,
   waiting_time_before_upload             float4,
   start_photo_upload                     timestamp,
   end_photo_upload                       timestamp,
   duration_upload                        float4,
   start_processing                       timestamp,
   end_processing                         timestamp,
   duration_processing                    float4,
   duration_end_upload_to_end_processing  float4,
   start_labelling                        timestamp,
   end_labelling                          timestamp,
   duration_labelling                     float4,
   start_verification                     timestamp,
   end_verification                       timestamp,
   duration_verification                  float4,
   ptt                                    float4,
   row_id                                 bigint,
   processed_on                           timestamptz,
   error_type                             varchar(11)
);

COMMIT;