DROP TABLE IF EXISTS error.product_fact CASCADE;

CREATE TABLE error.product_fact
(
   account_id                 varchar(50),
   visit_id                   varchar(50),
   owner_id                   varchar(50),
   store_id                   varchar(50),
   product_id                 varchar(50),
   visit_date                 date,
   photo_index                float4,
   section_index              float4,
   photo_group                float8,
   photo_group_type           varchar(255),
   shelf_level                float4,
   horizontal_position_index  float4,
   vertical_position          float4,
   categ_code                 varchar(255),
   categ_name                 varchar(255),
   ranking                    float8,
   facings_tgt                float8,
   own                        float4,
   currnt_stat                varchar(50),
   value                      float8,
   out_of_stock               float4,
   linear                     float8,
   facings                    float8,
   packages                   float8,
   is_promo                   boolean,
   price                      float8,
   row_id                     bigint,
   processed_on               timestamptz,
   error_type                 varchar(256)
);

COMMIT;