DROP TABLE IF EXISTS error.geolocation CASCADE;

CREATE TABLE error.geolocation
(
   account_id                varchar(255),
   visit_id                  varchar(255),
   visit_date                timestamp,
   owner_id                  varchar(255),
   store_code                varchar(255),
   number_of_pictures        float8,
   photo_index               float8,
   store_latitude            float8,
   store_longitude           float8,
   photo_latitude            float8,
   photo_longitude           float8,
   location_accuracy         float8,
   suspicious_photo          varchar(255),
   photo_distance_tolerance  float8,
   location_difference       float8,
   row_id                    bigint,
   processed_on              timestamptz,
   error_type                varchar(9)
);

COMMIT;