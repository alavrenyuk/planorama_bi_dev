DROP TABLE IF EXISTS error.master_data_user CASCADE;

CREATE TABLE error.master_data_user
(
   account_id    varchar(50),
   user_id       varchar(255),
   username      varchar(50),
   first_name    varchar(50),
   last_name     varchar(50),
   email         varchar(255),
   role          varchar(255),
   roles         varchar(255),
   manager       varchar(255),
   password      varchar(255),
   desc_a        varchar(255),
   desc_b        varchar(255),
   desc_c        varchar(255),
   desc_d        varchar(255),
   desc_e        varchar(255),
   desc_f        varchar(255),
   desc_g        varchar(255),
   desc_h        varchar(255),
   active        boolean,
   row_id        bigint,
   processed_on  timestamptz,
   error_type    varchar(256)
);

COMMIT;