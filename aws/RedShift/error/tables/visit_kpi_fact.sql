DROP TABLE IF EXISTS error.visit_kpi_fact CASCADE;

CREATE TABLE error.visit_kpi_fact
(
   account_id          varchar(50),
   visit_id            varchar(50),
   visit_date          timestamp,
   store_id            varchar(50),
   owner_id            varchar(50),
   analysis            varchar(255),
   kpi                 varchar(255),
   kpi_id              float8,
   own                 boolean,
   display_group       varchar(255),
   question_english    varchar(2000),
   question_local      varchar(2000),
   product_group_name  varchar(255),
   prod_manufacturer   varchar(255),
   prod_brand          varchar(255),
   shelf_level         float4,
   linear              float8,
   facings             float8,
   packages            float8,
   value               float8,
   target              float8,
   discrepancy         float8,
   compliant           boolean,
   kpi_group           varchar(255),
   question_answers    varchar(2000),
   photo_group         float8,
   photo_group_type    varchar(255),
   custom_field_a      varchar(255),
   custom_field_b      varchar(255),
   custom_field_c      varchar(255),
   custom_field_d      varchar(255),
   custom_field_e      varchar(255),
   row_id              bigint,
   processed_on        timestamptz,
   error_type          varchar(256)
);

COMMIT;