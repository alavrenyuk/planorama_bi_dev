DROP TABLE IF EXISTS error.product_kpi_fact CASCADE;

CREATE TABLE error.product_kpi_fact
(
   account_id          varchar(50),
   visit_id            varchar(50),
   product_id          varchar(50),
   store_id            varchar(50),
   owner_id            varchar(50),
   visit_date          timestamp,
   prod_ean            varchar(500),
   analysis            varchar(255),
   kpi                 varchar(255),
   own                 float4,
   currnt_stat         varchar(2000),
   value               float8,
   out_of_stock        float4,
   display_group       varchar(255),
   product_group_name  varchar(255),
   part_of_group       boolean,
   ranking             float8,
   categ_name          varchar(255),
   row_id              bigint,
   processed_on        timestamptz,
   error_type          varchar(11)
);

COMMIT;