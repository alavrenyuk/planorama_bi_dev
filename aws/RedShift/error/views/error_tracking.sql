DROP VIEW IF EXISTS error.error_tracking CASCADE;

CREATE OR REPLACE VIEW error.error_tracking
(
  tablename,
  error_numbers
)
AS 
SELECT 'error.accuracy_tracking'::character varying AS tablename, count(accuracy_tracking.account_id) AS error_numbers
   FROM error.accuracy_tracking
UNION 
 SELECT 'error.compliance_fact'::character varying AS tablename, count(compliance_fact.account_id) AS error_numbers
   FROM error.compliance_fact
UNION 
 SELECT 'error.geolocation'::character varying AS tablename, count(geolocation.account_id) AS error_numbers
   FROM error.geolocation
UNION 
 SELECT 'error.master_data_product'::character varying AS tablename, count(master_data_product.account_id) AS error_numbers
   FROM error.master_data_product
UNION 
 SELECT 'error.master_data_store'::character varying AS tablename, count(master_data_store.account_id) AS error_numbers
   FROM error.master_data_store
UNION 
 SELECT 'error.master_data_user'::character varying AS tablename, count(master_data_user.account_id) AS error_numbers
   FROM error.master_data_user
UNION 
 SELECT 'error.monitor_visit_source'::character varying AS tablename, count(monitor_visit_source.account_id) AS error_numbers
   FROM error.monitor_visit_source
UNION 
 SELECT 'error.new_time_tracking'::character varying AS tablename, count(new_time_tracking.account_id) AS error_numbers
   FROM error.new_time_tracking
UNION 
 SELECT 'error.photo_fact'::character varying AS tablename, count(photo_fact.account_id) AS error_numbers
   FROM error.photo_fact
UNION 
 SELECT 'error.product_fact'::character varying AS tablename, count(product_fact.account_id) AS error_numbers
   FROM error.product_fact
UNION 
 SELECT 'error.product_kpi_fact'::character varying AS tablename, count(product_kpi_fact.account_id) AS error_numbers
   FROM error.product_kpi_fact
UNION 
 SELECT 'error.user_feedback'::character varying AS tablename, count(user_feedback.account_id) AS error_numbers
   FROM error.user_feedback
UNION 
 SELECT 'error.visit_fact'::character varying AS tablename, count(visit_fact.account_id) AS error_numbers
   FROM error.visit_fact
UNION 
 SELECT 'error.visit_kpi_fact'::character varying AS tablename, count(visit_kpi_fact.account_id) AS error_numbers
   FROM error.visit_kpi_fact
ORDER BY 1
WITH NO SCHEMA BINDING;

COMMIT;