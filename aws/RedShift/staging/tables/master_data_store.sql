DROP TABLE IF EXISTS staging.master_data_store CASCADE;

CREATE TABLE staging.master_data_store
(
   account_id     varchar(50)      NOT NULL,
   store_id       varchar(50)      NOT NULL,
   internal_code  varchar(255),
   chainstore     varchar(255),
   description    varchar(1000),
   address        varchar(1000),
   region         varchar(255),
   city           varchar(255),
   postal_code    varchar(50),
   owners         varchar(65535),
   desc_a         varchar(255),
   desc_b         varchar(255),
   desc_c         varchar(255),
   desc_d         varchar(255),
   desc_e         varchar(255),
   desc_f         varchar(255),
   desc_g         varchar(255),
   desc_h         varchar(255),
   is_deleted     boolean,
   country        varchar(50),
   latitude       float8,
   longitude      float8,
   puzzle_mode    varchar(50),
   crea_on        timestamp,
   mod_on         timestamp
);

ALTER TABLE staging.master_data_store
   ADD CONSTRAINT pk_client_id_store_id_new
   PRIMARY KEY (account_id, store_id);

COMMIT;