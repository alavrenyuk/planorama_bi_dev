DROP TABLE IF EXISTS staging.photo_quality CASCADE;

CREATE TABLE staging.photo_quality
(
   quality_id   integer        NOT NULL,
   description  varchar(255)
);

ALTER TABLE staging.photo_quality
   ADD CONSTRAINT pk_photo_quality
   PRIMARY KEY (quality_id);

COMMIT;