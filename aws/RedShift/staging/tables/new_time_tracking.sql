DROP TABLE IF EXISTS staging.new_time_tracking CASCADE;

CREATE TABLE staging.new_time_tracking
(
   account_id                             varchar(50)    NOT NULL,
   account_name                           varchar(255),
   visit_id                               varchar(50)    NOT NULL,
   number_of_pictures                     float4,
   category_id                            varchar(50),
   category_name                          varchar(255),
   start_photo_capture                    timestamp,
   end_photo_capture                      timestamp,
   duration_photo_capture                 float4,
   visit_creation_on_server               timestamp,
   waiting_time_before_upload             float4,
   start_photo_upload                     timestamp,
   end_photo_upload                       timestamp,
   duration_upload                        float4,
   start_processing                       timestamp,
   end_processing                         timestamp,
   duration_processing                    float4,
   duration_end_upload_to_end_processing  float4,
   start_labelling                        timestamp,
   end_labelling                          timestamp,
   duration_labelling                     float4,
   start_verification                     timestamp,
   end_verification                       timestamp,
   duration_verification                  float4,
   user_id                                varchar(50),
   username                               varchar(255),
   store_id                               varchar(50),
   storecode                              varchar(50),
   storename                              varchar(255)
);

ALTER TABLE staging.new_time_tracking
   ADD CONSTRAINT pk_account_id_visit_id
   PRIMARY KEY (account_id, visit_id);

COMMIT;
