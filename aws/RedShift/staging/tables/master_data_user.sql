DROP TABLE IF EXISTS staging.master_data_user CASCADE;

CREATE TABLE staging.master_data_user
(
   account_id       varchar(50)    NOT NULL,
   user_id          varchar(255)   NOT NULL,
   username         varchar(50),
   first_name       varchar(50),
   last_name        varchar(50),
   email            varchar(255),
   phone            varchar(50),
   active           boolean,
   office           varchar(255),
   role             varchar(255),
   roles            varchar(255),
   manager          varchar(255),
   password         varchar(255),
   desc_a           varchar(255),
   desc_b           varchar(255),
   desc_c           varchar(255),
   desc_d           varchar(255),
   desc_e           varchar(255),
   desc_f           varchar(255),
   desc_g           varchar(255),
   desc_h           varchar(255),
   crea_on          timestamp,
   mod_on           timestamp,
   last_logon_date  timestamp
);

ALTER TABLE staging.master_data_user
   ADD CONSTRAINT pk_client_id_user_id
   PRIMARY KEY (account_id, user_id);

COMMIT;
