DROP TABLE IF EXISTS staging.user_feedback CASCADE;

CREATE TABLE staging.user_feedback
(
   account_id                  varchar(50)    NOT NULL,
   visit_id                    varchar(50)    NOT NULL,
   visit_date                  timestamp,
   analysis_date               timestamp,
   feedback_registration_date  timestamp,
   category_code               varchar(50),
   store_code                  varchar(50),
   store_name                  varchar(255),
   owner_id                    varchar(50),
   owner_name                  varchar(255),
   kpi_id                      varchar(10),
   kpi                         varchar(50),
   product_ean                 varchar(500)   NOT NULL,
   description                 varchar(255),
   toggle1                     varchar(2000),
   toggle2                     varchar(2000),
   toggle3                     varchar(2000),
   toggle4                     varchar(2000),
   toggle5                     varchar(2000),
   status_after_verif          varchar(10)
);

ALTER TABLE staging.user_feedback
   ADD CONSTRAINT pk_user_feedback
   PRIMARY KEY (account_id, visit_id, product_ean);

COMMIT;