DROP TABLE IF EXISTS staging.master_data_user_hierarchy CASCADE;

CREATE TABLE staging.master_data_user_hierarchy
(
   account_id  varchar(50),
   username    varchar(50),
   parent      varchar(50)
);

COMMIT;