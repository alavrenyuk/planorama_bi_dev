DROP TABLE IF EXISTS staging.master_data_product CASCADE;

CREATE TABLE staging.master_data_product
(
   account_id    varchar(50)    NOT NULL,
   base          varchar(255),
   product_ean   varchar(50),
   product_id    varchar(50)    NOT NULL,
   description   varchar(255),
   manufacturer  varchar(255),
   brand         varchar(255),
   height        float8,
   width         float8,
   depth         float8,
   code          varchar(255),
   crea_on       timestamp,
   mod_on        timestamp,
   seg1          varchar(255),
   seg2          varchar(255),
   seg3          varchar(255),
   seg4          varchar(255),
   seg5          varchar(255),
   desc_a        varchar(255),
   desc_b        varchar(255),
   desc_c        varchar(255),
   desc_d        varchar(255),
   desc_e        varchar(255),
   desc_f        varchar(255),
   desc_g        varchar(255),
   desc_h        varchar(255),
   desc_i        varchar(255),
   desc_j        varchar(255),
   desc_k        varchar(255),
   desc_l        varchar(255),
   desc_m        varchar(255),
   desc_n        varchar(255),
   desc_o        varchar(255),
   desc_p        varchar(255),
   desc_q        varchar(255),
   desc_r        varchar(255),
   desc_s        varchar(255),
   desc_t        varchar(255),
   desc_u        varchar(255),
   desc_v        varchar(255),
   desc_w        varchar(255),
   desc_x        varchar(255),
   desc_y        varchar(255),
   desc_z        varchar(255),
   kind          integer,
   labelling     varchar(255),
   is_validated  boolean,
   excl_from     boolean,
   img           integer,
   is_deleted    boolean
);

ALTER TABLE staging.master_data_product
   ADD CONSTRAINT pk_client_id_product_id
   PRIMARY KEY (account_id, product_id);

COMMIT;