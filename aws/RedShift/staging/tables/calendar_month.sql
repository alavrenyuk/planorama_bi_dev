DROP TABLE IF EXISTS staging.calendar_month CASCADE;

CREATE TABLE staging.calendar_month
(
   month_no    integer       NOT NULL,
   lang_short  char(3)       NOT NULL,
   month_name  varchar(50)   NOT NULL
);

ALTER TABLE staging.calendar_month
   ADD CONSTRAINT pk_month
   PRIMARY KEY (month_no, lang_short);

COMMIT;