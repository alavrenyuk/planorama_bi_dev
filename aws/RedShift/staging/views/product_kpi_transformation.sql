DROP VIEW IF EXISTS staging.product_kpi_transformation CASCADE;

CREATE OR REPLACE VIEW staging.product_kpi_transformation 
(
  account_id,
  visit_id,
  product_id,
  store_id,
  owner_id,
  visit_date,
  prod_ean,
  analysis,
  kpi,
  own,
  currnt_stat,
  value,
  out_of_stock,
  display_group,
  product_group_name,
  part_of_group,
  ranking,
  categ_name,
  first_loaded_on,
  last_modified_on
)
AS
SELECT a.account_id,
       a.visit_id,
       a.product_id,
       a.store_id,
       a.owner_id,
       a.visit_date,
       a.prod_ean,
       a.analysis,
       a.kpi,
       a.own,
       a.currnt_stat,
       a.value,
       a.out_of_stock,
       a.display_group,
       a.product_group_name,
       a.part_of_group,
       a.ranking,
       a.categ_name,
       a.first_loaded_on,
       a.last_modified_on
FROM (SELECT new_analysis.account_id,
             new_analysis.visit_id,
             new_analysis.product_id,
             new_analysis.store_id,
             new_analysis.owner_id,
             new_analysis.visit_date,
             new_analysis.prod_ean,
             new_analysis.analysis,
             new_analysis.kpi,
             CASE
               WHEN new_analysis.own::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE new_analysis.own
             END AS own,
             CASE
               WHEN new_analysis.analysis = 'group_presence' AND new_analysis.compliant = 1 THEN 'present'
               WHEN new_analysis.analysis = 'group_presence' AND new_analysis.compliant = 0 THEN 'oos'
               ELSE new_analysis.currnt_stat
             END AS currnt_stat,
             CASE
               WHEN new_analysis.value::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               WHEN new_analysis.analysis = 'group_presence' THEN new_analysis.compliant::DOUBLE precision
               ELSE new_analysis.value
             END AS value,
             CASE
               WHEN new_analysis.out_of_stock::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               WHEN new_analysis.analysis = 'group_presence' AND new_analysis.compliant = 1 THEN 0
               WHEN new_analysis.analysis = 'group_presence' AND new_analysis.compliant = 0 THEN 1
               ELSE new_analysis.out_of_stock
             END AS out_of_stock,
             new_analysis.display_group,
             new_analysis.product_group_name,
             CASE
               WHEN new_analysis.analysis = 'group_presence' AND TRIM(new_analysis.product_group_name) IS NOT NULL THEN 1::INTEGER::BOOLEAN
               WHEN new_analysis.analysis = 'group_presence' AND TRIM(new_analysis.product_group_name) IS NULL THEN 0::INTEGER::BOOLEAN
               ELSE NULL::BOOLEAN
             END AS part_of_group,
             new_analysis.ranking,
             new_analysis.categ_name,
             null::timestamptz as first_loaded_on,
             current_timestamp as last_modified_on,
             pg_catalog.row_number() OVER (PARTITION BY new_analysis.account_id,new_analysis.visit_id,new_analysis.analysis,new_analysis.product_id,new_analysis.kpi) AS "row"
      FROM staging.new_analysis
      WHERE new_analysis.analysis_date = ((SELECT "max"(na1.analysis_date) AS "max"
                                           FROM staging.new_analysis na1
                                           WHERE na1.account_id::TEXT = new_analysis.account_id::TEXT
                                           AND   na1.visit_id::TEXT = new_analysis.visit_id::TEXT
                                           AND   na1.visit_date = new_analysis.visit_date
                                           AND   na1.analysis::TEXT = new_analysis.analysis::TEXT
                                           AND   na1.product_id::TEXT = new_analysis.product_id::TEXT
                                           AND   na1.kpi::TEXT = new_analysis.kpi::TEXT))
      AND   (new_analysis.analysis::TEXT = 'present_extra_sku'::CHARACTER VARYING::TEXT 
      OR new_analysis.analysis = 'group_presence'
      OR new_analysis.kpi::TEXT = 'top_sku'::CHARACTER VARYING::TEXT 
      OR new_analysis.kpi::TEXT = 'star_sku'::CHARACTER VARYING::TEXT 
      OR new_analysis.kpi::TEXT = 'core_sku'::CHARACTER VARYING::TEXT 
      OR new_analysis.kpi::TEXT = 'npd'::CHARACTER VARYING::TEXT)
      AND   new_analysis.product_id::TEXT <> ''::CHARACTER VARYING::TEXT
      AND   LENGTH(BTRIM(new_analysis.account_id::TEXT)) > 0
      AND   LENGTH(BTRIM(new_analysis.visit_id::TEXT)) > 0
      AND   LENGTH(BTRIM(new_analysis.analysis::TEXT)) > 0
      AND   LENGTH(BTRIM(new_analysis.product_id::TEXT)) > 0
      AND   LENGTH(BTRIM(new_analysis.kpi::TEXT)) > 0) a
WHERE a."row" = 1
WITH NO SCHEMA BINDING;
COMMIT;