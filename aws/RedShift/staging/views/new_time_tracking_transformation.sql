DROP VIEW IF EXISTS staging.new_time_tracking_transformation CASCADE;

CREATE OR REPLACE VIEW staging.new_time_tracking_transformation
(
  account_id,
  visit_id,
  number_of_pictures,
  category_id,
  category_name,
  start_photo_capture,
  end_photo_capture,
  duration_photo_capture,
  visit_creation_on_server,
  waiting_time_before_upload,
  start_photo_upload,
  end_photo_upload,
  duration_upload,
  start_processing,
  end_processing,
  duration_processing,
  duration_end_upload_to_end_processing,
  start_labelling,
  end_labelling,
  duration_labelling,
  start_verification,
  end_verification,
  duration_verification,
  ptt,
  first_loaded_on,
  last_modified_on
)
AS 
 SELECT 
   a.account_id, 
   a.visit_id, 
   a.number_of_pictures, 
   a.category_id, 
   a.category_name, 
   a.start_photo_capture, 
   a.end_photo_capture, 
   a.duration_photo_capture, 
   a.visit_creation_on_server, 
   a.waiting_time_before_upload, 
   a.start_photo_upload, 
   a.end_photo_upload, 
   a.duration_upload, 
   a.start_processing, 
   a.end_processing, 
   a.duration_processing, 
   a.duration_end_upload_to_end_processing, 
   a.start_labelling, 
   a.end_labelling, 
   a.duration_labelling, 
   a.start_verification, 
   a.end_verification, 
   a.duration_verification, 
   a.ptt,
   a.first_loaded_on,
   a.last_modified_on
   FROM ( SELECT new_time_tracking.account_id, 
          new_time_tracking.visit_id, 
          new_time_tracking.number_of_pictures, 
          new_time_tracking.category_id, 
          new_time_tracking.category_name, 
          new_time_tracking.start_photo_capture, 
          new_time_tracking.end_photo_capture, 
          new_time_tracking.duration_photo_capture, 
          new_time_tracking.visit_creation_on_server, 
          new_time_tracking.waiting_time_before_upload, 
          new_time_tracking.start_photo_upload, 
          new_time_tracking.end_photo_upload, 
          new_time_tracking.duration_upload, 
          new_time_tracking.start_processing, 
          new_time_tracking.end_processing, 
          new_time_tracking.duration_processing, 
          new_time_tracking.duration_end_upload_to_end_processing, 
          new_time_tracking.start_labelling, 
          new_time_tracking.end_labelling, 
          new_time_tracking.duration_labelling, 
          new_time_tracking.start_verification, 
          new_time_tracking.end_verification, 
          new_time_tracking.duration_verification, 
          new_time_tracking.duration_end_upload_to_end_processing + new_time_tracking.duration_photo_capture AS ptt,
          null::timestamptz as first_loaded_on,
          current_timestamp as last_modified_on,
          pg_catalog.row_number()
          OVER( 
          PARTITION BY new_time_tracking.account_id, new_time_tracking.visit_id
          ORDER BY new_time_tracking.visit_creation_on_server DESC) AS row_id
           FROM staging.new_time_tracking
          WHERE length(btrim(new_time_tracking.account_id::text)) > 0 AND length(btrim(new_time_tracking.visit_id::text)) > 0) a
  WHERE a.row_id = 1
  WITH NO SCHEMA BINDING;

COMMIT;