DROP VIEW IF EXISTS staging.geolocation_transformation_err CASCADE;

CREATE OR REPLACE VIEW staging.geolocation_transformation_err 
(
  account_id,
  visit_id,
  visit_date,
  owner_id,
  store_code,
  number_of_pictures,
  photo_index,
  store_latitude,
  store_longitude,
  photo_latitude,
  photo_longitude,
  location_accuracy,
  suspicious_photo,
  photo_distance_tolerance,
  location_difference,
  row_id,
  processed_on,
  error_type
)
AS
SELECT a.account_id,
       a.visit_id,
       a.visit_date,
       a.owner_id,
       a.store_code,
       a.number_of_pictures,
       a.photo_index,
       a.store_latitude,
       a.store_longitude,
       a.photo_latitude,
       a.photo_longitude,
       a.location_accuracy,
       a.suspicious_photo,
       a.photo_distance_tolerance,
       a.location_difference,
       a.row_id,
       a.processed_on,
       a.error_type
FROM (SELECT geolocation.account_id::varchar(255),
             geolocation.visit_id::varchar(255),
             geolocation.visit_date,
             geolocation.owner_id::varchar(255),
             geolocation.store_code::varchar(255),
             geolocation.number_of_pictures,
             geolocation.photo_index,
             geolocation.store_latitude,
             geolocation.store_longitude,
             geolocation.photo_latitude,
             geolocation.photo_longitude,
             geolocation.location_accuracy,
             geolocation.suspicious_photo::varchar(255),
             geolocation.photo_distance_tolerance,
             geolocation.location_difference,
             pg_catalog.row_number() OVER (PARTITION BY geolocation.account_id,geolocation.visit_id,geolocation.photo_index) AS row_id,
             'now'::CHARACTER VARYING::TIMESTAMP WITH TIME ZONE AS processed_on,
             'duplicate'::CHARACTER VARYING AS error_type
      FROM staging.geolocation) a
WHERE a.row_id > 1
UNION
SELECT geolocation.account_id::varchar(255),
             geolocation.visit_id::varchar(255),
             geolocation.visit_date,
             geolocation.owner_id::varchar(255),
             geolocation.store_code::varchar(255),
             geolocation.number_of_pictures,
             geolocation.photo_index,
             geolocation.store_latitude,
             geolocation.store_longitude,
             geolocation.photo_latitude,
             geolocation.photo_longitude,
             geolocation.location_accuracy,
             geolocation.suspicious_photo::varchar(255),
             geolocation.photo_distance_tolerance,
             geolocation.location_difference,
             pg_catalog.row_number() OVER (PARTITION BY geolocation.account_id,geolocation.visit_id,geolocation.photo_index) AS row_id,
             'now'::CHARACTER VARYING::TIMESTAMP WITH TIME ZONE AS processed_on,
             'missing key'::CHARACTER VARYING AS error_type
      FROM staging.geolocation
      WHERE geolocation.photo_index is null
WITH NO SCHEMA BINDING;

COMMIT;