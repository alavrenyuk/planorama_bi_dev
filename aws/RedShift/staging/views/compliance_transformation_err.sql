DROP VIEW IF EXISTS staging.compliance_transformation_err CASCADE;

CREATE OR REPLACE VIEW staging.compliance_transformation_err 
(
  account_id,
  visit_id,
  kpi,
  kpi_id,
  value,
  compliant,
  threshold,
  weight,
  field,
  group_name,
  parent,
  field_value,
  force_na,
  row_id,
  processed_on,
  error_type
)
AS
SELECT a.account_id,
       a.visit_id,
       a.kpi,
       a.kpi_id,
       a.value,
       a.compliant,
       a.threshold,
       a.weight,
       a.field,
       a.group_name,
       a.parent,
       a.field_value,
       a.force_na,
       a.row_id,
       'now'::CHARACTER VARYING::TIMESTAMP WITH TIME zone AS processed_on,
       'duplicate'::CHARACTER VARYING AS error_type
FROM (SELECT new_analysis.account_id::varchar(50),
             new_analysis.visit_id::varchar(50),
             new_analysis.kpi::varchar(255),
             CASE
               WHEN new_analysis.kpi_id::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE new_analysis.kpi_id
             END AS kpi_id,
             CASE
               WHEN new_analysis.value::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE new_analysis.value
             END AS value,
             CASE
               WHEN new_analysis.compliant::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::BOOLEAN
               ELSE new_analysis.compliant::BIGINT::BOOLEAN
             END AS compliant,
             CASE
               WHEN new_analysis.threshold::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE new_analysis.threshold
             END AS threshold,
             CASE
               WHEN new_analysis.weight::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE new_analysis.weight
             END AS weight,
             new_analysis.field::varchar(255),
             new_analysis.group_name::varchar(255),
             new_analysis.parent::varchar(255),
             new_analysis.field_value::varchar(255),
             CASE
               WHEN new_analysis.force_na::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::BOOLEAN
               ELSE new_analysis.force_na::BIGINT::BOOLEAN
             END AS force_na,
             pg_catalog.row_number() OVER (PARTITION BY new_analysis.account_id,new_analysis.visit_id,new_analysis.kpi_id) AS row_id
      FROM staging.new_analysis
      WHERE new_analysis.analysis_date = ((SELECT "max"(na1.analysis_date) AS "max"
                                           FROM staging.new_analysis na1
                                           WHERE na1.account_id::TEXT = new_analysis.account_id::TEXT
                                           AND   na1.visit_id::TEXT = new_analysis.visit_id::TEXT
                                           AND   na1.visit_date = new_analysis.visit_date
                                           AND   na1.kpi_id = new_analysis.kpi_id))
      AND   new_analysis.analysis::TEXT = 'compliance_per_group'::CHARACTER VARYING::TEXT) a
WHERE a.row_id > 1
UNION
SELECT new_analysis.account_id,
       new_analysis.visit_id,
       new_analysis.kpi,
       CASE
         WHEN new_analysis.kpi_id::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.kpi_id
       END AS kpi_id,
       CASE
         WHEN new_analysis.value::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.value
       END AS value,
       CASE
         WHEN new_analysis.compliant::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::BOOLEAN
         ELSE new_analysis.compliant::BIGINT::BOOLEAN
       END AS compliant,
       CASE
         WHEN new_analysis.threshold::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.threshold
       END AS threshold,
       CASE
         WHEN new_analysis.weight::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.weight
       END AS weight,
       new_analysis.field,
       new_analysis.group_name,
       new_analysis.parent,
       new_analysis.field_value,
       CASE
         WHEN new_analysis.force_na::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::BOOLEAN
         ELSE new_analysis.force_na::BIGINT::BOOLEAN
       END AS force_na,
       pg_catalog.row_number() OVER (PARTITION BY new_analysis.account_id,new_analysis.visit_id,new_analysis.kpi_id) AS row_id,
       'now'::CHARACTER VARYING::TIMESTAMP WITH TIME zone AS processed_on,
       'missing key'::CHARACTER VARYING AS error_type
FROM staging.new_analysis
WHERE new_analysis.analysis::TEXT = 'compliance_per_group'::CHARACTER VARYING::TEXT
AND   (LENGTH(BTRIM(new_analysis.account_id::TEXT)) = 0 OR LENGTH(BTRIM(new_analysis.visit_id::TEXT)) = 0 OR LENGTH(BTRIM(new_analysis.kpi_id::CHARACTER VARYING::TEXT)) = 0)
WITH NO SCHEMA BINDING;

COMMIT;