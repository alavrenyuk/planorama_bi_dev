DROP VIEW IF EXISTS staging.user_feedback_transformation CASCADE;

CREATE OR REPLACE VIEW staging.user_feedback_transformation
(
  account_id,
  visit_id,
  product_ean,
  feedback_registration_date,
  kpi_id,
  kpi,
  toggle1,
  status_after_verif,
  first_loaded_on,
  last_modified_on
)
AS 
 SELECT 
   a.account_id, 
   a.visit_id, 
   a.product_ean, 
   a.feedback_registration_date, 
   a.kpi_id, 
   a.kpi, 
   a.toggle1, 
   a.status_after_verif,
   a.first_loaded_on,
   a.last_modified_on
   FROM ( SELECT 
          user_feedback.account_id, 
          user_feedback.visit_id, 
          user_feedback.product_ean, 
          user_feedback.feedback_registration_date, 
          user_feedback.kpi_id, 
          user_feedback.kpi, 
          left(user_feedback.toggle1,50) as toggle1, 
          user_feedback.status_after_verif, 
          null::timestamptz as first_loaded_on,
          current_timestamp as last_modified_on,         
          pg_catalog.row_number()
          OVER( 
          PARTITION BY user_feedback.account_id, user_feedback.visit_id, user_feedback.product_ean, user_feedback.kpi_id
          ORDER BY user_feedback.feedback_registration_date DESC) AS row_id
          FROM staging.user_feedback) a
  WHERE a.row_id = 1
WITH NO SCHEMA BINDING;

COMMIT;