DROP VIEW IF EXISTS staging.product_transformation_err CASCADE;

CREATE OR REPLACE VIEW staging.product_transformation_err 
(
  account_id,
  visit_id,
  owner_id,
  store_id,
  product_id,
  visit_date,
  photo_index,
  section_index,
  photo_group,
  photo_group_type,
  shelf_level,
  horizontal_position_index,
  vertical_position,
  categ_code,
  categ_name,
  ranking,
  facings_tgt,
  own,
  currnt_stat,
  value,
  out_of_stock,
  linear,
  facings,
  packages,
  is_promo,
  price,
  row_id,
  processed_on,
  error_type
)
AS
SELECT new_analysis.account_id,
       new_analysis.visit_id,
       new_analysis.owner_id,
       new_analysis.store_id,
       new_analysis.product_id,
       new_analysis.visit_date::DATE AS DATE,
       new_analysis.photo_index,
       new_analysis.section_index,
       new_analysis.photo_group,
       new_analysis.photo_group_type,
       new_analysis.shelf_level,
       new_analysis.horizontal_position_index,
       new_analysis.vertical_position,
       new_analysis.categ_code,
       new_analysis.categ_name,
       CASE
         WHEN new_analysis.ranking::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.ranking
       END AS ranking,
       CASE
         WHEN new_analysis.facings_tgt::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.facings_tgt
       END AS facings_tgt,
       CASE
         WHEN new_analysis.own::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE new_analysis.own
       END AS own,
       new_analysis.currnt_stat,
       CASE
         WHEN new_analysis.value::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.value
       END AS value,
       CASE
         WHEN new_analysis.out_of_stock::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE new_analysis.out_of_stock
       END AS out_of_stock,
       CASE
         WHEN new_analysis.linear::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.linear
       END AS linear,
       CASE
         WHEN new_analysis.facings::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.facings
       END AS facings,
       CASE
         WHEN new_analysis.packages::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.packages
       END AS packages,
       CASE
         WHEN new_analysis.promo::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::BOOLEAN
         ELSE new_analysis.promo::INTEGER::BOOLEAN
       END AS is_promo,
       CASE
         WHEN new_analysis.price::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.price
       END AS price,
       pg_catalog.row_number() OVER (PARTITION BY new_analysis.account_id,new_analysis.visit_id,new_analysis.product_id,new_analysis.photo_index,new_analysis.section_index,new_analysis.shelf_level,new_analysis.horizontal_position_index,new_analysis.vertical_position) AS row_id,
       'now'::CHARACTER VARYING::TIMESTAMP WITH TIME zone AS processed_on,
       'missing key'::CHARACTER VARYING AS error_type
FROM staging.new_analysis
WHERE new_analysis.analysis::TEXT = 'raw_data'::CHARACTER VARYING::TEXT
AND   (LENGTH(BTRIM(new_analysis.account_id::TEXT)) = 0 OR LENGTH(BTRIM(new_analysis.visit_id::TEXT)) = 0 OR LENGTH(BTRIM(new_analysis.product_id::TEXT)) = 0)
WITH NO SCHEMA BINDING;

COMMIT;