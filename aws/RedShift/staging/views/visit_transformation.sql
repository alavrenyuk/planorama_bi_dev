DROP VIEW IF EXISTS staging.visit_transformation CASCADE;

CREATE OR REPLACE VIEW staging.visit_transformation 
(
  account_id,
  visit_id,
  visit_date,
  visit_datetime,
  analysis_date,
  categ_code,
  categ_name,
  owner_id,
  store_id,
  number_of_pictures,
  number_of_sections,
  process_type,
  user_comment,
  plan_tag,
  first_loaded_on,
  last_modified_on
)
AS
SELECT DISTINCT new_analysis.account_id,
       new_analysis.visit_id,
       new_analysis.visit_date::DATE AS visit_date,
       new_analysis.visit_date::TIMESTAMPTZ as visit_datetime,
       new_analysis.analysis_date,
       new_analysis.categ_code,
       new_analysis.categ_name,
       new_analysis.owner_id,
       new_analysis.store_id,
       new_analysis.number_of_pictures,
       new_analysis.number_of_sections,
       new_analysis.process_type,
       LEFT (new_analysis.user_comment,1000)::VARCHAR(1000),
       new_analysis.plan_tag,
       NULL::TIMESTAMPTZ AS first_loaded_on,
       CURRENT_TIMESTAMP AS last_modified_on
FROM staging.new_analysis
WHERE new_analysis.analysis_date = ((SELECT "max"(na1.analysis_date) AS "max"
                                     FROM staging.new_analysis na1
                                     WHERE na1.account_id::TEXT = new_analysis.account_id::TEXT
                                     AND   na1.visit_id::TEXT = new_analysis.visit_id::TEXT
                                     AND   na1.visit_date = new_analysis.visit_date))
WITH NO SCHEMA BINDING;

COMMIT;