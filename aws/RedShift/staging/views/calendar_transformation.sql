DROP VIEW IF EXISTS staging.calendar_transformation CASCADE;

CREATE OR REPLACE VIEW staging.calendar_transformation 
(
  account_id,
  visit_date,
  year,
  month_no,
  month_name,
  week,
  week_no,
  year_week,
  quarter,
  quarter_no,
  past_year,
  past_month_no,
  past_week_no,
  past_quarter_no,
  first_loaded_on,
  last_modified_on
)
AS
SELECT derived_table1.account_id::varchar(50),
       derived_table1.visit_date_key::date AS visit_date,
       "date_part"('year'::CHARACTER VARYING::TEXT,derived_table1.visit_date_key)::integer AS "year",
       "date_part"('month'::CHARACTER VARYING::TEXT,derived_table1.visit_date_key)::integer AS month_no,
       (SELECT calendar_month.month_name
        FROM staging.calendar_month
        WHERE calendar_month.lang_short = 'eng'::bpchar
        AND   calendar_month.month_no = "date_part"('month'::CHARACTER VARYING::TEXT,derived_table1.visit_date_key))::varchar(20) AS month_name,
       ('Week '::CHARACTER VARYING::TEXT || "date_part"('week'::CHARACTER VARYING::TEXT,derived_table1.visit_date_key)::CHARACTER VARYING::TEXT)::varchar(20) AS week,
       "date_part"('week'::CHARACTER VARYING::TEXT,derived_table1.visit_date_key)::integer AS week_no,
       (("date_part"('year'::CHARACTER VARYING::TEXT,derived_table1.visit_date_key)::CHARACTER VARYING::TEXT || ' Week '::CHARACTER VARYING::TEXT) || "date_part"('week'::CHARACTER VARYING::TEXT,derived_table1.visit_date_key)::CHARACTER VARYING::TEXT)::varchar(20) AS year_week,
       ('Qtr '::CHARACTER VARYING::TEXT || "date_part"('quarter'::CHARACTER VARYING::TEXT,derived_table1.visit_date_key)::CHARACTER VARYING::TEXT)::varchar(10) AS quarter,
       ("date_part"('quarter'::CHARACTER VARYING::TEXT,derived_table1.visit_date_key))::integer AS quarter_no,
       ("date_part"('year'::CHARACTER VARYING::TEXT,date_add ('year'::CHARACTER VARYING::TEXT,- 1::BIGINT,derived_table1.visit_date_key::TIMESTAMP without TIME zone)))::integer AS past_year,
       ("date_part"('month'::CHARACTER VARYING::TEXT,date_add ('month'::CHARACTER VARYING::TEXT,- 1::BIGINT,derived_table1.visit_date_key::TIMESTAMP without TIME zone)))::integer AS past_month_no,
       ("date_part"('week'::CHARACTER VARYING::TEXT,date_add ('week'::CHARACTER VARYING::TEXT,- 1::BIGINT,derived_table1.visit_date_key::TIMESTAMP without TIME zone)))::integer AS past_week_no,
       ("date_part"('quarter'::CHARACTER VARYING::TEXT,date_add ('quarter'::CHARACTER VARYING::TEXT,- 1::BIGINT,derived_table1.visit_date_key::TIMESTAMP without TIME zone)))::integer AS past_quarter_no,
       NULL::TIMESTAMPTZ AS first_loaded_on,
       CURRENT_TIMESTAMP AS last_modified_on
FROM (SELECT DISTINCT new_analysis.account_id,
             DATE (new_analysis.visit_date) AS visit_date_key,
             "date_part"('year'::CHARACTER VARYING::TEXT,new_analysis.visit_date) AS "year"
      FROM staging.new_analysis) derived_table1
ORDER BY derived_table1.visit_date_key 
WITH NO SCHEMA BINDING;
COMMIT;