DROP VIW IF EXISTS staging.compliance_transformation CASCADE;

CREATE OR REPLACE VIEW staging.compliance_transformation 
(
  account_id,
  visit_id,
  kpi,
  kpi_id,
  value,
  compliant,
  threshold,
  weight,
  field,
  group_name,
  parent,
  field_value,
  force_na,
  first_loaded_on,
  last_modified_on
)
AS
SELECT a.account_id,
       a.visit_id,
       a.kpi,
       a.kpi_id,
       a.value,
       a.compliant,
       a.threshold,
       a.weight,
       a.field,
       a.group_name,
       a.parent,
       a.field_value,
       a.force_na,
       a.first_loaded_on,
       a.last_modified_on
FROM (SELECT new_analysis.account_id::varchar(50),
             new_analysis.visit_id::varchar(50),
             new_analysis.kpi::varchar(255),
             CASE
               WHEN new_analysis.kpi_id::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE new_analysis.kpi_id
             END AS kpi_id,
             CASE
               WHEN new_analysis.value::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE new_analysis.value
             END AS value,
             CASE
               WHEN new_analysis.compliant::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::BOOLEAN
               ELSE new_analysis.compliant::BIGINT::BOOLEAN
             END AS compliant,
             CASE
               WHEN new_analysis.threshold::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE new_analysis.threshold
             END AS threshold,
             CASE
               WHEN new_analysis.weight::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE new_analysis.weight
             END AS weight,
             new_analysis.field::varchar(255),
             new_analysis.group_name::varchar(255),
             new_analysis.parent::varchar(255),
             new_analysis.field_value::varchar(255),
             CASE
               WHEN new_analysis.force_na::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::BOOLEAN
               ELSE new_analysis.force_na::BIGINT::BOOLEAN
             END AS force_na,
             NULL::TIMESTAMPTZ AS first_loaded_on,
             CURRENT_TIMESTAMP AS last_modified_on,
             pg_catalog.row_number() OVER (PARTITION BY new_analysis.account_id,new_analysis.visit_id,new_analysis.product_id,new_analysis.kpi_id) AS "row"
      FROM staging.new_analysis
      WHERE new_analysis.analysis::TEXT = 'compliance_per_group'::CHARACTER VARYING::TEXT
      AND   new_analysis.analysis_date = ((SELECT "max"(na1.analysis_date) AS "max"
                                           FROM staging.new_analysis na1
                                           WHERE na1.account_id::TEXT = new_analysis.account_id::TEXT
                                           AND   na1.visit_id::TEXT = new_analysis.visit_id::TEXT
                                           AND   na1.kpi_id = new_analysis.kpi_id
                                           AND   na1.visit_date = new_analysis.visit_date))) a
WHERE a."row" = 1
WITH NO SCHEMA BINDING;

COMMIT;