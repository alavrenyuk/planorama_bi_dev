DROP VIEW IF EXISTS staging.master_user_transformation CASCADE;

CREATE OR REPLACE VIEW staging.master_user_transformation 
(
  account_id,
  user_id,
  username,
  first_name,
  last_name,
  email,
  role,
  roles,
  manager,
  password,
  desc_a,
  desc_b,
  desc_c,
  desc_d,
  desc_e,
  desc_f,
  desc_g,
  desc_h,
  active,
  first_loaded_on,
  last_modified_on
)
AS
SELECT a.account_id,
       a.user_id,
       a.username,
       a.first_name,
       a.last_name,
       a.email,
       a.role,
       a.roles,
       a.manager,
       a."password",
       a.desc_a,
       a.desc_b,
       a.desc_c,
       a.desc_d,
       a.desc_e,
       a.desc_f,
       a.desc_g,
       a.desc_h,
       a.active,
       a.first_loaded_on,
       a.last_modified_on
FROM (SELECT master_data_user.account_id::varchar(50),
             master_data_user.user_id::varchar(255),
             master_data_user.username::varchar(50),
             master_data_user.first_name::varchar(50),
             master_data_user.last_name::varchar(50),
             master_data_user.email::varchar(255),
             master_data_user.role::varchar(255),
             master_data_user.roles::varchar(255),
             master_data_user.manager::varchar(255),
             master_data_user."password"::varchar(255),
             master_data_user.desc_a::varchar(255),
             master_data_user.desc_b::varchar(255),
             master_data_user.desc_c::varchar(255),
             master_data_user.desc_d::varchar(255),
             master_data_user.desc_e::varchar(255),
             master_data_user.desc_f::varchar(255),
             master_data_user.desc_g::varchar(255),
             master_data_user.desc_h::varchar(255),
             master_data_user.active,
             NULL::TIMESTAMPTZ AS first_loaded_on,
             CURRENT_TIMESTAMP AS last_modified_on,
             pg_catalog.row_number() OVER (PARTITION BY master_data_user.account_id,master_data_user.user_id) AS "row"
      FROM staging.master_data_user
      WHERE LENGTH(BTRIM(master_data_user.account_id::TEXT)) > 0
      AND   LENGTH(BTRIM(master_data_user.user_id::TEXT)) > 0) a
WHERE a."row" = 1
WITH NO SCHEMA BINDING;

COMMIT;