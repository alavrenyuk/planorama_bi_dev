DROP VIEW IF EXISTS staging.user_feedback_transformation_err CASCADE;

CREATE OR REPLACE VIEW staging.user_feedback_transformation_err
(
  account_id,
  visit_id,
  visit_date,
  analysis_date,
  feedback_registration_date,
  category_code,
  store_code,
  owner_id,
  kpi_id,
  kpi,
  product_ean,
  toggle1,
  toggle2,
  toggle3,
  toggle4,
  toggle5,
  status_after_verif,
  row_id,
  processed_on,
  error_type
)
AS 
 SELECT a.account_id, a.visit_id, a.visit_date, a.analysis_date, a.feedback_registration_date, a.category_code, a.store_code, a.owner_id, a.kpi_id, a.kpi, a.product_ean, a.toggle1, a.toggle2, a.toggle3, a.toggle4, a.toggle5, a.status_after_verif, a.row_id, 'now'::character varying::timestamp with time zone AS processed_on, 'duplicate'::character varying AS error_type
   FROM ( SELECT user_feedback.account_id, user_feedback.visit_id, user_feedback.visit_date, user_feedback.analysis_date, user_feedback.feedback_registration_date, user_feedback.category_code, user_feedback.store_code, user_feedback.owner_id, user_feedback.kpi_id, user_feedback.kpi, user_feedback.product_ean, user_feedback.toggle1, user_feedback.toggle2, user_feedback.toggle3, user_feedback.toggle4, user_feedback.toggle5, user_feedback.status_after_verif, pg_catalog.row_number()
          OVER( 
          PARTITION BY user_feedback.account_id, user_feedback.visit_id, user_feedback.product_ean, user_feedback.kpi_id
          ORDER BY user_feedback.feedback_registration_date DESC) AS row_id
           FROM staging.user_feedback) a
  WHERE a.row_id > 1
UNION 
 SELECT user_feedback.account_id, user_feedback.visit_id, user_feedback.visit_date, user_feedback.analysis_date, user_feedback.feedback_registration_date, user_feedback.category_code, user_feedback.store_code, user_feedback.owner_id, user_feedback.kpi_id, user_feedback.kpi, user_feedback.product_ean, user_feedback.toggle1, user_feedback.toggle2, user_feedback.toggle3, user_feedback.toggle4, user_feedback.toggle5, user_feedback.status_after_verif, pg_catalog.row_number()
  OVER( 
  PARTITION BY user_feedback.account_id, user_feedback.visit_id, user_feedback.product_ean, user_feedback.kpi_id
  ORDER BY user_feedback.feedback_registration_date DESC) AS row_id, 'now'::character varying::timestamp with time zone AS processed_on, 'missing key'::character varying AS error_type
   FROM staging.user_feedback
  WHERE length(btrim(user_feedback.account_id::text)) = 0 OR length(btrim(user_feedback.visit_id::text)) = 0 OR length(btrim(user_feedback.product_ean::text)) = 0 OR length(btrim(user_feedback.kpi_id::text)) = 0
WITH NO SCHEMA BINDING;

COMMIT;