DROP VIEW IF EXISTS staging.accuracy_visit_transformation CASCADE;

CREATE OR REPLACE VIEW staging.accuracy_visit_transformation 
(
  account_id,
  visit_id,
  number_of_pictures,
  created_on,
  audit_operator,
  total_present_sku,
  total_present_sku_correct,
  own_present_sku,
  own_present_sku_correct,
  total_present_facings,
  total_present_facings_correct,
  own_present_facings,
  own_present_facings_correct,
  audit_total_sku,
  audit_total_sku_correct,
  audit_own_sku,
  audit_own_sku_correct,
  audit_total_facing,
  audit_total_facing_correct,
  audit_own_facing,
  audit_own_facing_correct,
  audit_precision_sku,
  audit_precision_sku_own,
  audit_precision_facing,
  audit_precision_facing_own,
  audit_recall_sku,
  audit_recall_sku_own,
  audit_recall_facing,
  audit_recall_facing_own,
  first_loaded_on,
  last_modified_on
)
AS
SELECT accuracy_tracking.account_id::varchar(50),
       accuracy_tracking.visit_id::varchar(50),
       accuracy_tracking.number_of_pictures::float4,
       TO_TIMESTAMP((accuracy_tracking.creation_date::CHARACTER VARYING::TEXT || ' '::CHARACTER VARYING::TEXT) || accuracy_tracking.creation_time::CHARACTER VARYING::TEXT,'YYYY-MM-DD HH24:MI:SS'::CHARACTER VARYING::TEXT) AS created_on,
       accuracy_tracking.audit_operator::varchar(255),
       CASE
         WHEN accuracy_tracking.total_present_sku::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.total_present_sku::REAL
       END AS total_present_sku,
       CASE
         WHEN accuracy_tracking.total_present_sku_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.total_present_sku_correct::REAL
       END AS total_present_sku_correct,
       CASE
         WHEN accuracy_tracking.own_present_sku::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.own_present_sku::REAL
       END AS own_present_sku,
       CASE
         WHEN accuracy_tracking.own_present_sku_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.own_present_sku_correct::REAL
       END AS own_present_sku_correct,
       CASE
         WHEN accuracy_tracking.total_present_facings::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.total_present_facings::REAL
       END AS total_present_facings,
       CASE
         WHEN accuracy_tracking.total_present_facings_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.total_present_facings_correct::REAL
       END AS total_present_facings_correct,
       CASE
         WHEN accuracy_tracking.own_present_facings::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.own_present_facings::REAL
       END AS own_present_facings,
       CASE
         WHEN accuracy_tracking.own_present_facings_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.own_present_facings_correct::REAL
       END AS own_present_facings_correct,
       CASE
         WHEN accuracy_tracking.audit_total_sku::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.audit_total_sku::REAL
       END AS audit_total_sku,
       CASE
         WHEN accuracy_tracking.audit_total_sku_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.audit_total_sku_correct::REAL
       END AS audit_total_sku_correct,
       CASE
         WHEN accuracy_tracking.audit_own_sku::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.audit_own_sku::REAL
       END AS audit_own_sku,
       CASE
         WHEN accuracy_tracking.audit_own_sku_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.audit_own_sku_correct::REAL
       END AS audit_own_sku_correct,
       CASE
         WHEN accuracy_tracking.audit_total_facing::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.audit_total_facing::REAL
       END AS audit_total_facing,
       CASE
         WHEN accuracy_tracking.audit_total_facing_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.audit_total_facing_correct::REAL
       END AS audit_total_facing_correct,
       CASE
         WHEN accuracy_tracking.audit_own_facing::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.audit_own_facing::REAL
       END AS audit_own_facing,
       CASE
         WHEN accuracy_tracking.audit_own_facing_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
         ELSE accuracy_tracking.audit_own_facing_correct::REAL
       END AS audit_own_facing_correct,
       CASE
         WHEN accuracy_tracking.audit_precision_sku::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE accuracy_tracking.audit_precision_sku::DOUBLE precision
       END AS audit_precision_sku,
       CASE
         WHEN accuracy_tracking.audit_precision_sku_own::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE accuracy_tracking.audit_precision_sku_own::DOUBLE precision
       END AS audit_precision_sku_own,
       CASE
         WHEN accuracy_tracking.audit_precision_facing::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE accuracy_tracking.audit_precision_facing::DOUBLE precision
       END AS audit_precision_facing,
       CASE
         WHEN accuracy_tracking.audit_precision_facing_own::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE accuracy_tracking.audit_precision_facing_own::DOUBLE precision
       END AS audit_precision_facing_own,
       CASE
         WHEN accuracy_tracking.audit_recall_sku::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE accuracy_tracking.audit_recall_sku::DOUBLE precision
       END AS audit_recall_sku,
       CASE
         WHEN accuracy_tracking.audit_recall_sku_own::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE accuracy_tracking.audit_recall_sku_own::DOUBLE precision
       END AS audit_recall_sku_own,
       CASE
         WHEN accuracy_tracking.audit_recall_facing::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE accuracy_tracking.audit_recall_facing::DOUBLE precision
       END AS audit_recall_facing,
       CASE
         WHEN accuracy_tracking.audit_recall_facing_own::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE accuracy_tracking.audit_recall_facing_own::DOUBLE precision
       END AS audit_recall_facing_own,
       NULL::TIMESTAMPTZ AS first_loaded_on,
       CURRENT_TIMESTAMP AS last_modified_on
FROM staging.accuracy_tracking
WHERE accuracy_tracking.product_ean::TEXT = ''::CHARACTER VARYING::TEXT
WITH NO SCHEMA BINDING;
COMMIT;