DROP VIEW IF EXISTS staging.photo_transformation_err CASCADE;

CREATE OR REPLACE VIEW staging.photo_transformation_err
(
  account_id,
  visit_id,
  owner_id,
  store_id,
  visit_date,
  photo_quality,
  photo_tag,
  photo_url,
  max_shelf,
  photo_quality_reason,
  photo_index,
  section_index,
  stack_index,
  photo_group,
  photo_group_type,
  photo_group_tags,
  row_id,
  processed_on,
  error_type
)
AS 
 SELECT new_analysis.account_id, new_analysis.visit_id, new_analysis.owner_id, new_analysis.store_id, new_analysis.visit_date::date AS visit_date, ( SELECT photo_quality.description
           FROM staging.photo_quality
          WHERE photo_quality.quality_id::character varying::text = new_analysis.value_text::text) AS photo_quality, new_analysis.photo_tag::varchar(255), new_analysis.photo_url, 
        CASE
            WHEN new_analysis.max_shelf::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_analysis.max_shelf
        END AS max_shelf, new_analysis.photo_quality_reason, 
        CASE
            WHEN new_analysis.photo_index::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_analysis.photo_index
        END AS photo_index, 
        CASE
            WHEN new_analysis.section_index::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_analysis.section_index
        END AS section_index,
        CASE
            WHEN new_analysis.stack_index::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_analysis.stack_index
        END AS stack_index, 
        CASE
            WHEN new_analysis.photo_group::character varying::text = ''::character varying::text THEN NULL::double precision
            ELSE new_analysis.photo_group::real::double precision
        END AS photo_group, new_analysis.photo_group_type, new_analysis.photo_group_tags, pg_catalog.row_number()
  OVER( 
  PARTITION BY new_analysis.account_id, new_analysis.visit_id, new_analysis.photo_index) AS row_id, 'now'::character varying::timestamp with time zone AS processed_on, 'missing key'::character varying AS error_type
   FROM staging.new_analysis
  WHERE new_analysis.analysis::text = 'photo_quality'::character varying::text AND (length(btrim(new_analysis.account_id::text)) = 0 OR length(btrim(new_analysis.visit_id::text)) = 0 OR length(btrim(new_analysis.photo_index::character varying::text)) = 0)
UNION 
 SELECT new_analysis.account_id, new_analysis.visit_id, new_analysis.owner_id, new_analysis.store_id, new_analysis.visit_date::date AS visit_date, ( SELECT photo_quality.description
           FROM staging.photo_quality
          WHERE photo_quality.quality_id::character varying::text = new_analysis.value_text::text) AS photo_quality, new_analysis.photo_tag::varchar(255), new_analysis.photo_url, 
        CASE
            WHEN new_analysis.max_shelf::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_analysis.max_shelf
        END AS max_shelf, new_analysis.photo_quality_reason, 
        CASE
            WHEN new_analysis.photo_index::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_analysis.photo_index
        END AS photo_index, 
        CASE
            WHEN new_analysis.section_index::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_analysis.section_index
        END AS section_index, 
        CASE
            WHEN new_analysis.stack_index::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_analysis.stack_index
        END AS stack_index,
        CASE
            WHEN new_analysis.photo_group::character varying::text = ''::character varying::text THEN NULL::double precision
            ELSE new_analysis.photo_group::real::double precision
        END AS photo_group, new_analysis.photo_group_type, new_analysis.photo_group_tags, pg_catalog.row_number()
  OVER( 
  PARTITION BY new_analysis.account_id, new_analysis.visit_id, new_analysis.photo_index) AS row_id, 'now'::character varying::timestamp with time zone AS processed_on, 'missing value_text'::character varying AS error_type
   FROM staging.new_analysis
  WHERE new_analysis.analysis::text = 'photo_quality'::character varying::text AND length(btrim(new_analysis.value_text::text)) = 0
WITH NO SCHEMA BINDING;

COMMIT;