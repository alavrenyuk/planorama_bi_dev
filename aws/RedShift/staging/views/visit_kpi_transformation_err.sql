DROP VIEW IF EXISTS staging.visit_kpi_transformation_err CASCADE;

CREATE OR REPLACE VIEW staging.visit_kpi_transformation_err 
(
  account_id,
  visit_id,
  visit_date,
  store_id,
  owner_id,
  analysis,
  kpi,
  kpi_id,
  own,
  display_group,
  question_english,
  question_local,
  product_group_name,
  prod_manufacturer,
  prod_brand,
  shelf_level,
  linear,
  facings,
  packages,
  value,
  target,
  discrepancy,
  compliant,
  kpi_group,
  question_answers,
  photo_group,
  photo_group_type,
  custom_field_a,
  custom_field_b,
  custom_field_c,
  custom_field_d,
  custom_field_e,
  row_id,
  processed_on,
  error_type
)
AS
SELECT new_analysis.account_id,
       new_analysis.visit_id,
       new_analysis.visit_date,
       new_analysis.store_id,
       new_analysis.owner_id,
       new_analysis.analysis,
       new_analysis.kpi,
       CASE
         WHEN new_analysis.kpi_id::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.kpi_id
       END AS kpi_id,
       CASE
         WHEN new_analysis.own::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::BOOLEAN
         ELSE
           CASE
             WHEN new_analysis.own = 0::DOUBLE precision OR new_analysis.own IS NULL AND 0 IS NULL THEN FALSE
             WHEN new_analysis.own = 1::DOUBLE precision OR new_analysis.own IS NULL AND 1 IS NULL THEN TRUE
             ELSE NULL::BOOLEAN
           END 
       END AS own,
       new_analysis.display_group,
       new_analysis.question_english,
       new_analysis.question_local,
       new_analysis.product_group_name,
       new_analysis.prod_manufacturer,
       new_analysis.prod_brand,
       new_analysis.shelf_level,
       CASE
         WHEN new_analysis.linear::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.linear
       END AS linear,
       CASE
         WHEN new_analysis.facings::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.facings
       END AS facings,
       CASE
         WHEN new_analysis.packages::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.packages
       END AS packages,
       CASE
         WHEN new_analysis.value::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.value
       END AS value,
       CASE
         WHEN new_analysis.target::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.target
       END AS target,
       CASE
         WHEN new_analysis.discrepancy::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.discrepancy
       END AS discrepancy,
       CASE
         WHEN new_analysis.compliant::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::BOOLEAN
         ELSE
           CASE
             WHEN new_analysis.compliant = 0::DOUBLE precision OR new_analysis.compliant IS NULL AND 0 IS NULL THEN FALSE
             WHEN new_analysis.compliant = 1::DOUBLE precision OR new_analysis.compliant IS NULL AND 1 IS NULL THEN TRUE
             ELSE NULL::BOOLEAN
           END 
       END AS compliant,
       new_analysis.kpi_group,
       new_analysis.question_answers,
       CASE
         WHEN new_analysis.photo_group::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
         ELSE new_analysis.photo_group
       END AS photo_group,
       new_analysis.photo_group_type,
       new_analysis.custom_field_a,
       new_analysis.custom_field_b,
       new_analysis.custom_field_c,
       new_analysis.custom_field_d,
       new_analysis.custom_field_e,
       pg_catalog.row_number() OVER (PARTITION BY new_analysis.visit_id) AS row_id,
       'now'::CHARACTER VARYING::TIMESTAMP WITH TIME zone AS processed_on,
       'missing key'::CHARACTER VARYING AS error_type
FROM staging.new_analysis
WHERE new_analysis.analysis::TEXT <> 'raw_data'::CHARACTER VARYING::TEXT
AND   new_analysis.analysis::TEXT <> 'photo_quality'::CHARACTER VARYING::TEXT
AND   new_analysis.analysis::TEXT <> 'compliance_per_group'::CHARACTER VARYING::TEXT
AND   new_analysis.analysis::TEXT <> 'group_presence'::CHARACTER VARYING::TEXT
AND   new_analysis.analysis::TEXT <> 'present_extra_sku'::CHARACTER VARYING::TEXT
AND   new_analysis.kpi::TEXT <> 'top_sku'::CHARACTER VARYING::TEXT
AND   new_analysis.kpi::TEXT <> 'star_sku'::CHARACTER VARYING::TEXT
AND   new_analysis.kpi::TEXT <> 'core_sku'::CHARACTER VARYING::TEXT
AND   new_analysis.kpi::TEXT <> 'npd'::CHARACTER VARYING::TEXT
AND   (LENGTH(BTRIM(new_analysis.account_id::TEXT)) = 0 OR LENGTH(BTRIM(new_analysis.visit_id::TEXT)) = 0)
WITH NO SCHEMA BINDING;

COMMIT;