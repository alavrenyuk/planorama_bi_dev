DROP VIEW IF EXISTS staging.product_transformation CASCADE;

CREATE OR REPLACE VIEW staging.product_transformation 
(
  account_id,
  visit_id,
  owner_id,
  store_id,
  product_id,
  visit_date,
  photo_index,
  section_index,
  photo_group,
  photo_group_type,
  shelf_level,
  horizontal_position_index,
  vertical_position,
  categ_code,
  categ_name,
  ranking,
  facings_tgt,
  own,
  currnt_stat,
  value,
  out_of_stock,
  linear,
  facings,
  packages,
  is_promo,
  price,
  first_loaded_on,
  last_modified_on
)
AS
SELECT new_analysis.account_id,
       new_analysis.visit_id,
       new_analysis.owner_id,
       new_analysis.store_id,
       new_analysis.product_id,
       DATE (new_analysis.visit_date) AS visit_date,
       new_analysis.photo_index,
       new_analysis.section_index,
       new_analysis.photo_group,
       new_analysis.photo_group_type,
       new_analysis.shelf_level,
       new_analysis.horizontal_position_index,
       new_analysis.vertical_position,
       new_analysis.categ_code,
       new_analysis.categ_name,
       new_analysis.ranking,
       new_analysis.facings_tgt,
       new_analysis.own,
       CASE
           WHEN length(new_analysis.currnt_stat)>50 THEN NULL
           ELSE new_analysis.currnt_stat
       END AS currnt_stat, 
       new_analysis.value,
       new_analysis.out_of_stock,
       new_analysis.linear,
       new_analysis.facings,
       new_analysis.packages,
       new_analysis.promo::INTEGER::BOOLEAN AS is_promo,
       new_analysis.price,
       null::timestamptz as first_loaded_on,
       current_timestamp as last_modified_on
FROM staging.new_analysis
WHERE new_analysis.analysis::TEXT = 'raw_data'::CHARACTER VARYING::TEXT
AND   new_analysis.analysis_date = ((SELECT "max"(na1.analysis_date) AS "max"
                                     FROM staging.new_analysis na1
                                     WHERE na1.account_id::TEXT = new_analysis.account_id::TEXT
                                     AND   na1.visit_id::TEXT = new_analysis.visit_id::TEXT
                                     AND   na1.visit_date = new_analysis.visit_date
                                     AND   na1.product_id::TEXT = new_analysis.product_id::TEXT
                                     AND   na1.photo_index = new_analysis.photo_index
                                     AND   na1.section_index = new_analysis.section_index
                                     AND   na1.shelf_level = new_analysis.shelf_level
                                     AND   na1.horizontal_position_index = new_analysis.horizontal_position_index))
WITH NO SCHEMA BINDING;

COMMIT;