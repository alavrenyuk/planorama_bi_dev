DROP VIEW IF EXISTS staging.photo_transformation CASCADE;

CREATE OR REPLACE VIEW staging.photo_transformation 
(
  account_id,
  visit_id,
  owner_id,
  store_id,
  visit_date,
  photo_quality_int,
  photo_quality,
  photo_tag,
  photo_url,
  max_shelf,
  photo_quality_reason,
  photo_index,
  section_index,
  stack_index,
  photo_group,
  photo_group_type,
  photo_group_tags,
  first_loaded_on,
  last_modified_on
)
AS
SELECT a.account_id,
       a.visit_id,
       a.owner_id,
       a.store_id,
       a.visit_date,
       a.photo_quality_int,
       a.photo_quality,
       a.photo_tag,
       a.photo_url,
       a.max_shelf,
       a.photo_quality_reason,
       a.photo_index,
       a.section_index,
       a.stack_index,
       a.photo_group,
       a.photo_group_type,
       a.photo_group_tags,
       a.first_loaded_on,
       a.last_modified_on
FROM (SELECT new_analysis.account_id,
             new_analysis.visit_id,
             new_analysis.owner_id,
             new_analysis.store_id,
             new_analysis.visit_date::DATE AS visit_date,
             new_analysis.value_text::INTEGER AS photo_quality_int,
             (SELECT photo_quality.description
              FROM staging.photo_quality
              WHERE photo_quality.quality_id::CHARACTER VARYING::TEXT = new_analysis.value_text::TEXT) AS photo_quality,
             new_analysis.photo_tag::varchar(255),
             new_analysis.photo_url,
             CASE
               WHEN new_analysis.max_shelf::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE new_analysis.max_shelf
             END AS max_shelf,
             new_analysis.photo_quality_reason,
             CASE
               WHEN new_analysis.photo_index::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE new_analysis.photo_index
             END AS photo_index,
             CASE
               WHEN new_analysis.section_index::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE new_analysis.section_index
             END AS section_index,
             CASE
               WHEN new_analysis.stack_index::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE new_analysis.stack_index
             END AS stack_index,
             CASE
               WHEN new_analysis.photo_group::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE new_analysis.photo_group::REAL::DOUBLE precision
             END AS photo_group,
             new_analysis.photo_group_type,
             new_analysis.photo_group_tags,
             null::timestamptz as first_loaded_on,
             current_timestamp as last_modified_on
      FROM staging.new_analysis
      WHERE new_analysis.analysis_date = ((SELECT "max"(na1.analysis_date) AS "max"
                                           FROM staging.new_analysis na1
                                           WHERE na1.account_id::TEXT = new_analysis.account_id::TEXT
                                           AND   na1.visit_id::TEXT = new_analysis.visit_id::TEXT
                                           AND   na1.visit_date = new_analysis.visit_date))
      AND   new_analysis.analysis::TEXT = 'photo_quality'::CHARACTER VARYING::TEXT
      AND   LENGTH(BTRIM(new_analysis.account_id::TEXT)) > 0
      AND   LENGTH(BTRIM(new_analysis.visit_id::TEXT)) > 0
      AND   LENGTH(BTRIM(new_analysis.photo_index::CHARACTER VARYING::TEXT)) > 0
      AND   LENGTH(BTRIM(new_analysis.value_text::TEXT)) > 0) a
WITH NO SCHEMA BINDING;

COMMIT;