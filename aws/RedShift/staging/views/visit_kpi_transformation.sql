DROP VIEW IF EXISTS staging.visit_kpi_transformation CASCADE;

CREATE OR REPLACE VIEW staging.visit_kpi_transformation 
(
  account_id,
  visit_id,
  visit_date,
  store_id,
  owner_id,
  analysis,
  kpi,
  kpi_id,
  own,
  display_group,
  question_english,
  question_local,
  product_group_name,
  prod_manufacturer,
  prod_brand,
  shelf_level,
  linear,
  facings,
  packages,
  value,
  target,
  discrepancy,
  compliant,
  kpi_group,
  question_answers,
  photo_group,
  photo_group_type,
  custom_field_a,
  custom_field_b,
  custom_field_c,
  custom_field_d,
  custom_field_e,
  row_id,
  first_loaded_on,
  last_modified_on
)
AS
SELECT new_analysis.account_id,
       new_analysis.visit_id,
       new_analysis.visit_date,
       new_analysis.store_id,
       new_analysis.owner_id,
       new_analysis.analysis,
       new_analysis.kpi,
       new_analysis.kpi_id,
       new_analysis.own::INTEGER::BOOLEAN AS own,
       new_analysis.display_group,
       left(new_analysis.question_english,1000)::varchar(1000),
       left(new_analysis.question_local,1000)::varchar(1000),
       new_analysis.product_group_name,
       new_analysis.prod_manufacturer,
       new_analysis.prod_brand,
       new_analysis.shelf_level,
       new_analysis.linear,
       new_analysis.facings,
       new_analysis.packages,
       new_analysis.value,
       new_analysis.target,
       new_analysis.discrepancy,
       new_analysis.compliant::INTEGER::BOOLEAN AS compliant,
       new_analysis.kpi_group,
       left(new_analysis.question_answers,1000)::varchar(1000),
       new_analysis.photo_group,
       new_analysis.photo_group_type,
       new_analysis.custom_field_a,
       new_analysis.custom_field_b,
       new_analysis.custom_field_c,
       new_analysis.custom_field_d,
       new_analysis.custom_field_e,
       pg_catalog.row_number() OVER (PARTITION BY new_analysis.visit_id) AS row_id,
       null::timestamptz as first_loaded_on,
       current_timestamp as last_modified_on
FROM staging.new_analysis
WHERE new_analysis.analysis::TEXT <> 'raw_data'::CHARACTER VARYING::TEXT
AND   new_analysis.analysis::TEXT <> 'photo_quality'::CHARACTER VARYING::TEXT
AND   new_analysis.analysis::TEXT <> 'compliance_per_group'::CHARACTER VARYING::TEXT
AND   new_analysis.analysis::TEXT <> 'group_presence'::CHARACTER VARYING::TEXT
AND   new_analysis.analysis::TEXT <> 'present_extra_sku'::CHARACTER VARYING::TEXT
AND   new_analysis.kpi::TEXT <> 'top_sku'::CHARACTER VARYING::TEXT
AND   new_analysis.kpi::TEXT <> 'star_sku'::CHARACTER VARYING::TEXT
AND   new_analysis.kpi::TEXT <> 'core_sku'::CHARACTER VARYING::TEXT
AND   new_analysis.kpi::TEXT <> 'npd'::CHARACTER VARYING::TEXT
AND   new_analysis.analysis_date = ((SELECT "max"(na1.analysis_date) AS "max"
                                     FROM staging.new_analysis na1
                                     WHERE na1.account_id::TEXT = new_analysis.account_id::TEXT
                                     AND   na1.visit_id::TEXT = new_analysis.visit_id::TEXT
                                     AND   na1.visit_date = new_analysis.visit_date))
WITH NO SCHEMA BINDING;

COMMIT;