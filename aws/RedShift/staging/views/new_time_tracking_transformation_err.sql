DROP VIEW IF EXISTS staging.new_time_tracking_transformation_err CASCADE;

CREATE OR REPLACE VIEW staging.new_time_tracking_transformation_err
(
  account_id,
  visit_id,
  number_of_pictures,
  category_name,
  start_photo_capture,
  end_photo_capture,
  duration_photo_capture,
  visit_creation_on_server,
  waiting_time_before_upload,
  start_photo_upload,
  end_photo_upload,
  duration_upload,
  start_processing,
  end_processing,
  duration_processing,
  duration_end_upload_to_end_processing,
  start_labelling,
  end_labelling,
  duration_labelling,
  start_verification,
  end_verification,
  duration_verification,
  ptt,
  row_id,
  processed_on,
  error_type
)
AS 
 SELECT a.account_id, a.visit_id, a.number_of_pictures, a.category_name, a.start_photo_capture, a.end_photo_capture, a.duration_photo_capture, a.visit_creation_on_server, a.waiting_time_before_upload, a.start_photo_upload, a.end_photo_upload, a.duration_upload, a.start_processing, a.end_processing, a.duration_processing, a.duration_end_upload_to_end_processing, a.start_labelling, a.end_labelling, a.duration_labelling, a.start_verification, a.end_verification, a.duration_verification, a.ptt, a.row_id, 'now'::character varying::timestamp with time zone AS processed_on, 'duplicate'::character varying AS error_type
   FROM ( SELECT new_time_tracking.account_id, new_time_tracking.visit_id, new_time_tracking.number_of_pictures, new_time_tracking.category_name, 
                CASE
                    WHEN new_time_tracking.start_photo_capture::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.start_photo_capture
                END AS start_photo_capture, 
                CASE
                    WHEN new_time_tracking.end_photo_capture::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.end_photo_capture
                END AS end_photo_capture, 
                CASE
                    WHEN new_time_tracking.duration_photo_capture::character varying::text = ''::character varying::text THEN NULL::real
                    ELSE new_time_tracking.duration_photo_capture
                END AS duration_photo_capture, 
                CASE
                    WHEN new_time_tracking.visit_creation_on_server::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.visit_creation_on_server
                END AS visit_creation_on_server, 
                CASE
                    WHEN new_time_tracking.waiting_time_before_upload::character varying::text = ''::character varying::text THEN NULL::real
                    ELSE new_time_tracking.waiting_time_before_upload
                END AS waiting_time_before_upload, 
                CASE
                    WHEN new_time_tracking.start_photo_upload::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.start_photo_upload
                END AS start_photo_upload, 
                CASE
                    WHEN new_time_tracking.end_photo_upload::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.end_photo_upload
                END AS end_photo_upload, 
                CASE
                    WHEN new_time_tracking.duration_upload::character varying::text = ''::character varying::text THEN NULL::real
                    ELSE new_time_tracking.duration_upload
                END AS duration_upload, 
                CASE
                    WHEN new_time_tracking.start_processing::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.start_processing
                END AS start_processing, 
                CASE
                    WHEN new_time_tracking.end_processing::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.end_processing
                END AS end_processing, 
                CASE
                    WHEN new_time_tracking.duration_processing::character varying::text = ''::character varying::text THEN NULL::real
                    ELSE new_time_tracking.duration_processing
                END AS duration_processing, 
                CASE
                    WHEN new_time_tracking.duration_end_upload_to_end_processing::character varying::text = ''::character varying::text THEN NULL::real
                    ELSE new_time_tracking.duration_end_upload_to_end_processing
                END AS duration_end_upload_to_end_processing, 
                CASE
                    WHEN new_time_tracking.start_labelling::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.start_labelling
                END AS start_labelling, 
                CASE
                    WHEN new_time_tracking.end_labelling::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.end_labelling
                END AS end_labelling, 
                CASE
                    WHEN new_time_tracking.duration_labelling::character varying::text = ''::character varying::text THEN NULL::real
                    ELSE new_time_tracking.duration_labelling
                END AS duration_labelling, 
                CASE
                    WHEN new_time_tracking.start_verification::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.start_verification
                END AS start_verification, 
                CASE
                    WHEN new_time_tracking.end_verification::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
                    ELSE new_time_tracking.end_verification
                END AS end_verification, 
                CASE
                    WHEN new_time_tracking.duration_verification::character varying::text = ''::character varying::text THEN NULL::real
                    ELSE new_time_tracking.duration_verification
                END AS duration_verification, new_time_tracking.duration_end_upload_to_end_processing + new_time_tracking.duration_photo_capture AS ptt, pg_catalog.row_number()
          OVER( 
          PARTITION BY new_time_tracking.account_id, new_time_tracking.visit_id
          ORDER BY new_time_tracking.visit_creation_on_server DESC) AS row_id, 'now'::character varying::timestamp with time zone AS processed_on, 'duplicate'::character varying AS error_type
           FROM staging.new_time_tracking) a
  WHERE a.row_id > 1
UNION 
 SELECT new_time_tracking.account_id, new_time_tracking.visit_id, new_time_tracking.number_of_pictures, new_time_tracking.category_name, 
        CASE
            WHEN new_time_tracking.start_photo_capture::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.start_photo_capture
        END AS start_photo_capture, 
        CASE
            WHEN new_time_tracking.end_photo_capture::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.end_photo_capture
        END AS end_photo_capture, 
        CASE
            WHEN new_time_tracking.duration_photo_capture::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_time_tracking.duration_photo_capture
        END AS duration_photo_capture, 
        CASE
            WHEN new_time_tracking.visit_creation_on_server::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.visit_creation_on_server
        END AS visit_creation_on_server, 
        CASE
            WHEN new_time_tracking.waiting_time_before_upload::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_time_tracking.waiting_time_before_upload
        END AS waiting_time_before_upload, 
        CASE
            WHEN new_time_tracking.start_photo_upload::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.start_photo_upload
        END AS start_photo_upload, 
        CASE
            WHEN new_time_tracking.end_photo_upload::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.end_photo_upload
        END AS end_photo_upload, 
        CASE
            WHEN new_time_tracking.duration_upload::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_time_tracking.duration_upload
        END AS duration_upload, 
        CASE
            WHEN new_time_tracking.start_processing::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.start_processing
        END AS start_processing, 
        CASE
            WHEN new_time_tracking.end_processing::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.end_processing
        END AS end_processing, 
        CASE
            WHEN new_time_tracking.duration_processing::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_time_tracking.duration_processing
        END AS duration_processing, 
        CASE
            WHEN new_time_tracking.duration_end_upload_to_end_processing::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_time_tracking.duration_end_upload_to_end_processing
        END AS duration_end_upload_to_end_processing, 
        CASE
            WHEN new_time_tracking.start_labelling::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.start_labelling
        END AS start_labelling, 
        CASE
            WHEN new_time_tracking.end_labelling::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.end_labelling
        END AS end_labelling, 
        CASE
            WHEN new_time_tracking.duration_labelling::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_time_tracking.duration_labelling
        END AS duration_labelling, 
        CASE
            WHEN new_time_tracking.start_verification::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.start_verification
        END AS start_verification, 
        CASE
            WHEN new_time_tracking.end_verification::character varying::text = ''::character varying::text THEN NULL::timestamp without time zone
            ELSE new_time_tracking.end_verification
        END AS end_verification, 
        CASE
            WHEN new_time_tracking.duration_verification::character varying::text = ''::character varying::text THEN NULL::real
            ELSE new_time_tracking.duration_verification
        END AS duration_verification, new_time_tracking.duration_end_upload_to_end_processing + new_time_tracking.duration_photo_capture AS ptt, pg_catalog.row_number()
  OVER( 
  PARTITION BY new_time_tracking.account_id, new_time_tracking.visit_id
  ORDER BY new_time_tracking.visit_creation_on_server DESC) AS row_id, 'now'::character varying::timestamp with time zone AS processed_on, 'missing key'::character varying AS error_type
   FROM staging.new_time_tracking
  WHERE length(btrim(new_time_tracking.account_id::text)) = 0 OR length(btrim(new_time_tracking.visit_id::text)) = 0
WITH NO SCHEMA BINDING;

COMMIT;