DROP VIEW IF EXISTS staging.accuracy_product_transformation CASCADE;

CREATE OR REPLACE VIEW staging.accuracy_product_transformation
AS 
SELECT a.account_id,
       a.visit_id,
       a.account_name,
       a.category,
       a.creation_date,
       a.product_ean,
       a.kind,
       a.product_name,
       a.labelling_operator,
       a.verification_operator,
       a.audit_operator,
       a.ref_total_present_sku,
       a.ref_total_present_sku_correct,
       a.ref_own_present_sku,
       a.ref_own_present_sku_correct,
       a.ref_total_present_facings,
       a.ref_total_present_facings_correct,
       a.ref_own_present_facings,
       a.ref_own_present_facings_correct,
       a.verif_total_sku,
       a.verif_total_sku_correct,
       a.verif_own_sku,
       a.verif_own_sku_correct,
       a.verif_total_facing,
       a.verif_total_facing_correct,
       a.verif_own_facing,
       a.verif_own_facing_correct,
       a.audit_total_sku,
       a.audit_total_sku_correct,
       a.audit_own_sku,
       a.audit_own_sku_correct,
       a.audit_total_facing,
       a.audit_total_facing_correct,
       a.audit_own_facing,
       a.audit_own_facing_correct,
       a.audit_precision_sku,
       a.audit_precision_sku_own,
       a.audit_precision_facing,
       a.audit_precision_facing_own,
       a.audit_recall_sku,
       a.audit_recall_sku_own,
       a.audit_recall_facing,
       a.audit_recall_facing_own,
       a.top_sku,
       a.star_sku,
       a.core_sku,
       a.npd,
       a.osa,
       a.first_loaded_on,
       a.last_modified_on
FROM (SELECT accuracy_tracking.account_id::varchar(50),
             accuracy_tracking.visit_id::varchar(50),
             accuracy_tracking.account_name::varchar(255),
             accuracy_tracking.category_name::varchar(255) as category,
             TO_TIMESTAMP((accuracy_tracking.creation_date::CHARACTER VARYING::TEXT || ' '::CHARACTER VARYING::TEXT) || accuracy_tracking.creation_time::CHARACTER VARYING::TEXT,'YYYY-MM-DD HH24:MI:SS'::CHARACTER VARYING::TEXT) as creation_date,
             accuracy_tracking.product_ean::varchar(50),
             accuracy_tracking.kind::varchar(20),
             accuracy_tracking.product_name::varchar(255),
             accuracy_tracking.labelling_operator::varchar(255),
             accuracy_tracking.verification_operator::varchar(255),
             accuracy_tracking.audit_operator::varchar(255),
             CASE
               WHEN accuracy_tracking.total_present_sku::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.total_present_sku::REAL
             END AS ref_total_present_sku,
             CASE
               WHEN accuracy_tracking.total_present_sku_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.total_present_sku_correct::REAL
             END AS ref_total_present_sku_correct,
             CASE
               WHEN accuracy_tracking.own_present_sku::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.own_present_sku::REAL
             END AS ref_own_present_sku,
             CASE
               WHEN accuracy_tracking.own_present_sku_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.own_present_sku_correct::REAL
             END AS ref_own_present_sku_correct,
             CASE
               WHEN accuracy_tracking.total_present_facings::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.total_present_facings::REAL
             END AS ref_total_present_facings,
             CASE
               WHEN accuracy_tracking.total_present_facings_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.total_present_facings_correct::REAL
             END AS ref_total_present_facings_correct,
             CASE
               WHEN accuracy_tracking.own_present_facings::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.own_present_facings::REAL
             END AS ref_own_present_facings,
             CASE
               WHEN accuracy_tracking.own_present_facings_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.own_present_facings_correct::REAL
             END AS ref_own_present_facings_correct,
             CASE
               WHEN accuracy_tracking.verif_total_sku::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.verif_total_sku::REAL
             END AS verif_total_sku,
             CASE
               WHEN accuracy_tracking.verif_total_sku_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.verif_total_sku_correct::REAL
             END AS verif_total_sku_correct,
             CASE
               WHEN accuracy_tracking.verif_own_sku::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.verif_own_sku::REAL
             END AS verif_own_sku,
             CASE
               WHEN accuracy_tracking.verif_own_sku_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.verif_own_sku_correct::REAL
             END AS verif_own_sku_correct,
             CASE
               WHEN accuracy_tracking.verif_total_facing::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.verif_total_facing::REAL
             END AS verif_total_facing,            
             CASE
               WHEN accuracy_tracking.verif_total_facing_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.verif_total_facing_correct::REAL
             END AS verif_total_facing_correct,           
             CASE
               WHEN accuracy_tracking.verif_own_facing::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.verif_own_facing::REAL
             END AS verif_own_facing,
             CASE
               WHEN accuracy_tracking.verif_own_facing_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.verif_own_facing_correct::REAL
             END AS verif_own_facing_correct,             
             CASE
               WHEN accuracy_tracking.audit_total_sku::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.audit_total_sku::REAL
             END AS audit_total_sku,
             CASE
               WHEN accuracy_tracking.audit_total_sku_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.audit_total_sku_correct::REAL
             END AS audit_total_sku_correct,
             CASE
               WHEN accuracy_tracking.audit_own_sku::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.audit_own_sku::REAL
             END AS audit_own_sku,
             CASE
               WHEN accuracy_tracking.audit_own_sku_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.audit_own_sku_correct::REAL
             END AS audit_own_sku_correct,
             CASE
               WHEN accuracy_tracking.audit_total_facing::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.audit_total_facing::REAL
             END AS audit_total_facing,
             CASE
               WHEN accuracy_tracking.audit_total_facing_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.audit_total_facing_correct::REAL
             END AS audit_total_facing_correct,
             CASE
               WHEN accuracy_tracking.audit_own_facing::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.audit_own_facing::REAL
             END AS audit_own_facing,
             CASE
               WHEN accuracy_tracking.audit_own_facing_correct::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::REAL
               ELSE accuracy_tracking.audit_own_facing_correct::REAL
             END AS audit_own_facing_correct,
             CASE
               WHEN accuracy_tracking.audit_precision_sku::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE accuracy_tracking.audit_precision_sku::DOUBLE precision
             END AS audit_precision_sku,
             CASE
               WHEN accuracy_tracking.audit_precision_sku_own::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE accuracy_tracking.audit_precision_sku_own::DOUBLE precision
             END AS audit_precision_sku_own,
             CASE
               WHEN accuracy_tracking.audit_precision_facing::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE accuracy_tracking.audit_precision_facing::DOUBLE precision
             END AS audit_precision_facing,
             CASE
               WHEN accuracy_tracking.audit_precision_facing_own::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE accuracy_tracking.audit_precision_facing_own::DOUBLE precision
             END AS audit_precision_facing_own,
             CASE
               WHEN accuracy_tracking.audit_recall_sku::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE accuracy_tracking.audit_recall_sku::DOUBLE precision
             END AS audit_recall_sku,
             CASE
               WHEN accuracy_tracking.audit_recall_sku_own::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE accuracy_tracking.audit_recall_sku_own::DOUBLE precision
             END AS audit_recall_sku_own,
             CASE
               WHEN accuracy_tracking.audit_recall_facing::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE accuracy_tracking.audit_recall_facing::DOUBLE precision
             END AS audit_recall_facing,
             CASE
               WHEN accuracy_tracking.audit_recall_facing_own::TEXT = 'NaN'::CHARACTER VARYING::TEXT THEN NULL::DOUBLE precision
               ELSE accuracy_tracking.audit_recall_facing_own::DOUBLE precision
             END AS audit_recall_facing_own,
             accuracy_tracking.top_sku::REAL AS top_sku,
             accuracy_tracking.star_sku::REAL AS star_sku,
             accuracy_tracking.core_sku::REAL AS core_sku,
             accuracy_tracking.npd::REAL AS npd,
             CASE
               WHEN accuracy_tracking.top_sku::REAL = 1::DOUBLE precision OR accuracy_tracking.star_sku::REAL = 1::DOUBLE precision OR accuracy_tracking.core_sku::REAL = 1::DOUBLE precision OR accuracy_tracking.npd::REAL = 1::DOUBLE precision THEN 1::REAL
               ELSE 0::REAL
             END AS osa,
             NULL::TIMESTAMPTZ AS first_loaded_on,
             CURRENT_TIMESTAMP AS last_modified_on,
             pg_catalog.row_number() OVER (PARTITION BY accuracy_tracking.account_id,accuracy_tracking.visit_id,accuracy_tracking.product_ean) AS "row"
      FROM staging.accuracy_tracking
      WHERE accuracy_tracking.product_ean::TEXT <> ''::CHARACTER VARYING::TEXT) a
WHERE a."row" = 1
WITH NO SCHEMA BINDING;
COMMIT;