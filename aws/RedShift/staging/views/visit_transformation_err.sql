DROP VIEW IF EXISTS staging.visit_transformation_err CASCADE;

CREATE OR REPLACE VIEW staging.visit_transformation_err 
(
  account_id,
  visit_id,
  visit_date,
  visit_datetime,
  categ_code,
  categ_name,
  owner_id,
  store_id,
  number_of_pictures,
  number_of_sections,
  process_type,
  processed_on,
  error_type
)
AS
SELECT DISTINCT new_analysis.account_id,
       new_analysis.visit_id,
       new_analysis.visit_date::DATE AS visit_date,
       new_analysis.visit_date AS visit_datetime,
       new_analysis.categ_code,
       new_analysis.categ_name,
       new_analysis.owner_id,
       new_analysis.store_id,
       new_analysis.number_of_pictures,
       new_analysis.number_of_sections,
       new_analysis.process_type,
       'now'::TEXT::TIMESTAMP WITH TIME zone AS processed_on,
       'missing key'::CHARACTER VARYING AS error_type
FROM staging.new_analysis
WHERE LENGTH(BTRIM(new_analysis.account_id::TEXT)) = 0
OR    LENGTH(BTRIM(new_analysis.visit_id::TEXT)) = 0
WITH NO SCHEMA BINDING;

COMMIT;