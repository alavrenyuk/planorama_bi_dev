DROP VIEW IF EXISTS staging.master_store_transformation CASCADE;

CREATE OR REPLACE VIEW staging.master_store_transformation 
(
  account_id,
  store_id,
  internal_code,
  chainstore,
  description,
  address,
  region,
  city,
  postal_code,
  owners,
  desc_a,
  desc_b,
  desc_c,
  desc_d,
  desc_e,
  desc_f,
  desc_g,
  desc_h,
  country,
  latitude,
  longitude,
  is_deleted,
  first_loaded_on,
  last_modified_on
)
AS
SELECT a.account_id,
       a.store_id,
       a.internal_code,
       a.chainstore,
       a.description,
       a.address,
       a."region",
       a.city,
       a.postal_code,
       a.owners,
       a.desc_a,
       a.desc_b,
       a.desc_c,
       a.desc_d,
       a.desc_e,
       a.desc_f,
       a.desc_g,
       a.desc_h,
       a.country,
       a.latitude,
       a.longitude,
       a.is_deleted,
       a.first_loaded_on,
       a.last_modified_on
FROM (SELECT master_data_store.account_id::varchar(50),
             master_data_store.store_id::varchar(50),
             master_data_store.internal_code::varchar(255),
             master_data_store.chainstore::varchar(255),
             master_data_store.description::varchar(255),
             master_data_store.address::varchar(255),
             master_data_store."region"::varchar(255),
             master_data_store.city::VARCHAR(50),
             master_data_store.postal_code::varchar(50),
             master_data_store.owners::varchar(65535),
             master_data_store.desc_a::varchar(255),
             master_data_store.desc_b::varchar(255),
             master_data_store.desc_c::varchar(255),
             master_data_store.desc_d::varchar(255),
             master_data_store.desc_e::varchar(255),
             master_data_store.desc_f::varchar(255),
             master_data_store.desc_g::varchar(255),
             master_data_store.desc_h::varchar(255),
             master_data_store.country::varchar(50),
             CASE
               WHEN master_data_store.latitude::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::CHARACTER VARYING
               ELSE master_data_store.latitude::CHARACTER VARYING
             END 
      ::DOUBLE precision AS latitude,
             CASE
               WHEN master_data_store.longitude::CHARACTER VARYING::TEXT = ''::CHARACTER VARYING::TEXT THEN NULL::CHARACTER VARYING
               ELSE master_data_store.longitude::CHARACTER VARYING
             END 
      ::DOUBLE precision AS longitude,
             master_data_store.is_deleted,
             NULL::TIMESTAMPTZ AS first_loaded_on,
             CURRENT_TIMESTAMP AS last_modified_on,
             pg_catalog.row_number() OVER (PARTITION BY master_data_store.account_id,master_data_store.store_id) AS "row"
      FROM staging.master_data_store
      WHERE LENGTH(BTRIM(master_data_store.account_id::TEXT)) > 0
      AND   LENGTH(BTRIM(master_data_store.store_id::TEXT)) > 0) a
WHERE a."row" = 1
WITH NO SCHEMA BINDING;

COMMIT;