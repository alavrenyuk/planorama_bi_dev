DROP VIEW IF EXISTS staging.master_user_hierarchy_transformation CASCADE;

CREATE OR REPLACE VIEW staging.master_user_hierarchy_transformation 
(
  account_id,
  parent,
  username,
  first_loaded_on,
  last_modified_on
)
AS

(
  (((((((( SELECT master_data_user_hierarchy.account_id, master_data_user_hierarchy.username AS parent, master_data_user_hierarchy.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on
  FROM staging.master_data_user_hierarchy UNION SELECT master_data_user_hierarchy.account_id, master_data_user_hierarchy.parent, master_data_user_hierarchy.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on
  FROM staging.master_data_user_hierarchy WHERE master_data_user_hierarchy.parent::TEXT <> ''::CHARACTER VARYING::TEXT) UNION SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN staging.master_data_user_hierarchy b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) UNION SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN staging.master_data_user_hierarchy b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) UNION SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN staging.master_data_user_hierarchy b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) UNION SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN staging.master_data_user_hierarchy b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) UNION SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN staging.master_data_user_hierarchy b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) UNION SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN staging.master_data_user_hierarchy b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) UNION SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN staging.master_data_user_hierarchy b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) UNION SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on  
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
  FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
             FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
             FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
             FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
             FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
             FROM staging.master_data_user_hierarchy a JOIN ( SELECT a.account_id, a.parent, b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
             FROM staging.master_data_user_hierarchy a JOIN staging.master_data_user_hierarchy b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT) b ON a.username::TEXT = b.parent::TEXT AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT AND a.account_id::TEXT = b.account_id::TEXT
)UNION 
SELECT a.account_id,
       a.parent,
       b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on 
FROM staging.master_data_user_hierarchy a
  JOIN (SELECT a.account_id,
               a.parent,
               b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on
        FROM staging.master_data_user_hierarchy a
          JOIN (SELECT a.account_id,
                       a.parent,
                       b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on
                FROM staging.master_data_user_hierarchy a
                  JOIN (SELECT a.account_id,
                               a.parent,
                               b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on
                        FROM staging.master_data_user_hierarchy a
                          JOIN (SELECT a.account_id,
                                       a.parent,
                                       b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on
                                FROM staging.master_data_user_hierarchy a
                                  JOIN (SELECT a.account_id,
                                               a.parent,
                                               b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on
                                        FROM staging.master_data_user_hierarchy a
                                          JOIN (SELECT a.account_id,
                                                       a.parent,
                                                       b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on
                                                FROM staging.master_data_user_hierarchy a
                                                  JOIN (SELECT a.account_id,
                                                               a.parent,
                                                               b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on
                                                        FROM staging.master_data_user_hierarchy a
                                                          JOIN (SELECT a.account_id,
                                                                       a.parent,
                                                                       b.username, null::timestamptz as first_loaded_on, current_timestamp as last_modified_on
                                                                FROM staging.master_data_user_hierarchy a
                                                                  JOIN staging.master_data_user_hierarchy b
                                                                    ON a.username::TEXT = b.parent::TEXT
                                                                   AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT
                                                                   AND a.account_id::TEXT = b.account_id::TEXT) b
                                                            ON a.username::TEXT = b.parent::TEXT
                                                           AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT
                                                           AND a.account_id::TEXT = b.account_id::TEXT) b
                                                    ON a.username::TEXT = b.parent::TEXT
                                                   AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT
                                                   AND a.account_id::TEXT = b.account_id::TEXT) b
                                            ON a.username::TEXT = b.parent::TEXT
                                           AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT
                                           AND a.account_id::TEXT = b.account_id::TEXT) b
                                    ON a.username::TEXT = b.parent::TEXT
                                   AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT
                                   AND a.account_id::TEXT = b.account_id::TEXT) b
                            ON a.username::TEXT = b.parent::TEXT
                           AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT
                           AND a.account_id::TEXT = b.account_id::TEXT) b
                    ON a.username::TEXT = b.parent::TEXT
                   AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT
                   AND a.account_id::TEXT = b.account_id::TEXT) b
            ON a.username::TEXT = b.parent::TEXT
           AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT
           AND a.account_id::TEXT = b.account_id::TEXT) b
    ON a.username::TEXT = b.parent::TEXT
   AND a.parent::TEXT <> ''::CHARACTER VARYING::TEXT
   AND a.account_id::TEXT = b.account_id::TEXT
WITH NO SCHEMA BINDING;

COMMIT;