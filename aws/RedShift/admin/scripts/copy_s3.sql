COPY middle.master_store FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/master_product_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 18s */

COPY middle.master_product FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/master_product_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 38s */

COPY middle.master_user FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/master_user_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 6s */

COPY middle.master_user_hierarchy FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/master_user_hierarchy_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 9s */

COPY middle.accuracy_product_fact FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/accuracy_product_fact_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 22s */

COPY middle.accuracy_visit_fact FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/accuracy_visit_fact_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 6s */

COPY middle.calendar FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/calendar_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 6s */

COPY middle.compliance_fact FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/compliance_fact_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 24s */

COPY middle.geolocation FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/geolocation_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT
/* 16s */

COPY middle.monitor_visit_source FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/monitor_visit_source_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 22s */                                                     

COPY middle.new_time_tracking_fact FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/new_time_tracking_fact_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 9s */

COPY middle.photo_fact FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/photo_fact_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 2.1 GB - 1m 27s */

COPY middle.product_fact FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/product_fact_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 2m 28s */

COPY middle.product_kpi_fact FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/product_kpi_fact'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;

 /* 7m */

COPY middle.user_feedback_fact FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/user_feedback_fact_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 13s */

COPY middle.visit_fact FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/visit_fact_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 29s */

COPY middle.visit_kpi_fact FROM 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/visit_kpi_fact_parquet'
iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET;
COMMIT;
/* 3m 30s */