UNLOAD ('select * from consolidated.master_product') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/master_product_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.master_store') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/master_store_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.master_user') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/master_user_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.master_user_hierarchy') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/master_user_hierarchy_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.accuracy_product_fact') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/accuracy_product_fact_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.accuracy_visit_fact') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/accuracy_visit_fact_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.calendar') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/calendar_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.compliance_fact') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/compliance_fact_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.geolocation') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/geolocation_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.monitor_visit_source') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/monitor_visit_source_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.new_time_tracking_fact') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/new_time_tracking_fact_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.photo_fact') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/photo_fact_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.product_fact') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/product_fact_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.product_kpi_fact') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/product_kpi_fact_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.user_feedback_fact') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/user_feedback_fact_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.visit_fact') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/visit_fact_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;

UNLOAD ('select * from consolidated.visit_kpi_fact') 
to 's3://aws-glue-temporary-615620803006-eu-west-1/test_export_consolidated/visit_kpi_fact_parquet' iam_role 'arn:aws:iam::615620803006:role/glue-s3test' 
PARQUET PARALLEL OFF ALLOWOVERWRITE;