DROP VIEW IF EXISTS consolidated.ph_userfeedback_per_categ CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_userfeedback_per_categ

as
SELECT 
  calendar.account_id || calendar.visit_date AS uid, 
  calendar.account_id, 
  calendar.visit_date,
  visit_fact.categ_name,
  count (distinct  (user_feedback_fact.visit_id)) AS number_of_reported_visits, 
  count (distinct (visit_fact.visit_id)) AS number_of_visits,
  count (distinct  (user_feedback_fact.visit_id)) :: decimal / nullif (count (distinct (visit_fact.visit_id)),0) :: decimal as ratio_of_reported_visits
FROM consolidated.calendar
  JOIN consolidated.visit_fact 
    ON calendar.visit_date = visit_fact.visit_date AND calendar.account_id::text = visit_fact.account_id::text
  left join consolidated.user_feedback_fact
    ON visit_fact.account_id = user_feedback_fact.account_id AND visit_fact.visit_id = user_feedback_fact.visit_id
where calendar.year >= 2020
GROUP BY calendar.account_id, calendar.visit_date, visit_fact.categ_name
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_userfeedback_per_categ TO plano_auto_export;


COMMIT;