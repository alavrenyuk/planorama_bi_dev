DROP VIEW IF EXISTS consolidated.ph_allphoto_per_user CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_allphoto_per_user

AS 
 SELECT 
  calendar.account_id || calendar.visit_date AS uid, 
  master_user.account_id,
  master_user.username, 
  visit_fact.categ_name, 
  master_store.chainstore, 
  master_store.description, 
  master_store.city, 
  master_store.address, 
  visit_fact.visit_date, 
  'https://portal.planorama.com/checks/'::character varying::text + visit_fact.visit_id::text AS visit_url, 
  photo_fact.photo_url, 
  photo_fact.photo_index, 
  photo_fact.photo_quality, 
  photo_fact.photo_quality_reason, 
  count(photo_fact.visit_id) AS number_of_photos, 
  count(
        CASE
            WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN photo_fact.visit_id
            ELSE NULL::character varying
        END) AS is_not_analyzable, count(
        CASE
            WHEN photo_fact.photo_quality::text = 'analyzable'::character varying::text THEN photo_fact.visit_id
            ELSE NULL::character varying
        END) AS is_analyzable, count(
        CASE
            WHEN photo_fact.photo_quality::text = 'good'::character varying::text THEN photo_fact.visit_id
            ELSE NULL::character varying
        END) AS is_good
   FROM consolidated.photo_fact
   LEFT JOIN consolidated.master_user ON master_user.account_id::text = photo_fact.account_id::text AND master_user.owner_id::text = photo_fact.owner_id::text
   LEFT JOIN consolidated.master_store ON master_store.account_id::text = photo_fact.account_id::text AND master_store.store_id::text = photo_fact.store_id::text
   LEFT JOIN consolidated.visit_fact ON visit_fact.account_id::text = photo_fact.account_id::text AND visit_fact.visit_id::text = photo_fact.visit_id::text
   LEFT JOIN consolidated.calendar ON calendar.visit_date = photo_fact.visit_date AND calendar.account_id::text = photo_fact.account_id::text
   where calendar.year >=2020
  GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_allphoto_per_user TO plano_auto_export;

COMMIT;