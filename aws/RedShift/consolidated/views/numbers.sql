DROP VIEW IF EXISTS consolidated.numbers CASCADE;

CREATE OR REPLACE VIEW consolidated.numbers
(
  number
)
AS 
 SELECT derived_table1.number
   FROM ( SELECT (p0.n + p1.n * 2)::double precision + p2.n::double precision * power(2::double precision, 2::double precision) + p3.n::double precision * power(2::double precision, 3::double precision) + p4.n::double precision * power(2::double precision, 4::double precision) + p5.n::double precision * power(2::double precision, 5::double precision) + p6.n::double precision * power(2::double precision, 6::double precision) + p7.n::double precision * power(2::double precision, 7::double precision) AS number
           FROM ( SELECT 0 AS n
        UNION 
                 SELECT 1) p0, ( SELECT 0 AS n
        UNION 
                 SELECT 1) p1, ( SELECT 0 AS n
        UNION 
                 SELECT 1) p2, ( SELECT 0 AS n
        UNION 
                 SELECT 1) p3, ( SELECT 0 AS n
        UNION 
                 SELECT 1) p4, ( SELECT 0 AS n
        UNION 
                 SELECT 1) p5, ( SELECT 0 AS n
        UNION 
                 SELECT 1) p6, ( SELECT 0 AS n
        UNION 
                 SELECT 1) p7
          ORDER BY (p0.n + p1.n * 2)::double precision + p2.n::double precision * power(2::double precision, 2::double precision) + p3.n::double precision * power(2::double precision, 3::double precision) + p4.n::double precision * power(2::double precision, 4::double precision) + p5.n::double precision * power(2::double precision, 5::double precision) + p6.n::double precision * power(2::double precision, 6::double precision) + p7.n::double precision * power(2::double precision, 7::double precision)) derived_table1
  WHERE derived_table1.number > 0::double precision;

COMMIT;