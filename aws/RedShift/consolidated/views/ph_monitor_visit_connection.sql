DROP VIEW IF EXISTS consolidated.ph_monitor_visit_connection CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_monitor_visit_connection

as
select
  a.account_id,
  a.visit_id,
  a.store_id,
  b.internal_code,
  b.description,
  a.owner_id,
  c.username,
  1 as number_of_visit,
  abs (a.connectiontime) as connection_time,
  abs (a.uploadtime) as upload_time,
  a.account_id || date(a.visiton) :: varchar(255) as uid
from consolidated.monitor_visit_source a
left join consolidated.master_store b
  on b.store_id = a.store_id
left join consolidated.master_user c
  on c.owner_id = a.owner_id
where 
  a.call_type = 0 
  and a.nbphotos > 0
  and date_part (y, date(a.visiton)) >= 2020 
  and ( a.stdet = 'FINAL_READY' or a.stdet = 'AUTO_VERIFIED' or a.stdet = 'LIVE_READY' or a.stdet = 'UPLOADING' or a.stdet = 'PROCESSING')
order by 1,2
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_monitor_visit_connection TO plano_auto_export;


COMMIT;