DROP VIEW IF EXISTS consolidated.ph_accuracy CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_accuracy
(
  uid,
  account_id,
  year_week,
  audit_total_facing,
  audit_total_facing_correct,
  overall_accuracy,
  osa_total_facing,
  osa_correct_facing,
  osa_accuracy,
  number_of_reported_osa,
  number_of_osa,
  ratio_of_reported_osa
)
AS 
 SELECT calendar.account_id::text + calendar.year_week::text AS uid, calendar.account_id, calendar.year_week, sum(accuracy_product_fact.audit_total_facing) AS audit_total_facing, sum(accuracy_product_fact.audit_total_facing_correct) AS audit_total_facing_correct, sum(accuracy_product_fact.audit_total_facing_correct) * 1.00::double precision / sum(accuracy_product_fact.audit_total_facing) * 1.00::double precision AS overall_accuracy, sum(
        CASE
            WHEN accuracy_product_fact.osa = 1::double precision THEN accuracy_product_fact.audit_total_facing
            ELSE NULL::real
        END) AS osa_total_facing, sum(
        CASE
            WHEN accuracy_product_fact.osa = 1::double precision THEN accuracy_product_fact.audit_total_facing_correct
            ELSE NULL::real
        END) AS osa_correct_facing, sum(
        CASE
            WHEN accuracy_product_fact.osa = 1::double precision THEN accuracy_product_fact.audit_total_facing_correct
            ELSE NULL::real
        END) * 1.00::double precision / 
        CASE
            WHEN (sum(
            CASE
                WHEN accuracy_product_fact.osa = 1::double precision THEN accuracy_product_fact.audit_total_facing
                ELSE NULL::real
            END) * 1.00::double precision) = 0::double precision THEN NULL::double precision
            ELSE sum(
            CASE
                WHEN accuracy_product_fact.osa = 1::double precision THEN accuracy_product_fact.audit_total_facing
                ELSE NULL::real
            END) * 1.00::double precision
        END AS osa_accuracy, count(
        CASE
            WHEN accuracy_product_fact.osa = 1::double precision AND user_feedback_fact.product_ean::text = accuracy_product_fact.product_ean::text THEN accuracy_product_fact.product_ean
            ELSE NULL::character varying
        END) AS number_of_reported_osa, count(
        CASE
            WHEN accuracy_product_fact.osa = 1::double precision THEN accuracy_product_fact.product_ean
            ELSE NULL::character varying
        END) AS number_of_osa, count(
        CASE
            WHEN accuracy_product_fact.osa = 1::double precision AND user_feedback_fact.product_ean::text = accuracy_product_fact.product_ean::text THEN accuracy_product_fact.product_ean
            ELSE NULL::character varying
        END)::numeric::numeric(18,0) * 1.00 / 
        CASE
            WHEN (count(
            CASE
                WHEN accuracy_product_fact.osa = 1::double precision THEN accuracy_product_fact.product_ean
                ELSE NULL::character varying
            END)::numeric::numeric(18,0) * 1.00) = 0::numeric::numeric(18,0) THEN NULL::numeric::numeric(18,0)
            ELSE count(
            CASE
                WHEN accuracy_product_fact.osa = 1::double precision THEN accuracy_product_fact.product_ean
                ELSE NULL::character varying
            END)::numeric::numeric(18,0) * 1.00
        END AS ratio_of_reported_osa
   FROM consolidated.accuracy_product_fact
   JOIN consolidated.calendar ON accuracy_product_fact.account_id::text = calendar.account_id::text AND calendar.visit_date = (( SELECT vf.visit_date
      FROM consolidated.visit_fact vf
     WHERE vf.account_id::text = accuracy_product_fact.account_id::text AND vf.visit_id::text = accuracy_product_fact.visit_id::text))
   LEFT JOIN consolidated.user_feedback_fact ON user_feedback_fact.account_id::text = accuracy_product_fact.visit_id::text AND user_feedback_fact.visit_id::text = accuracy_product_fact.visit_id::text AND user_feedback_fact.product_ean::text = accuracy_product_fact.product_ean::text
  GROUP BY calendar.account_id, calendar.year_week
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.ph_accuracy TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.ph_accuracy TO planorama;

COMMIT;