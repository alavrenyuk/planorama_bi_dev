DROP VIEW IF EXISTS consolidated.ph_calendar_extended CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_calendar_extended

as

SELECT 
  a.account_id,
  a.visit_date,
  y.account_name,
  a.year,
  a.month_no,
  a.month_name,
  case when len (a.week) = 6 then 'Week 0' || a.week_no else a.week end as week,
  a.week_no,
  a.year || ' ' || a.quarter as quarter,
  a.quarter_no,
  a.past_year,
  a.past_month_no,
  a.past_week_no,
  a.past_quarter_no,
  a.year || ' ' || a.month_name as year_month,
  left (a.visit_date,8) || '01' as start_of_month,
  case when len (a.year_week) = 11 then a.year || ' Week 0' || a.week_no else a.year_week end as year_week,
  case when (a.quarter_no) = 1 then a.past_year || a.past_quarter_no else a.year || a.past_quarter_no end as past_qtr,
  case when len (case when (a.month_no) = 1 then a.past_year || a.past_month_no else a.year || a.past_month_no end) = 6
    then (case when (a.month_no) = 1 then a.past_year || a.past_month_no else a.year || a.past_month_no end) else
    (case when (a.month_no) = 1 then a.past_year || '0' || a.past_month_no else a.year || '0' || a.past_month_no end) end
    as past_month,
  case when len (case when (a.week_no) = 1 then a.past_year || a.past_week_no else a.year || a.past_week_no end) = 6 
    then (case when (a.week_no) = 1 then a.past_year || a.past_week_no else a.year || a.past_week_no end) else
    (case when (a.week_no) = 1 then a.past_year || '0' || a.past_week_no else a.year || '0' ||  a.past_week_no end) end
    as past_week,
  x.mindate,
  x.maxdate,
  (case when len (a.week) = 6 then 'Week 0' || a.week_no else a.week end) || ' from ' || x.mindate AS week_of,
  case when (date_part (y, current_date) || date_part (w, current_date)) = (a.year || a.week_no) then 0 else 1 end as exclude_crnt_week,
  a.account_id || a.visit_date as uid
FROM consolidated.calendar a
left join 
  (select
    b.year_week,
    min(b.visit_date) AS mindate, 
    max(b.visit_date) AS maxdate
    from  consolidated.calendar b
    group by 1) x
  on a.year_week = x.year_week
left join 
  (select distinct
    c.account_id,
    c.account_name
    from consolidated.monitor_visit_source c) y
  on a.account_id = y.account_id
where a.year >= 2020
order by 1,2
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_calendar_extended TO plano_auto_export;

COMMIT;