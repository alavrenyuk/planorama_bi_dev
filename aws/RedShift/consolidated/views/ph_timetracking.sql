DROP VIEW IF EXISTS consolidated.ph_timetracking CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_timetracking
(
  uid,
  account_id,
  year_week,
  tt,
  ptt
)
AS 
 SELECT calendar.account_id::text + calendar.year_week::text AS uid, calendar.account_id, calendar.year_week, avg(new_time_tracking_fact.duration_end_upload_to_end_processing) AS tt, avg(new_time_tracking_fact.ptt) AS ptt
   FROM consolidated.calendar
   JOIN consolidated.visit_fact ON visit_fact.account_id::text = calendar.account_id::text AND visit_fact.visit_date = calendar.visit_date
   JOIN consolidated.new_time_tracking_fact ON new_time_tracking_fact.account_id::text = visit_fact.account_id::text AND new_time_tracking_fact.visit_id::text = visit_fact.visit_id::text
  GROUP BY concat(calendar.account_id::text, calendar.year_week::text), calendar.account_id, calendar.year_week
  WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.ph_timetracking TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.ph_timetracking TO planorama;


COMMIT;