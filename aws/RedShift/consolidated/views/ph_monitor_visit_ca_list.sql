DROP VIEW IF EXISTS consolidated.ph_monitor_visit_ca_list CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_monitor_visit_ca_list

as
select distinct
  a.account_id,
  a.account_name
from consolidated.monitor_visit_source a
where date_part (y, date(a.visiton)) >= 2020
order by 1
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_monitor_visit_ca_list TO plano_auto_export;


COMMIT;