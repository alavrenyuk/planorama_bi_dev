DROP VIEW IF EXISTS consolidated.ph_calendar_short CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_calendar_short
(
  account_id,
  week,
  week_no,
  year_week,
  uid,
  mindate,
  maxdate,
  week_of
)
AS 
 SELECT calendar.account_id, calendar.week, calendar.week_no, calendar.year_week, calendar.account_id::text + calendar.year_week::text AS uid, min(calendar.visit_date) AS mindate, "max"(calendar.visit_date) AS maxdate, calendar.week::text + ' from '::character varying::text + min(calendar.visit_date)::character varying::text AS week_of
   FROM consolidated.calendar
  GROUP BY calendar.account_id, calendar.week, calendar.week_no, calendar.year_week
  WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.ph_calendar_short TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.ph_calendar_short TO planorama;

COMMIT;