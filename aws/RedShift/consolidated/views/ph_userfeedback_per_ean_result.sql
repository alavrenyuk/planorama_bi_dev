DROP VIEW IF EXISTS consolidated.ph_userfeedback_per_ean_result CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_userfeedback_per_ean_result

AS 
 SELECT calendar.account_id || calendar.visit_date AS uid, calendar.account_id, calendar.visit_date, visit_fact.categ_name, user_feedback_fact.product_ean, count(user_feedback_fact.visit_id) AS sum_of_reports
   FROM consolidated.calendar
   JOIN consolidated.visit_fact ON calendar.account_id::text = visit_fact.account_id::text AND calendar.visit_date = visit_fact.visit_date
   LEFT JOIN consolidated.user_feedback_fact ON user_feedback_fact.account_id::text = visit_fact.account_id::text AND user_feedback_fact.visit_id::text = visit_fact.visit_id::text
  WHERE length(user_feedback_fact.product_ean::text) > 0
  and calendar.year >= 2020
  GROUP BY calendar.account_id, calendar.visit_date, visit_fact.categ_name, user_feedback_fact.product_ean
  WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_userfeedback_per_ean_result TO plano_auto_export;


COMMIT;