DROP VIEW IF EXISTS consolidated.ph_timetracking_per_chain CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_timetracking_per_chain
(
  uid,
  account_id,
  year_week,
  chainstore,
  tt,
  ptt
)
AS 
 SELECT calendar.account_id::text + calendar.year_week::text AS uid, calendar.account_id, calendar.year_week, master_store.chainstore, avg(new_time_tracking_fact.duration_end_upload_to_end_processing) AS tt, avg(new_time_tracking_fact.ptt) AS ptt
   FROM consolidated.calendar
   JOIN consolidated.visit_fact ON visit_fact.account_id::text = calendar.account_id::text AND visit_fact.visit_date = calendar.visit_date
   JOIN consolidated.new_time_tracking_fact ON new_time_tracking_fact.account_id::text = visit_fact.account_id::text AND new_time_tracking_fact.visit_id::text = visit_fact.visit_id::text
   LEFT JOIN consolidated.master_store ON visit_fact.account_id::text = master_store.account_id::text AND visit_fact.store_id::text = master_store.store_id::text
  GROUP BY calendar.account_id, calendar.year_week, master_store.chainstore
  WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.ph_timetracking_per_chain TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.ph_timetracking_per_chain TO planorama;


COMMIT;