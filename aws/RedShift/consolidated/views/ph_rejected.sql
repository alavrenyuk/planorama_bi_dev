DROP VIEW IF EXISTS consolidated.ph_rejected CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_rejected
as

select
  a.account_id || a.visit_date as uid,
  a.account_id,
  a.visit_date,
  count(b.visit_id) AS number_of_pictures,
  count(CASE WHEN b.photo_quality = 'not analyzable' THEN 1 ELSE NULL END) AS is_not_analyzable, 
  count(CASE WHEN b.photo_quality = 'analyzable' THEN 1 ELSE NULL END) AS is_analyzable, 
  count(CASE WHEN b.photo_quality = 'good' THEN 1 ELSE NULL END) AS is_good, 
  count(CASE WHEN b.photo_quality = 'not analyzable' THEN 1 ELSE NULL END) :: decimal / nullif (count(b.visit_id),0) :: decimal AS ratio_of_not_analyzable
from consolidated.calendar a
join consolidated.photo_fact b 
  ON a.visit_date = b.visit_date AND a.account_id = b.account_id
where a.year >= 2020
GROUP BY 2,3
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_rejected TO plano_auto_export;


COMMIT;