DROP VIEW IF EXISTS consolidated.ph_monitor_visit_tt_category CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_monitor_visit_tt_category

as

select 
  a.account_id,
  a.account_id || date(a.visiton) :: varchar(255) as uid,
  a.base_name,
  a.base_id,
  sum(abs(a.tt_live)) as sum_tt_live,
  sum(abs(a.tt_final)) as sum_tt_final,
  sum(abs(a.ptt_live)) as sum_ptt,
  count (case
    when a.late_live = 'true' then 1 end) as late_live_visits,
  count (case
    when a.late_final = 'true' then 1 end) as late_final_visits,
  sum(abs(a.connectiontime)) as sum_connection_time,
  sum(abs(a.uploadtime)) as sum_upload_time,
  sum(abs(a.capturetime)) as sum_capture_time,
  count (a.visit_id) as visits_total,
  count (case when a.late_live = 'true' then 1 end) :: decimal / count (a.visit_id) :: decimal  as visits_above_sla_live,
  count (case when a.late_final = 'true' then 1 end) :: decimal / count (a.visit_id) :: decimal  as visits_above_sla_final,
  sum (a.nbphotos) as sum_nbphotos
from consolidated.monitor_visit_source a
left join
  (select
    b.visit_id,
    SUBSTRING(b.first_closed_on,1,24)::timestamp as firstclosedon_timestamp
    from consolidated.monitor_visit_source b) x
  on x.visit_id = a.visit_id
where a.call_type = 0
  and (a.stdet = 'FINAL_READY' or a.stdet = 'AUTO_VERIFIED' or a.stdet = 'LIVE_READY')
  and a.nbphotos > 0 --anomaly?
  and date_part (y, date(a.visiton)) >= 2020
  and datediff (mins, a.visiton, x.firstclosedon_timestamp) <= 2880 --anomaly?
  and x.firstclosedon_timestamp >= a.visiton --anomaly?
group by 1,2,3,4
order by 1,2,3
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_monitor_visit_tt_category TO plano_auto_export;


COMMIT;