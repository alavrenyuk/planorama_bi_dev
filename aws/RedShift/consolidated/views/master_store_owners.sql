DROP VIEW IF EXISTS consolidated.master_store_owners CASCADE;

CREATE OR REPLACE VIEW consolidated.master_store_owners
(
  account_id,
  store_id,
  owner
)
AS 
 SELECT DISTINCT master_store.account_id, master_store.store_id, split_part(master_store.owners::text, '|'::character varying::text, numbers.number::integer) AS "owner"
   FROM consolidated.master_store
  CROSS JOIN consolidated.numbers
  WHERE split_part(master_store.owners::text, '|'::character varying::text, numbers.number::integer) IS NOT NULL AND split_part(master_store.owners::text, '|'::character varying::text, numbers.number::integer) <> ''::character varying::text
  ORDER BY master_store.account_id, master_store.store_id
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.master_store_owners TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.master_store_owners TO planorama;

COMMIT;