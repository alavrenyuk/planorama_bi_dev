DROP VIEW IF EXISTS consolidated.visit_dimension CASCADE;

CREATE VIEW consolidated.visit_dimension
AS
SELECT DISTINCT visit_id,
       visiton AS visit_date,
       ready_final AS analysis_date,
       firstreceivedon,
       lastreceivedon,
       lastphotoeventon,
       lasttakenon,
       deadline_live,
       ready_live,
       ready_final,
       visit_type,
       base_name AS categ_name,
       first_loaded_on,
       last_modified_on
FROM consolidated.monitor_visit_source
WITH NO SCHEMA BINDING;

COMMIT;