DROP VIEW IF EXISTS consolidated.ph_pqreason CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_pqreason

as
 SELECT calendar.account_id || calendar.visit_date AS uid, calendar.account_id, calendar.visit_date, photo_fact.photo_quality_reason, count(photo_fact.visit_id) AS number_of_photos
   FROM consolidated.calendar
   JOIN consolidated.photo_fact ON calendar.account_id::text = photo_fact.account_id::text AND calendar.visit_date = photo_fact.visit_date
  WHERE photo_fact.photo_quality::text = 'not analyzable'::character varying::text
  and calendar.year >= 2020
  GROUP BY 2,3,4
  WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_pqreason TO plano_auto_export;


COMMIT;