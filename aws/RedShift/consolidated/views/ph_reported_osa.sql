DROP VIEW IF EXISTS consolidated.ph_reported_osa CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_reported_osa

AS 
 SELECT calendar.account_id || calendar.visit_date AS uid, calendar.account_id, calendar.visit_date, count(product_kpi_fact.prod_ean) AS all_osa, sum(
        CASE
            WHEN user_feedback_fact.product_ean::text = product_kpi_fact.prod_ean::text THEN 1
            ELSE 0
        END) AS reported_osa
   FROM consolidated.product_kpi_fact
   LEFT JOIN consolidated.calendar ON product_kpi_fact.visit_date = calendar.visit_date AND product_kpi_fact.account_id::text = calendar.account_id::text
   LEFT JOIN consolidated.user_feedback_fact ON user_feedback_fact.account_id::text = product_kpi_fact.account_id::text AND user_feedback_fact.visit_id::text = product_kpi_fact.visit_id::text AND user_feedback_fact.product_ean::text = product_kpi_fact.prod_ean::text
  where calendar.year >= 2020
  GROUP BY calendar.account_id, calendar.visit_date
  WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_reported_osa TO plano_auto_export;


COMMIT;