DROP VIEW IF EXISTS consolidated.ph_coverage CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_coverage
as
select
  visit_fact.account_id,
  visit_fact.account_id || visit_fact.visit_date ::  varchar(255) as uid,
  x.number_of_stores,
  count (distinct (visit_fact.store_id)) as number_of_visited_stores,
  count (distinct (visit_fact.store_id)) :: decimal / x.number_of_stores :: decimal as coverage_vs_master
from consolidated.visit_fact
  inner join
    (select
      master_store.account_id,
      count (master_store.store_id) as number_of_stores
      from consolidated.master_store
      where master_store.is_deleted = false
      group by 1) x
  on visit_fact.account_id = x.account_id 
where date_part (y,date(visit_fact.visit_date)) >=2020
group by 1,2,3
order by 2
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_coverage TO plano_auto_export;

COMMIT;