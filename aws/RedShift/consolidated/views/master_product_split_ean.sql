DROP VIEW IF EXISTS consolidated.master_product_split_ean CASCADE;

CREATE OR REPLACE VIEW consolidated.master_product_split_ean
(
  account_id,
  product_id,
  base,
  product_ean,
  description,
  manufacturer,
  brand,
  height,
  width,
  depth,
  code,
  crea_on,
  mod_on,
  seg1,
  seg2,
  seg3,
  seg4,
  seg5,
  desc_a,
  desc_b,
  desc_c,
  desc_d,
  desc_e,
  desc_f,
  desc_g,
  desc_h,
  desc_i,
  desc_j,
  kind,
  labelling,
  is_validated,
  excl_from,
  img,
  is_deleted
)
AS 
 SELECT master_product.account_id, master_product.product_id, master_product.base, split_part(master_product.product_ean::text, '/'::character varying::text, numbers.number::integer) AS product_ean, master_product.description, master_product.manufacturer, master_product.brand, master_product.height, master_product.width, master_product.depth, master_product.code, master_product.crea_on, master_product.mod_on, master_product.seg1, master_product.seg2, master_product.seg3, master_product.seg4, master_product.seg5, master_product.desc_a, master_product.desc_b, master_product.desc_c, master_product.desc_d, master_product.desc_e, master_product.desc_f, master_product.desc_g, master_product.desc_h, master_product.desc_i, master_product.desc_j, master_product.kind, master_product.labelling, master_product.is_validated, master_product.excl_from, master_product.img, master_product.is_deleted
   FROM consolidated.master_product
  CROSS JOIN consolidated.numbers
  WHERE split_part(master_product.product_ean::text, '/'::character varying::text, numbers.number::integer) IS NOT NULL AND split_part(master_product.product_ean::text, '/'::character varying::text, numbers.number::integer) <> ''::character varying::text
WITH NO SCHEMA BINDING;

COMMIT;