DROP VIEW IF EXISTS consolidated.ph_home_timetracking CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_home_timetracking
(
  account_id,
  week_no,
  year_week,
  uid,
  current_tt,
  current_ptt,
  prev_week_tt,
  prev_week_ptt
)
AS 
 SELECT calendar.account_id, calendar.week_no, calendar.year_week, concat(calendar.account_id::text, calendar.year_week::text) AS uid, avg(new_time_tracking_fact.duration_end_upload_to_end_processing) AS current_tt, avg(new_time_tracking_fact.ptt) AS current_ptt, pg_catalog.lead(avg(new_time_tracking_fact.duration_end_upload_to_end_processing), 1)
  OVER( 
  PARTITION BY calendar.account_id
  ORDER BY calendar.account_id DESC, calendar.week_no DESC) AS prev_week_tt, pg_catalog.lead(avg(new_time_tracking_fact.ptt), 1)
  OVER( 
  PARTITION BY calendar.account_id
  ORDER BY calendar.account_id DESC, calendar.week_no DESC) AS prev_week_ptt
   FROM consolidated.calendar
   JOIN consolidated.visit_fact ON visit_fact.account_id::text = calendar.account_id::text AND visit_fact.visit_date = calendar.visit_date
   JOIN consolidated.new_time_tracking_fact ON new_time_tracking_fact.account_id::text = visit_fact.account_id::text AND new_time_tracking_fact.visit_id::text = visit_fact.visit_id::text
  GROUP BY calendar.account_id, calendar.week_no, calendar.year_week
  ORDER BY calendar.account_id, calendar.week_no
  WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.ph_home_timetracking TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.ph_home_timetracking TO planorama;


COMMIT;