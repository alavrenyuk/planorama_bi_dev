DROP VIEW IF EXISTS consolidated.ph_home_volume CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_home_volume

as
SELECT 
  calendar.account_id,
  calendar.year,
  calendar.week_no, 
  calendar.year_week, 
  calendar.account_id::text + calendar.year_week::text AS uid, 
  sum(visit_fact.number_of_pictures) AS number_of_pictures, 
  pg_catalog.lead(sum(visit_fact.number_of_pictures), 1)
    OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC) AS prev_week_photos, 
  count(visit_fact.visit_id) AS number_of_visits, 
  pg_catalog.lead(count(visit_fact.visit_id), 1)
    OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC) AS prev_week_visits,
  case when pg_catalog.lead(sum(visit_fact.number_of_pictures), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC) > 0 then
    sum(visit_fact.number_of_pictures) / pg_catalog.lead(sum(visit_fact.number_of_pictures), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC)-1
    else 1 end as change_ratio_photo,
  case when pg_catalog.lead(count(visit_fact.visit_id), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC) > 0 then
    count(visit_fact.visit_id) :: decimal / pg_catalog.lead(count(visit_fact.visit_id), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC)-1 :: decimal
    else 1 end as change_ratio_visit,
  case when
    ( case when pg_catalog.lead(sum(visit_fact.number_of_pictures), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC) > 0 then
    sum(visit_fact.number_of_pictures) / pg_catalog.lead(sum(visit_fact.number_of_pictures), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC)-1
    else 1 end ) between -0.25 and 0.25 then 0 else 1 end as pic_volume_flagged,
  case when
    ( case when pg_catalog.lead(count(visit_fact.visit_id), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC) > 0 then
    count(visit_fact.visit_id) :: decimal / pg_catalog.lead(count(visit_fact.visit_id), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC)-1 :: decimal
    else 1 end ) between -0.25 and 0.25 then 0 else 1 end as visit_volume_flagged,
  case when
    ( sum(visit_fact.number_of_pictures) > pg_catalog.lead(sum(visit_fact.number_of_pictures), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC))
    then 2
    when ( sum(visit_fact.number_of_pictures) = pg_catalog.lead(sum(visit_fact.number_of_pictures), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC))
    then 1
    when ( sum(visit_fact.number_of_pictures) < pg_catalog.lead(sum(visit_fact.number_of_pictures), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC))
    then 0
    else 2 end as pic_change_direction,
  case when
    (  count(visit_fact.visit_id) > pg_catalog.lead(count(visit_fact.visit_id), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC))
    then 2
    when (  count(visit_fact.visit_id) = pg_catalog.lead(count(visit_fact.visit_id), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC))
    then 1
    when (  count(visit_fact.visit_id) < pg_catalog.lead(count(visit_fact.visit_id), 1) OVER(PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC))
    then 0
    else 2 end as visit_change_direction
FROM consolidated.calendar
JOIN consolidated.visit_fact ON calendar.visit_date = visit_fact.visit_date AND calendar.account_id::text = visit_fact.account_id::text
GROUP BY calendar.account_id, calendar.year, calendar.week_no, calendar.year_week
ORDER BY calendar.account_id, calendar.year, calendar.week_no

WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.ph_home_volume TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.ph_home_volume TO planorama;


COMMIT;