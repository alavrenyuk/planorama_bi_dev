DROP VIEW IF EXISTS consolidated.accuracy_product_fact_opex_view CASCADE;

CREATE OR REPLACE VIEW consolidated.accuracy_product_fact_opex_view
(
  account_id,
  account_name,
  category,
  creation_date,
  visit_id,
  audit_operator,
  audit_total_sku_correct,
  audit_total_sku,
  verif_total_sku,
  precision_audit_all_sku,
  recall_audit_all_sku,
  audit_own_sku_correct,
  audit_own_sku,
  verif_own_sku,
  precision_audit_own_sku,
  recall_audit_own_sku,
  audit_total_facing_correct,
  audit_total_facing,
  verif_total_facing,
  precision_audit_all_facing,
  recall_audit_all_facing,
  audit_own_facing_correct,
  audit_own_facing,
  verif_own_facing,
  precision_audit_own_facing,
  recall_audit_own_facing,
  f1_score_audit_all_sku,
  f1_score_audit_own_sku,
  f1_score_audit_all_facing,
  f1_score_audit_own_facing
)
AS 
 SELECT accuracy_product_fact.account_id, accuracy_product_fact.account_name, accuracy_product_fact.category, accuracy_product_fact.creation_date, accuracy_product_fact.visit_id, accuracy_product_fact.audit_operator, sum(accuracy_product_fact.audit_total_sku_correct) AS audit_total_sku_correct, sum(accuracy_product_fact.audit_total_sku) AS audit_total_sku, sum(accuracy_product_fact.verif_total_sku) AS verif_total_sku, sum(accuracy_product_fact.audit_total_sku_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.verif_total_sku) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.verif_total_sku)
        END::numeric(18,0) AS precision_audit_all_sku, sum(accuracy_product_fact.audit_total_sku_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.audit_total_sku) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.audit_total_sku)
        END::numeric(18,0) AS recall_audit_all_sku, sum(accuracy_product_fact.audit_own_sku_correct) AS audit_own_sku_correct, sum(accuracy_product_fact.audit_own_sku) AS audit_own_sku, sum(accuracy_product_fact.verif_own_sku) AS verif_own_sku, sum(accuracy_product_fact.audit_own_sku_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.verif_own_sku) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.verif_own_sku)
        END::numeric(18,0) AS precision_audit_own_sku, sum(accuracy_product_fact.audit_own_sku_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.audit_own_sku) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.audit_own_sku)
        END::numeric(18,0) AS recall_audit_own_sku, sum(accuracy_product_fact.audit_total_facing_correct) AS audit_total_facing_correct, sum(accuracy_product_fact.audit_total_facing) AS audit_total_facing, sum(accuracy_product_fact.verif_total_facing) AS verif_total_facing, sum(accuracy_product_fact.audit_total_facing_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.verif_total_facing) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.verif_total_facing)
        END::numeric(18,0) AS precision_audit_all_facing, sum(accuracy_product_fact.audit_total_facing_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.audit_total_facing) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.audit_total_facing)
        END::numeric(18,0) AS recall_audit_all_facing, sum(accuracy_product_fact.audit_own_facing_correct) AS audit_own_facing_correct, sum(accuracy_product_fact.audit_own_facing) AS audit_own_facing, sum(accuracy_product_fact.verif_own_facing) AS verif_own_facing, sum(accuracy_product_fact.audit_own_facing_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.verif_own_facing) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.verif_own_facing)
        END::numeric(18,0) AS precision_audit_own_facing, sum(accuracy_product_fact.audit_own_facing_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.audit_own_facing) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.audit_own_facing)
        END::numeric(18,0) AS recall_audit_own_facing, 2::numeric * (sum(accuracy_product_fact.audit_total_sku_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.audit_total_sku) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.audit_total_sku)
        END::numeric(18,0) * sum(accuracy_product_fact.audit_total_sku_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.verif_total_sku) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.verif_total_sku)
        END::numeric(18,0)) / 
        CASE
            WHEN (sum(accuracy_product_fact.audit_total_sku_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.audit_total_sku) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.audit_total_sku)
            END::numeric(18,0) + sum(accuracy_product_fact.audit_total_sku_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.verif_total_sku) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.verif_total_sku)
            END::numeric(18,0)) = 0::numeric THEN NULL::numeric
            ELSE sum(accuracy_product_fact.audit_total_sku_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.audit_total_sku) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.audit_total_sku)
            END::numeric(18,0) + sum(accuracy_product_fact.audit_total_sku_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.verif_total_sku) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.verif_total_sku)
            END::numeric(18,0)
        END AS f1_score_audit_all_sku, 2::numeric * (sum(accuracy_product_fact.audit_own_sku_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.audit_own_sku) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.audit_own_sku)
        END::numeric(18,0) * sum(accuracy_product_fact.audit_own_sku_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.verif_own_sku) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.verif_own_sku)
        END::numeric(18,0)) / 
        CASE
            WHEN (sum(accuracy_product_fact.audit_own_sku_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.audit_own_sku) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.audit_own_sku)
            END::numeric(18,0) + sum(accuracy_product_fact.audit_own_sku_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.verif_own_sku) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.verif_own_sku)
            END::numeric(18,0)) = 0::numeric THEN NULL::numeric
            ELSE sum(accuracy_product_fact.audit_own_sku_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.audit_own_sku) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.audit_own_sku)
            END::numeric(18,0) + sum(accuracy_product_fact.audit_own_sku_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.verif_own_sku) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.verif_own_sku)
            END::numeric(18,0)
        END AS f1_score_audit_own_sku, 2::numeric * (sum(accuracy_product_fact.audit_total_facing_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.audit_total_facing) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.audit_total_facing)
        END::numeric(18,0) * sum(accuracy_product_fact.audit_total_facing_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.verif_total_facing) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.verif_total_facing)
        END::numeric(18,0)) / 
        CASE
            WHEN (sum(accuracy_product_fact.audit_total_facing_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.audit_total_facing) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.audit_total_facing)
            END::numeric(18,0) + sum(accuracy_product_fact.audit_total_facing_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.verif_total_facing) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.verif_total_facing)
            END::numeric(18,0)) = 0::numeric THEN NULL::numeric
            ELSE sum(accuracy_product_fact.audit_total_facing_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.audit_total_facing) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.audit_total_facing)
            END::numeric(18,0) + sum(accuracy_product_fact.audit_total_facing_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.verif_total_facing) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.verif_total_facing)
            END::numeric(18,0)
        END AS f1_score_audit_all_facing, 2::numeric * (sum(accuracy_product_fact.audit_own_facing_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.audit_own_facing) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.audit_own_facing)
        END::numeric(18,0) * sum(accuracy_product_fact.audit_own_facing_correct)::numeric(18,0) / 
        CASE
            WHEN sum(accuracy_product_fact.verif_own_facing) = 0::double precision THEN NULL::double precision
            ELSE sum(accuracy_product_fact.verif_own_facing)
        END::numeric(18,0)) / 
        CASE
            WHEN (sum(accuracy_product_fact.audit_own_facing_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.audit_own_facing) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.audit_own_facing)
            END::numeric(18,0) + sum(accuracy_product_fact.audit_own_facing_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.verif_own_facing) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.verif_own_facing)
            END::numeric(18,0)) = 0::numeric THEN NULL::numeric
            ELSE sum(accuracy_product_fact.audit_own_facing_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.audit_own_facing) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.audit_own_facing)
            END::numeric(18,0) + sum(accuracy_product_fact.audit_own_facing_correct)::numeric(18,0) / 
            CASE
                WHEN sum(accuracy_product_fact.verif_own_facing) = 0::double precision THEN NULL::double precision
                ELSE sum(accuracy_product_fact.verif_own_facing)
            END::numeric(18,0)
        END AS f1_score_audit_own_facing
   FROM consolidated.accuracy_product_fact
  WHERE accuracy_product_fact.audit_operator::text <> ''::text AND "left"(accuracy_product_fact.creation_date::text, 4) = '2020'::text
  GROUP BY accuracy_product_fact.account_id, accuracy_product_fact.account_name, accuracy_product_fact.category, accuracy_product_fact.creation_date, accuracy_product_fact.visit_id, accuracy_product_fact.audit_operator
  ORDER BY accuracy_product_fact.account_id, accuracy_product_fact.category, accuracy_product_fact.creation_date, accuracy_product_fact.visit_id;


GRANT SELECT ON consolidated.accuracy_product_fact_opex_view TO powerbi_direct;
GRANT SELECT ON consolidated.accuracy_product_fact_opex_view TO powerbi_user1;
GRANT DELETE, REFERENCES, UPDATE, RULE, TRIGGER, SELECT, INSERT ON consolidated.accuracy_product_fact_opex_view TO plano_auto_export;
GRANT TRIGGER, REFERENCES, DELETE, INSERT, UPDATE, RULE, SELECT ON consolidated.accuracy_product_fact_opex_view TO planorama;

COMMIT;