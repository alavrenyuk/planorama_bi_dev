DROP VIEW IF EXISTS consolidated.ph_accuracy_export CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_accuracy_export
(
  uid,
  account_id,
  year_week,
  visit_id,
  category,
  product_ean,
  number_of_reported_osa
)
AS 
 SELECT calendar.account_id::text + calendar.year_week::text AS uid, calendar.account_id, calendar.year_week, visit_fact.visit_id, visit_fact.categ_name AS category, accuracy_product_fact.product_ean, count(
        CASE
            WHEN accuracy_product_fact.osa = 1::double precision AND user_feedback_fact.product_ean::text = accuracy_product_fact.product_ean::text THEN accuracy_product_fact.product_ean
            ELSE NULL::character varying
        END) AS number_of_reported_osa
   FROM consolidated.visit_fact
   JOIN consolidated.calendar ON visit_fact.account_id::text = calendar.account_id::text AND visit_fact.visit_date = calendar.visit_date
   LEFT JOIN consolidated.accuracy_product_fact ON accuracy_product_fact.account_id::text = visit_fact.account_id::text AND accuracy_product_fact.visit_id::text = visit_fact.visit_id::text
   LEFT JOIN consolidated.user_feedback_fact ON user_feedback_fact.account_id::text = accuracy_product_fact.visit_id::text AND user_feedback_fact.visit_id::text = accuracy_product_fact.visit_id::text AND user_feedback_fact.product_ean::text = accuracy_product_fact.product_ean::text
  GROUP BY visit_fact.account_id, visit_fact.visit_id, calendar.account_id, calendar.year_week, visit_fact.categ_name, accuracy_product_fact.product_ean
  WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.ph_accuracy_export TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.ph_accuracy_export TO planorama;

COMMIT;