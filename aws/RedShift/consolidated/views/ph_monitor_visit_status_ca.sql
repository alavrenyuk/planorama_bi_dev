DROP VIEW IF EXISTS consolidated.ph_monitor_visit_status_ca CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_monitor_visit_status_ca

as

select 
  a.account_id,
  a.account_id || date (a.visiton) :: varchar(255) as uid,
  count (case
    when a.stdet = 'UPLOADING' then 1 end) as visits_uploading,
  count (case
    when a.stdet = 'PROCESSING' then 1 end) as visits_processing,
  count (case
    when a.stdet = 'LIVE_READY' then 1 end) as visits_live_rdy,
  count (case
    when a.stdet = 'FINAL_READY' or a.stdet = 'AUTO_VERIFIED'  then 1 end) as visits_verified,
  count (case
    when a.stdet = 'DELETED' then 1 end) as visits_deleted,
  count (case
    when a.stdet = 'SKIPPED' then 1 end) as visits_skipped,
  count (case
    when a.stdet = 'SKIPPED' or a.stdet = 'DELETED' then 1 end) as visits_not_processed,
  count (case
    when a.stdet = 'LIVE_READY' or a.stdet = 'FINAL_READY' or a.stdet = 'AUTO_VERIFIED' then 1 end) as visits_analysed,
  count (case
    when a.stdet = 'UPLOADING' or a.stdet = 'PROCESSING' then 1 end) as visits_under_analysis,
  count (case
    when a.stdet = 'LIVE_READY' or a.stdet = 'FINAL_READY' or a.stdet = 'AUTO_VERIFIED' or a.stdet = 'UPLOADING' or a.stdet = 'PROCESSING' then 1 end) as visits_total_relevant,
  count (a.visit_id) as visits_total_w_not_relevant,
  count (case
    when a.stdet = 'N/A' then 1 end) as visits_not_from_pma,
  count (case
    when a.call_type = 0 then 1 end) as visits_digital,
  count (case
    when a.call_type = 1 then 1 end) as visits_manual
from consolidated.monitor_visit_source a
where date_part (y, date (a.visiton)) >= 2020
group by 1,2
order by 1,2
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_monitor_visit_status_ca TO plano_auto_export;


COMMIT;