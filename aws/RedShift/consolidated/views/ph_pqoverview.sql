DROP VIEW IF EXISTS consolidated.ph_pqoverview CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_pqoverview
(
  uid,
  account_id,
  year_week,
  photo_quality,
  is_not_analyzable,
  is_analyzable,
  is_good,
  number_of_photos
)
AS 
 SELECT calendar.account_id::text + calendar.year_week::text AS uid, calendar.account_id, calendar.year_week, photo_fact.photo_quality, count(
        CASE
            WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN photo_fact.visit_id
            ELSE NULL::character varying
        END) AS is_not_analyzable, count(
        CASE
            WHEN photo_fact.photo_quality::text = 'analyzable'::character varying::text THEN photo_fact.visit_id
            ELSE NULL::character varying
        END) AS is_analyzable, count(
        CASE
            WHEN photo_fact.photo_quality::text = 'good'::character varying::text THEN photo_fact.visit_id
            ELSE NULL::character varying
        END) AS is_good, count(photo_fact.visit_id) AS number_of_photos
   FROM consolidated.calendar
   JOIN consolidated.photo_fact ON calendar.account_id::text = photo_fact.account_id::text AND calendar.visit_date = photo_fact.visit_date
  GROUP BY calendar.account_id, calendar.year_week, photo_fact.photo_quality
  WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.ph_pqoverview TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.ph_pqoverview TO planorama;


COMMIT;