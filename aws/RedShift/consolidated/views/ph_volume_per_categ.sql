DROP VIEW IF EXISTS consolidated.ph_volume_per_categ CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_volume_per_categ

as
select
  a.account_id || a.visit_date as uid,
  a.account_id,
  a.visit_date,
  b.base_name as categ_name,
  count (b.visit_id) as number_of_visits,
  sum (b.nbphotos) as number_of_pictures,
  sum (b.nbphotos) :: decimal / nullif(count (b.visit_id),0) :: decimal as avg_photos_per_visit
from consolidated.calendar a
join consolidated.monitor_visit_source b
  on a.account_id = b.account_id and a.visit_date = date (b.visiton)
where b.nbphotos > 0 and b.call_type = 0
  and not b.stdet = 'SKIPPED' and not b.stdet = 'DELETED'
and a.year >= 2020
group by 2,3,4
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_volume_per_categ TO plano_auto_export;


COMMIT;