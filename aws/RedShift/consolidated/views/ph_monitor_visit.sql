DROP VIEW IF EXISTS consolidated.ph_monitor_visit CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_monitor_visit
(
  account_id,
  visit_id,
  account_name,
  base_id,
  base_name,
  store_id,
  visiton,
  firstreceivedon,
  lastreceivedon,
  lastphotoeventon,
  lasttakenon,
  stdet,
  nbphotos,
  uplphotos,
  allup,
  owner_id,
  username,
  deadline_live,
  deadline_final,
  ready_live,
  ready_final,
  accuracy,
  tt_live,
  tt_final,
  ptt_live,
  ptt_final,
  late_live,
  late_quasi,
  late_final,
  connectiontime,
  uploadtime,
  capturetime,
  isreopened,
  lastreopen,
  visit_type,
  has_negative_value,
  uid
)
AS 
 SELECT monitor_visit.account_id, monitor_visit.visit_id, monitor_visit.account_name, monitor_visit.base_id, monitor_visit.base_name, monitor_visit.store_id, monitor_visit.visiton, monitor_visit.firstreceivedon, monitor_visit.lastreceivedon, monitor_visit.lastphotoeventon, monitor_visit.lasttakenon, monitor_visit.stdet, monitor_visit.nbphotos, monitor_visit.uplphotos, monitor_visit.allup, monitor_visit.owner_id, monitor_visit.username, monitor_visit.deadline_live, monitor_visit.deadline_final, monitor_visit.ready_live, monitor_visit.ready_final, monitor_visit.accuracy, monitor_visit.tt_live, monitor_visit.tt_final, monitor_visit.ptt_live, monitor_visit.ptt_final, monitor_visit.late_live, monitor_visit.late_quasi, monitor_visit.late_final, monitor_visit.connectiontime, monitor_visit.uploadtime, monitor_visit.capturetime, monitor_visit.isreopened, monitor_visit.lastreopen, monitor_visit.visit_type, 
        CASE
            WHEN monitor_visit.tt_live < 0::double precision THEN 1
            WHEN monitor_visit.connectiontime < 0::double precision THEN 1
            ELSE 0
        END AS has_negative_value, monitor_visit.account_id::text + pgdate_part('year'::text, monitor_visit.visiton)::character varying(255)::text + ' Week '::text + pgdate_part('w'::text, monitor_visit.visiton)::character varying(255)::text AS uid
   FROM consolidated.monitor_visit
   WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.ph_monitor_visit TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.ph_monitor_visit TO planorama;


COMMIT;