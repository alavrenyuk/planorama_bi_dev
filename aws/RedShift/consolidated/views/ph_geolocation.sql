DROP VIEW IF EXISTS consolidated.ph_geolocation CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_geolocation

AS 
SELECT 
  geolocation.account_id, 
  geolocation.visit_id, 
  geolocation.visit_date, 
  b.username, 
  geolocation.store_code, 
  geolocation.number_of_pictures, 
  geolocation.photo_index, 
  geolocation.store_latitude, 
  geolocation.store_longitude, 
  geolocation.photo_latitude, 
  geolocation.photo_longitude, 
  geolocation.location_accuracy, 
  geolocation.suspicious_photo, 
  geolocation.photo_distance_tolerance, 
  geolocation.location_difference, 
  geolocation.account_id || date(geolocation.visit_date) :: varchar(255) AS uid
FROM consolidated.geolocation
left join consolidated.monitor_visit_source b
  on geolocation.visit_id = b.visit_id
where date_part (y, date (geolocation.visit_date)) >= 2020 and  geolocation.suspicious_photo = '1'
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_geolocation TO plano_auto_export;


COMMIT;