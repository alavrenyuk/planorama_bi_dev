DROP VIEW IF EXISTS consolidated.ph_userfeedback_per_toggle CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_userfeedback_per_toggle

AS 
SELECT 
 a.account_id || a.visit_date AS uid, 
 a.account_id, 
 a.visit_date, 
 b.categ_name,
 c.visit_id, 
 c.product_ean, 
 count(case when c.toggle1 = 'present' then 1 end) as present,
 count(case when c.toggle1 = 'void' then 1 end) as void,
 count(case when c.toggle1 = 'oos' then 1 end) as oos,
 count(case when c.toggle1 = 'not_present' then 1 end) as not_present
FROM consolidated.calendar a
JOIN consolidated.visit_fact  b
  ON a.account_id::text = b.account_id::text AND a.visit_date = b.visit_date
LEFT JOIN consolidated.user_feedback_fact c
  ON c.account_id::text = b.account_id::text AND c.visit_id::text = b.visit_id::text
WHERE length(c.product_ean::text) > 0
and a.year >= 2020
GROUP BY 2,3,4,5,6
  WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, UPDATE, RULE, INSERT, REFERENCES ON consolidated.ph_userfeedback_per_toggle TO plano_auto_export;


COMMIT;