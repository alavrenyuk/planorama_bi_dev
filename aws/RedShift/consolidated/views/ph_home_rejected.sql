DROP VIEW IF EXISTS consolidated.ph_home_rejected CASCADE;

CREATE OR REPLACE VIEW consolidated.ph_home_rejected

as
SELECT 
  calendar.account_id,
  calendar.year,
  calendar.week_no, 
  calendar.year_week, 
  concat(calendar.account_id::text, calendar.year_week::text) AS uid, 
  count(photo_fact.visit_id) AS number_of_pictures, 
  count(CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END) AS is_not_analyzable, 
  pg_catalog.lead(count(photo_fact.visit_id), 1) OVER( PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.week_no DESC) AS prev_week_photos, 
  pg_catalog.lead(count
    ( CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END), 1) 
    OVER( PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC) AS prev_week_rejected,
  count(CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END) :: decimal / count(photo_fact.visit_id) :: decimal as ratio_rejected_crnt,
  pg_catalog.lead(count
    ( CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END), 1) 
    OVER( PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC) :: decimal / 
      pg_catalog.lead(count(photo_fact.visit_id), 1) OVER( PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.week_no DESC) :: decimal as ratio_rejected_prev,
  case when
    ( count(CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END)) >
    (pg_catalog.lead(count
    ( CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END), 1) 
    OVER( PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC)) then 0
    when
    ( count(CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END)) =
    (pg_catalog.lead(count
    ( CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END), 1) 
    OVER( PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC)) then 1
    when
    ( count(CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END)) <
    (pg_catalog.lead(count
    ( CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END), 1) 
    OVER( PARTITION BY calendar.account_id ORDER BY calendar.account_id DESC, calendar.year DESC, calendar.week_no DESC)) then 2
    end as rejected_change_dir,
  case when 
    ( count(CASE WHEN photo_fact.photo_quality::text = 'not analyzable'::character varying::text THEN 1 ELSE NULL::integer END) :: decimal / 
      count(photo_fact.visit_id) :: decimal) >= 0.05 then 1 else 0 end as rejected_flag
FROM consolidated.calendar
JOIN consolidated.photo_fact ON calendar.visit_date = photo_fact.visit_date AND calendar.account_id::text = photo_fact.account_id::text
GROUP BY calendar.account_id, calendar.year, calendar.week_no, calendar.year_week
ORDER BY calendar.account_id, calendar.year, calendar.week_no
WITH NO SCHEMA BINDING;

GRANT TRIGGER, SELECT, DELETE, REFERENCES, UPDATE, RULE, INSERT ON consolidated.ph_home_rejected TO plano_auto_export;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.ph_home_rejected TO planorama;


COMMIT;