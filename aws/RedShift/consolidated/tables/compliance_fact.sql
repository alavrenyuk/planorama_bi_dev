DROP TABLE IF EXISTS consolidated.compliance_fact CASCADE;

CREATE TABLE consolidated.compliance_fact
(
   account_id        varchar(50)    NOT NULL,
   visit_id          varchar(50)    NOT NULL,
   kpi               varchar(255),
   kpi_id            float8,
   value             float8,
   compliant         boolean,
   threshold         float8,
   weight            float8,
   field             varchar(255),
   group_name        varchar(255),
   parent            varchar(255),
   field_value       varchar(255),
   force_na          boolean,
   first_loaded_on   timestamptz,
   last_modified_on  timestamptz
);

ALTER TABLE consolidated.compliance_fact
   ADD CONSTRAINT pk_compliance_fact
   PRIMARY KEY (account_id, visit_id);

GRANT SELECT ON consolidated.compliance_fact TO group powerbi_direct;
GRANT SELECT ON consolidated.compliance_fact TO powerbi_direct;

COMMIT;