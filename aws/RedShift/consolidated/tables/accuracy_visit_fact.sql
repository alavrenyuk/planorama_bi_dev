DROP TABLE IF EXISTS consolidated.accuracy_visit_fact CASCADE;

CREATE TABLE consolidated.accuracy_visit_fact
(
   account_id                     varchar(50)    NOT NULL,
   visit_id                       varchar(50)    NOT NULL,
   number_of_pictures             float4,
   created_on                     timestamptz,
   audit_operator                 varchar(255),
   total_present_sku              float4,
   total_present_sku_correct      float4,
   own_present_sku                float4,
   own_present_sku_correct        float4,
   total_present_facings          float4,
   total_present_facings_correct  float4,
   own_present_facings            float4,
   own_present_facings_correct    float4,
   audit_total_sku                float4,
   audit_total_sku_correct        float4,
   audit_own_sku                  float4,
   audit_own_sku_correct          float4,
   audit_total_facing             float4,
   audit_total_facing_correct     float4,
   audit_own_facing               float4,
   audit_own_facing_correct       float4,
   audit_precision_sku            float8,
   audit_precision_sku_own        float8,
   audit_precision_facing         float8,
   audit_precision_facing_own     float8,
   audit_recall_sku               float8,
   audit_recall_sku_own           float8,
   audit_recall_facing            float8,
   audit_recall_facing_own        float8,
   first_loaded_on                timestamptz,
   last_modified_on               timestamptz
);

ALTER TABLE consolidated.accuracy_visit_fact
   ADD CONSTRAINT pk_accuracy_visit_fact
   PRIMARY KEY (account_id, visit_id);

GRANT SELECT ON consolidated.accuracy_visit_fact TO group powerbi_direct;
GRANT SELECT ON consolidated.accuracy_visit_fact TO powerbi_direct;

COMMIT;