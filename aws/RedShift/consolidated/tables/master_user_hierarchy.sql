DROP TABLE IF EXISTS consolidated.master_user_hierarchy CASCADE;

CREATE TABLE consolidated.master_user_hierarchy
(
   account_id         varchar(50),
   owner_name_parent  varchar(50),
   owner_name_child   varchar(50),
   first_loaded_on    timestamptz,
   last_modified_on   timestamptz
);

GRANT SELECT ON consolidated.master_user_hierarchy TO group powerbi_direct;
GRANT SELECT ON consolidated.master_user_hierarchy TO powerbi_direct;

COMMIT;