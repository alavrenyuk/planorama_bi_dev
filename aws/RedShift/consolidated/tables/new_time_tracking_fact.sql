DROP TABLE IF EXISTS consolidated.new_time_tracking_fact CASCADE;

CREATE TABLE consolidated.new_time_tracking_fact
(
   account_id                             varchar(50)    NOT NULL,
   visit_id                               varchar(50)    NOT NULL,
   number_of_pictures                     float4,
   category_id                            varchar(50),
   category_name                          varchar(255),
   start_photo_capture                    timestamp,
   end_photo_capture                      timestamp,
   duration_photo_capture                 float4,
   visit_creation_on_server               timestamp,
   waiting_time_before_upload             float4,
   start_photo_upload                     timestamp,
   end_photo_upload                       timestamp,
   duration_upload                        float4,
   start_processing                       timestamp,
   end_processing                         timestamp,
   duration_processing                    float4,
   duration_end_upload_to_end_processing  float4,
   start_labelling                        timestamp,
   end_labelling                          timestamp,
   duration_labelling                     float4,
   start_verification                     timestamp,
   end_verification                       timestamp,
   duration_verification                  float4,
   ptt                                    float4,
   first_loaded_on                        timestamptz,
   last_modified_on                       timestamptz
);

ALTER TABLE consolidated.new_time_tracking_fact
   ADD CONSTRAINT pk_new_time_tracking_fact
   PRIMARY KEY (account_id, visit_id);

GRANT SELECT ON consolidated.new_time_tracking_fact TO group powerbi_direct;
GRANT SELECT ON consolidated.new_time_tracking_fact TO powerbi_direct;

COMMIT;