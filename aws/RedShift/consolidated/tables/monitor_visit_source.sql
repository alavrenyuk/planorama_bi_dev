DROP TABLE IF EXISTS consolidated.monitor_visit_source CASCADE;

CREATE TABLE consolidated.monitor_visit_source
(
   account_id        varchar(255)   NOT NULL,
   visit_id          varchar(255)   NOT NULL,
   account_name      varchar(255),
   base_id           varchar(255),
   base_name         varchar(255),
   store_id          varchar(255),
   visiton           timestamp,
   firstreceivedon   timestamp,
   lastreceivedon    timestamp,
   lastphotoeventon  timestamp,
   lasttakenon       timestamp,
   stdet             varchar(255),
   nbphotos          float8,
   uplphotos         float8,
   allup             varchar(255),
   owner_id          varchar(255),
   username          varchar(255),
   deadline_live     timestamp,
   deadline_final    timestamp,
   ready_live        timestamp,
   ready_final       timestamp,
   accuracy          float8,
   tt_live           float8,
   tt_final          float8,
   ptt_live          float8,
   ptt_final         float8,
   late_live         varchar(255),
   late_quasi        varchar(255),
   late_final        varchar(255),
   connectiontime    float8,
   uploadtime        float8,
   capturetime       float8,
   isreopened        varchar(255),
   lastreopen        timestamp,
   first_closed_on   varchar(255),
   duplicate         varchar(255),
   call_type         integer,
   pending_submit    varchar(255),
   has_low_quality   varchar(255),
   has_med_quality   varchar(255),
   has_good_quality  varchar(255),
   visit_type        varchar(7),
   first_loaded_on   timestamptz,
   last_modified_on  timestamptz
);

ALTER TABLE consolidated.monitor_visit_source
   ADD CONSTRAINT pk_monitor_visit_source
   PRIMARY KEY (account_id, visit_id);

COMMIT;