DROP TABLE IF EXISTS consolidated.user_feedback_fact CASCADE;

CREATE TABLE consolidated.user_feedback_fact
(
   account_id                  varchar(50)    NOT NULL,
   visit_id                    varchar(50)    NOT NULL,
   product_ean                 varchar(500)   NOT NULL,
   feedback_registration_date  timestamp,
   kpi_id                      varchar(10),
   kpi                         varchar(50),
   toggle1                     varchar(50),
   status_after_verif          varchar(10),
   first_loaded_on             timestamptz,
   last_modified_on            timestamptz
);

ALTER TABLE consolidated.user_feedback_fact
   ADD CONSTRAINT pk_user_feedback_fact
   PRIMARY KEY (account_id, visit_id, product_ean);

GRANT SELECT ON consolidated.user_feedback_fact TO group powerbi_direct;
GRANT SELECT ON consolidated.user_feedback_fact TO powerbi_direct;

COMMIT;