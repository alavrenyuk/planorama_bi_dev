DROP TABLE IF EXISTS consolidated.visit_fact CASCADE;

CREATE TABLE consolidated.visit_fact
(
   account_id          varchar(50)     NOT NULL,
   visit_id            varchar(50)     NOT NULL,
   visit_date          date,
   visit_datetime      timestamptz,
   analysis_date       date,
   categ_code          varchar(30),
   categ_name          varchar(255),
   owner_id            varchar(50),
   store_id            varchar(50),
   number_of_pictures  float4,
   number_of_sections  float4,
   process_type        varchar(50),
   user_comment        varchar(1000),
   plan_tag            varchar(255),
   first_loaded_on     timestamptz,
   last_modified_on    timestamptz
);

ALTER TABLE consolidated.visit_fact
   ADD CONSTRAINT pk_visit_fact
   PRIMARY KEY (account_id, visit_id);

GRANT SELECT ON consolidated.visit_fact TO powerbi_direct;

COMMIT;