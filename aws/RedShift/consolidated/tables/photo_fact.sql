DROP TABLE IF EXISTS consolidated.photo_fact CASCADE;

CREATE TABLE consolidated.photo_fact
(
   account_id            varchar(50)     NOT NULL,
   visit_id              varchar(50)     NOT NULL,
   owner_id              varchar(50)     NOT NULL,
   store_id              varchar(50)     NOT NULL,
   visit_date            date            NOT NULL,
   photo_quality_int     smallint,
   photo_quality         varchar(255),
   photo_tag             varchar(255),
   photo_url             varchar(2083),
   max_shelf             float4,
   photo_quality_reason  varchar(255),
   photo_index           float4,
   section_index         float4,
   stack_index           float4,
   photo_group           float8,
   photo_group_type      varchar(255),
   photo_group_tags      varchar(255),
   first_loaded_on       timestamptz,
   last_modified_on      timestamptz
);

ALTER TABLE consolidated.photo_fact
   ADD CONSTRAINT pk_photo_fact
   PRIMARY KEY (account_id, visit_id, owner_id, store_id);

GRANT SELECT ON consolidated.photo_fact TO group powerbi_direct;
GRANT SELECT ON consolidated.photo_fact TO powerbi_direct;

COMMIT;