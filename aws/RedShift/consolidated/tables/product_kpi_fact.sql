DROP TABLE IF EXISTS consolidated.product_kpi_fact CASCADE;

CREATE TABLE consolidated.product_kpi_fact
(
   account_id          varchar(50)    NOT NULL,
   visit_id            varchar(50)    NOT NULL,
   product_id          varchar(50)    NOT NULL,
   store_id            varchar(50),
   owner_id            varchar(50),
   visit_date          date,
   prod_ean            varchar(500),
   analysis            varchar(255),
   kpi                 varchar(255),
   own                 float4,
   currnt_stat         varchar(50),
   value               float8,
   out_of_stock        float4,
   display_group       varchar(255),
   product_group_name  varchar(255),
   part_of_group       boolean,
   ranking             smallint,
   categ_name          varchar(255),
   first_loaded_on     timestamptz,
   last_modified_on    timestamptz
);

ALTER TABLE consolidated.product_kpi_fact
   ADD CONSTRAINT pk_product_kpi_fact
   PRIMARY KEY (account_id, visit_id, product_id);

GRANT SELECT ON consolidated.product_kpi_fact TO group powerbi_direct;
GRANT SELECT ON consolidated.product_kpi_fact TO powerbi_user1;
GRANT TRIGGER, DELETE, INSERT, UPDATE, REFERENCES, RULE, SELECT ON consolidated.product_kpi_fact TO planorama;

COMMIT;