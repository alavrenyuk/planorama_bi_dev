DROP TABLE IF EXISTS consolidated.master_store CASCADE;

CREATE TABLE consolidated.master_store
(
   account_id        varchar(50)      NOT NULL,
   store_id          varchar(50)      NOT NULL,
   internal_code     varchar(255),
   chainstore        varchar(255),
   description       varchar(255),
   address           varchar(255),
   region            varchar(255),
   city              varchar(50),
   postal_code       varchar(50),
   owners            varchar(65535),
   desc_a            varchar(255),
   desc_b            varchar(255),
   desc_c            varchar(255),
   desc_d            varchar(255),
   desc_e            varchar(255),
   desc_f            varchar(255),
   desc_g            varchar(255),
   desc_h            varchar(255),
   country           varchar(50),
   latitude          float8,
   longitude         float8,
   is_deleted        boolean,
   first_loaded_on   timestamptz,
   last_modified_on  timestamptz
);

ALTER TABLE consolidated.master_store
   ADD CONSTRAINT pk_account_id_store_id
   PRIMARY KEY (account_id, store_id);

GRANT INSERT, REFERENCES, RULE, TRIGGER, UPDATE, DELETE, SELECT ON consolidated.master_store TO group powerbi_direct;
GRANT TRIGGER, INSERT, REFERENCES, RULE, UPDATE, SELECT, DELETE ON consolidated.master_store TO powerbi_direct;
GRANT TRIGGER, REFERENCES, DELETE, INSERT, UPDATE, RULE, SELECT ON consolidated.master_store TO planorama;

COMMIT;