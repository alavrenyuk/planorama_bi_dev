DROP TABLE IF EXISTS consolidated.master_product CASCADE;

CREATE TABLE consolidated.master_product
(
   account_id        varchar(50)    NOT NULL,
   product_id        varchar(50)    NOT NULL,
   base              varchar(255),
   product_ean       varchar(50),
   description       varchar(255),
   manufacturer      varchar(255),
   brand             varchar(255),
   height            integer,
   width             integer,
   depth             integer,
   code              varchar(255),
   crea_on           timestamp,
   mod_on            timestamp,
   seg1              varchar(255),
   seg2              varchar(255),
   seg3              varchar(255),
   seg4              varchar(255),
   seg5              varchar(255),
   desc_a            varchar(255),
   desc_b            varchar(255),
   desc_c            varchar(255),
   desc_d            varchar(255),
   desc_e            varchar(255),
   desc_f            varchar(255),
   desc_g            varchar(255),
   desc_h            varchar(255),
   desc_i            varchar(255),
   desc_j            varchar(255),
   kind              integer,
   labelling         varchar(255),
   is_validated      boolean,
   excl_from         boolean,
   img               integer,
   is_deleted        boolean,
   desc_k            varchar(256),
   desc_l            varchar(256),
   desc_m            varchar(256),
   desc_n            varchar(256),
   desc_o            varchar(256),
   desc_p            varchar(256),
   desc_q            varchar(256),
   desc_r            varchar(256),
   desc_s            varchar(256),
   desc_t            varchar(256),
   desc_u            varchar(256),
   desc_v            varchar(256),
   desc_w            varchar(256),
   desc_x            varchar(256),
   desc_y            varchar(256),
   desc_z            varchar(256),
   first_loaded_on   timestamptz,
   last_modified_on  timestamptz
);

ALTER TABLE consolidated.master_product
   ADD CONSTRAINT pk_master_product
   PRIMARY KEY (account_id, product_id);

GRANT TRIGGER, INSERT, REFERENCES, RULE, UPDATE, SELECT, DELETE ON consolidated.master_product TO powerbi_direct;
GRANT SELECT ON consolidated.master_product TO powerbi_user1;
GRANT TRIGGER, REFERENCES, DELETE, INSERT, UPDATE, RULE, SELECT ON consolidated.master_product TO planorama;
GRANT INSERT, REFERENCES, RULE, TRIGGER, UPDATE, DELETE, SELECT ON consolidated.master_product TO group powerbi_direct;

COMMIT;