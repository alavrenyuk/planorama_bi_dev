DROP TABLE IF EXISTS consolidated.product_fact CASCADE;

CREATE TABLE consolidated.product_fact
(
   account_id                 varchar(50)    NOT NULL,
   visit_id                   varchar(50)    NOT NULL,
   owner_id                   varchar(50)    NOT NULL,
   store_id                   varchar(50)    NOT NULL,
   product_id                 varchar(50)    NOT NULL,
   visit_date                 date           NOT NULL,
   photo_index                smallint,
   section_index              smallint,
   photo_group                smallint,
   photo_group_type           varchar(255),
   shelf_level                smallint,
   horizontal_position_index  smallint,
   vertical_position          smallint,
   categ_code                 varchar(255),
   categ_name                 varchar(255),
   ranking                    float8,
   facings_tgt                float8,
   own                        float4,
   currnt_stat                varchar(50),
   value                      float8,
   out_of_stock               float4,
   linear                     float8,
   facings                    float8,
   packages                   float8,
   is_promo                   boolean,
   price                      float8,
   first_loaded_on            timestamptz,
   last_modified_on           timestamptz
);

ALTER TABLE consolidated.product_fact
   ADD CONSTRAINT pk_product_fact
   PRIMARY KEY (account_id, visit_id, owner_id, store_id, product_id, visit_date);

GRANT SELECT ON consolidated.product_fact TO group powerbi_direct;
GRANT SELECT ON consolidated.product_fact TO powerbi_direct;

COMMIT;