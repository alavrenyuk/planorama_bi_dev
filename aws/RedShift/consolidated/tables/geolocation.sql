DROP TABLE IF EXISTS consolidated.geolocation CASCADE;

CREATE TABLE consolidated.geolocation
(
   account_id                varchar(255) NOT NULL,
   visit_id                  varchar(255) NOT NULL,
   visit_date                timestamp,
   owner_id                  varchar(255),
   store_code                varchar(255),
   number_of_pictures        float8,
   photo_index               float8 NOT NULL,
   store_latitude            float8,
   store_longitude           float8,
   photo_latitude            float8,
   photo_longitude           float8,
   location_accuracy         float8,
   suspicious_photo          varchar(255),
   photo_distance_tolerance  float8,
   location_difference       float8,
   first_loaded_on           timestamptz,
   last_modified_on          timestamptz
);

ALTER TABLE consolidated.geolocation
   ADD CONSTRAINT pk_geolocation
   PRIMARY KEY (account_id, visit_id, photo_index);

GRANT SELECT ON consolidated.geolocation TO group powerbi_direct;
GRANT SELECT ON consolidated.geolocation TO powerbi_direct;

COMMIT;