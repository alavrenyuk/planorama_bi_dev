DROP TABLE IF EXISTS consolidated.accuracy_product_fact CASCADE;

CREATE TABLE consolidated.accuracy_product_fact
(
   account_id                         varchar(50)   NOT NULL,
   visit_id                           varchar(50)   NOT NULL,
   account_name                       varchar(255),
   category                           varchar(255),
   creation_date                      timestamptz,
   product_ean                        varchar(50)   NOT NULL,
   kind                               varchar(20),
   product_name                       varchar(255),
   labelling_operator                 varchar(255),
   verification_operator              varchar(255),
   audit_operator                     varchar(255),
   ref_total_present_sku              float4,
   ref_total_present_sku_correct      float4,
   ref_own_present_sku                float4,
   ref_own_present_sku_correct        float4,
   ref_total_present_facings          float4,
   ref_total_present_facings_correct  float4,
   ref_own_present_facings            float4,
   ref_own_present_facings_correct    float4,
   verif_total_sku                    float4,
   verif_total_sku_correct            float4,
   verif_own_sku                      float4,
   verif_own_sku_correct              float4,
   verif_total_facing                 float4,
   verif_total_facing_correct         float4,
   verif_own_facing                   float4,
   verif_own_facing_correct           float4,
   audit_total_sku                    float4,
   audit_total_sku_correct            float4,
   audit_own_sku                      float4,
   audit_own_sku_correct              float4,
   audit_total_facing                 float4,
   audit_total_facing_correct         float4,
   audit_own_facing                   float4,
   audit_own_facing_correct           float4,
   audit_precision_sku                float8,
   audit_precision_sku_own            float8,
   audit_precision_facing             float8,
   audit_precision_facing_own         float8,
   audit_recall_sku                   float8,
   audit_recall_sku_own               float8,
   audit_recall_facing                float8,
   audit_recall_facing_own            float8,
   top_sku                            float4,
   star_sku                           float4,
   core_sku                           float4,
   npd                                float4,
   osa                                float4,
   first_loaded_on                    timestamptz,
   last_modified_on                   timestamptz
);

ALTER TABLE consolidated.accuracy_product_fact
   ADD CONSTRAINT pk_accuracy_product_fact
   PRIMARY KEY (account_id, visit_id, product_ean);

GRANT SELECT ON consolidated.accuracy_product_fact TO group powerbi_direct;
GRANT SELECT ON consolidated.accuracy_product_fact TO powerbi_direct;

COMMIT;