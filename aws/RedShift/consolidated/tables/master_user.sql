DROP TABLE IF EXISTS consolidated.master_user CASCADE;

CREATE TABLE consolidated.master_user
(
   account_id        varchar(50)    NOT NULL,
   owner_id          varchar(50)    NOT NULL,
   username          varchar(50),
   first_name        varchar(50),
   last_name         varchar(50),
   email             varchar(255),
   role              varchar(255),
   roles             varchar(255),
   manager           varchar(255),
   password          varchar(255),
   desc_a            varchar(255),
   desc_b            varchar(255),
   desc_c            varchar(255),
   desc_d            varchar(255),
   desc_e            varchar(255),
   desc_f            varchar(255),
   desc_g            varchar(255),
   desc_h            varchar(255),
   active            boolean,
   first_loaded_on   timestamptz,
   last_modified_on  timestamptz
);

ALTER TABLE consolidated.master_user
   ADD CONSTRAINT pk_master_user
   PRIMARY KEY (account_id, owner_id);

GRANT SELECT ON consolidated.master_user TO group powerbi_direct;
GRANT SELECT ON consolidated.master_user TO powerbi_direct;

COMMIT;