DROP TABLE IF EXISTS consolidated.calendar CASCADE;

CREATE TABLE consolidated.calendar
(
   account_id        varchar(50)   NOT NULL,
   visit_date        date          NOT NULL,
   year              integer,
   month_no          integer,
   month_name        varchar(50),
   week              varchar(16),
   week_no           integer,
   year_week         varchar(12),
   quarter           varchar(10),
   quarter_no        integer,
   past_year         integer,
   past_month_no     integer,
   past_week_no      integer,
   past_quarter_no   integer,
   first_loaded_on   timestamptz,
   last_modified_on  timestamptz
);

ALTER TABLE consolidated.calendar
   ADD CONSTRAINT pk_calendar
   PRIMARY KEY (account_id, visit_date);

GRANT SELECT ON consolidated.calendar TO group powerbi_direct;
GRANT SELECT ON consolidated.calendar TO powerbi_direct;

COMMIT;