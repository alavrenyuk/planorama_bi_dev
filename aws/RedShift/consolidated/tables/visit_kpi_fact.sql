DROP TABLE IF EXISTS consolidated.visit_kpi_fact CASCADE;

CREATE TABLE consolidated.visit_kpi_fact
(
   account_id          char(24)        NOT NULL,
   visit_id            char(24)        NOT NULL,
   visit_date          timestamp,
   store_id            varchar(256),
   owner_id            varchar(256),
   analysis            varchar(256),
   kpi                 varchar(256),
   kpi_id              integer,
   own                 boolean,
   display_group       varchar(256),
   question_english    varchar(1000),
   question_local      varchar(1000),
   product_group_name  varchar(256),
   prod_manufacturer   varchar(256),
   prod_brand          varchar(256),
   shelf_level         float4,
   linear              float8,
   facings             integer,
   packages            integer,
   value               float8,
   target              float8,
   discrepancy         float8,
   compliant           boolean,
   kpi_group           varchar(256),
   question_answers    varchar(1000),
   photo_group         smallint,
   photo_group_type    varchar(256),
   custom_field_a      varchar(256),
   custom_field_b      varchar(256),
   custom_field_c      varchar(256),
   custom_field_d      varchar(256),
   custom_field_e      varchar(256),
   row_id              integer         NOT NULL,
   first_loaded_on     timestamptz,
   last_modified_on    timestamptz
);

ALTER TABLE consolidated.visit_kpi_fact
   ADD CONSTRAINT pk_visit_kpi_fact
   PRIMARY KEY (account_id, visit_id, row_id);

GRANT SELECT ON consolidated.visit_kpi_fact TO group powerbi_direct;
GRANT SELECT ON consolidated.visit_kpi_fact TO powerbi_direct;

COMMIT;